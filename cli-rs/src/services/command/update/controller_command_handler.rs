use crate::dtos::input::federation_controller_update::FederationControllerUpdateInputVariables;
use crate::dtos::trile_cli::environment_variables::{TrileFederationEnvVars, TRILE_ENVIRONMENT};
use crate::dtos::trile_cli::{defaults, Component, Environment, File};
use crate::files::loader;
use crate::services::env_vars_file_loader::env_vars_file_loader;
use crate::services::path_sanitizer;
use crate::services::shell::docker_compose_up::docker_compose_up;
use crate::services::shell::env_sust::env_sust;
use std::str::FromStr;

pub fn update_controller_command_handler(input_variables: FederationControllerUpdateInputVariables) -> Result<(), Box<dyn std::error::Error>> {
    let user_home_absolute_path = path_sanitizer::expand_home(&defaults::user_home_absolute_path());
    let configuration_path = format!("{}/.config/trile", user_home_absolute_path);
    let installation_path = format!("{}/federations/{}/controller", configuration_path, input_variables.federation_name);
    let environment_variables = env_vars_file_loader(&(installation_path.clone() + "/.installationEnvVars"));
    let fed_vars = env_vars_file_loader(&(installation_path.clone() + "/.fedVars"));
    let mut final_env_vars = environment_variables.clone();
    final_env_vars.extend(fed_vars);
    let docker_compose_absolute_path = get_docker_compose_absolute_path(&installation_path);

    update_configuration_files(&final_env_vars, &installation_path)?;
    docker_compose_up(&docker_compose_absolute_path)
}

fn update_configuration_files(env_vars: &TrileFederationEnvVars, installation_path: &str) -> Result<(), Box<dyn std::error::Error>> {
    let env = Environment::from_str(&env_vars[TRILE_ENVIRONMENT])?;
    let docker_compose_absolute_path = get_docker_compose_absolute_path(installation_path);
    let docker_compose_template_content = loader::get(File::DockerCompose, Component::FederationController, env);

    env_sust(env_vars.clone(), &docker_compose_template_content, &docker_compose_absolute_path)?;
    Ok(())
}

fn get_docker_compose_absolute_path(configuration_folder_absolute_path: &str) -> String {
    format!("{}/docker-compose.yaml", configuration_folder_absolute_path)
}