use crate::dtos::input::federation_member_install::FederationMemberInstallInputVariables;
use crate::dtos::trile_cli::environment_variables::{TrileFederationEnvVars, TFM_CONFIGURATION_FOLDER, TFM_SHARING_FOLDER, TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_DATA_DIRECTORY, TRILE_IPFS_DOCKER_CONTAINER_DATA_DIRECTORY};
use crate::dtos::trile_cli::{Component, File};
use crate::files::loader;
use crate::services::federation_member_environment_variables::federation_member_environment_variables;
use crate::services::shell::docker_compose_up::docker_compose_up;
use crate::services::shell::env_sust::env_sust;
use crate::services::shell::mkdir_p::mkdir_p;
use crate::services::shell::sudo_rm::sudo_rm;
use std::fs;

pub fn install_member_command_handler(
    input_variables: FederationMemberInstallInputVariables,
    fedvars_as_map: TrileFederationEnvVars,
) -> Result<(), Box<dyn std::error::Error>> {
    let environment_variables: TrileFederationEnvVars = federation_member_environment_variables(input_variables.clone(), fedvars_as_map);
    let environment = input_variables.environment;
    let configuration_folder_absolute_path = get_configuration_folder_absolute_path(&environment_variables);
    let ipfs_configuration_folder_absolute_path = get_ipfs_configuration_folder_absolute_path(&environment_variables);
    let ipfs_cluster_configuration_folder_absolute_path = get_ipfs_cluster_configuration_folder_absolute_path(&environment_variables);
    let federation_member_sharing_folder = get_federation_member_sharing_folder(&environment_variables);
    let docker_compose_absolute_path = format!("{}/docker-compose.yaml", configuration_folder_absolute_path);
    let swarm_key_absolute_path = format!("{}/swarm.key", ipfs_configuration_folder_absolute_path);
    let swarm_key_template_content = loader::get(File::SwarmKey, Component::FederationMember, environment.clone());
    let docker_compose_template_content = loader::get(File::DockerCompose, Component::FederationMember, environment);

    sudo_rm(&configuration_folder_absolute_path)?;
    mkdir_p(&configuration_folder_absolute_path)?;
    mkdir_p(&ipfs_configuration_folder_absolute_path)?;
    mkdir_p(&ipfs_cluster_configuration_folder_absolute_path)?;
    mkdir_p(&federation_member_sharing_folder)?;
    env_sust(environment_variables.clone(), &swarm_key_template_content, &swarm_key_absolute_path)?;
    env_sust(environment_variables.clone(), &docker_compose_template_content, &docker_compose_absolute_path)?;
    docker_compose_up(&docker_compose_absolute_path)?;
    generate_installation_env_vars(&environment_variables)?;

    Ok(())
}

fn generate_installation_env_vars(env_vars: &TrileFederationEnvVars) -> Result<(), Box<dyn std::error::Error>> {
    let configuration_folder_absolute_path = get_configuration_folder_absolute_path(env_vars);
    let installation_env_vars_absolute_path = format!("{}/.installationEnvVars", configuration_folder_absolute_path);
    let env_vars_content: String = env_vars.iter()
        .map(|(k, v)| format!("{}={}", k, v))
        .collect::<Vec<_>>()
        .join("\n");
    fs::write(installation_env_vars_absolute_path, env_vars_content.as_bytes())?;
    Ok(())
}

fn get_federation_member_sharing_folder(env_vars: &TrileFederationEnvVars) -> String {
    env_vars[TFM_SHARING_FOLDER].clone()
}

fn get_configuration_folder_absolute_path(env_vars: &TrileFederationEnvVars) -> String {
    env_vars[TFM_CONFIGURATION_FOLDER].clone()
}

fn get_ipfs_configuration_folder_absolute_path(env_vars: &TrileFederationEnvVars) -> String {
    env_vars[TRILE_IPFS_DOCKER_CONTAINER_DATA_DIRECTORY].clone()
}

fn get_ipfs_cluster_configuration_folder_absolute_path(env_vars: &TrileFederationEnvVars) -> String {
    env_vars[TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_DATA_DIRECTORY].clone()
}