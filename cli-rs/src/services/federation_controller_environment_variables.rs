use crate::dtos::input::federation_controller_install::FederationControllerInstallInputVariables;
use crate::dtos::trile_cli;
use crate::dtos::trile_cli::environment_variables::*;
use crate::dtos::trile_cli::Environment;
use crate::services::shell::ed25519_key_generation::ed25519_key_generation;
use crate::services::shell::group_id::get_group_id;
use crate::services::shell::user_id::get_user_id;
use std::collections::HashMap;
use std::net::IpAddr;

pub fn federation_controller_environment_variables(
    input_variables: FederationControllerInstallInputVariables,
) -> TrileFederationEnvVars {
    let user_home_absolute_path = expand_home(&trile_cli::defaults::user_home_absolute_path());
    let configuration_path = format!("{}/.config/trile", user_home_absolute_path);
    let environment = input_variables.clone().environment;
    let federation_name = input_variables.clone().federation_name;
    let controller_network_address = input_variables.clone().network_address;
    let controller_name = format!("{}-federation-controller", federation_name);
    let controller_configuration_folder = format!("{}/federations/{}/controller", configuration_path, federation_name);

    let certbot_container_name = format!("trile-{}-certbot", controller_name);
    let certbot_service_name = certbot_container_name.replace('-', "_");
    let nginx_container_name = format!("trile-{}-nginx", controller_name);
    let nginx_service_name = nginx_container_name.replace('-', "_");
    let backend_container_name = format!("trile-{}-backend", controller_name);
    let backend_service_name = backend_container_name.replace('-', "_");
    let ipfs_swarm_port = trile_cli::defaults::IPFS_SWARM_PORT;
    let ipfs_cluster_swarm_port = trile_cli::defaults::IPFS_CLUSTER_SWARM_PORT;
    let shared_disk_space_gb = input_variables.clone()
        .shared_disk_space_gb
        .unwrap_or(trile_cli::defaults::SHARED_DISK_SPACE_GB);
    let certbot_directory = input_variables.clone()
        .certbot_folder_absolute_path_optional
        .unwrap_or(format!("{}/certbot", controller_configuration_folder));

    let mut env_vars = HashMap::new();
    env_vars.insert(DOLLAR.to_string(), "$".to_string());
    env_vars.insert(HOST_UID.to_string(), get_user_id().to_string());
    env_vars.insert(HOST_GID.to_string(), get_group_id().to_string());
    env_vars.insert(TRILE_ENVIRONMENT.to_string(), environment.to_string());
    env_vars.insert(TRILE_FEDERATION_NAME.to_string(), federation_name);
    env_vars.insert(TRILE_CONFIGURATION_FOLDER.to_string(), configuration_path);
    env_vars.insert(
        TRILE_BACKEND_DOCKER_CONTAINER_NAME.to_string(),
        backend_container_name,
    );
    env_vars.insert(
        TRILE_FRONTEND_DOCKER_CONTAINER_NAME.to_string(),
        format!("trile-{}-frontend", controller_name),
    );
    env_vars.insert(
        TRILE_IPFS_DOCKER_CONTAINER_NAME.to_string(),
        format!("trile-{}-ipfs", controller_name),
    );
    env_vars.insert(
        TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_NAME.to_string(),
        format!("trile-{}-ipfs-cluster", controller_name),
    );
    env_vars.insert(
        TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_DATA_DIRECTORY.to_string(),
        format!("{}/ipfs-cluster", controller_configuration_folder),
    );
    env_vars.insert(
        TRILE_IPFS_DOCKER_CONTAINER_DATA_DIRECTORY.to_string(),
        format!("{}/ipfs", controller_configuration_folder),
    );
    env_vars.insert(
        TRILE_IPFS_DOCKER_CONTAINER_DATA_CERTBOT_DIRECTORY.to_string(),
        certbot_directory,
    );
    env_vars.insert(TFC_BACKEND_SERVICE_NAME.to_string(), backend_service_name);
    env_vars.insert(
        TFC_CERTBOT_CONTAINER_NAME.to_string(),
        certbot_container_name,
    );
    env_vars.insert(TFC_CERTBOT_SERVICE_NAME.to_string(), certbot_service_name);
    env_vars.insert(TFC_CERTBOT_STAGING.to_string(), input_variables.clone().staging_ssl);
    env_vars.insert(
        TFC_CONFIGURATION_FOLDER.to_string(),
        controller_configuration_folder.clone(),
    );
    env_vars.insert(
        TFC_JOIN_TOKEN.to_string(),
        generate_random_string(32),
    );
    env_vars.insert(
        TFC_FEDVARS_ABSOLUTE_PATH.to_string(),
        format!("{}/.fedvars", controller_configuration_folder.clone()),
    );
    env_vars.insert(
        TFC_IPFS_CLUSTER_ADDRESS.to_string(),
        get_ipfs_cluster_address(controller_network_address, environment.clone(), ipfs_cluster_swarm_port),
    );
    env_vars.insert(
        TFC_IPFS_CLUSTER_REPLICA_FACTOR_MAX.to_string(),
        input_variables.ipfs_cluster_replica_factor_max.to_string(),
    );
    env_vars.insert(
        TFC_IPFS_MAX_DISK_SPACE_GB.to_string(),
        shared_disk_space_gb.to_string(),
    );
    env_vars.insert(
        TFC_IPFS_ADDRESS.to_string(),
        get_ipfs_address(controller_network_address, environment.clone(), ipfs_swarm_port),
    );
    env_vars.insert(TFC_IPFS_SWARM_PORT.to_string(), ipfs_swarm_port.to_string());
    env_vars.insert(
        TFC_IPFS_CLUSTER_SWARM_PORT.to_string(),
        ipfs_cluster_swarm_port.to_string(),
    );
    env_vars.insert(
        TFC_NETWORK_ADDRESS.to_string(),
        get_network_address(&input_variables),
    );
    env_vars.insert(TFC_P2P_PRIVATE_KEY.to_string(), ed25519_key_generation().ok().unwrap());
    env_vars.insert(TFC_P2P_PORT.to_string(), "7078".to_string());
    env_vars.insert(
        TFC_REVERSE_PROXY_CONFIGURATION.to_string(),
        format!("{}/nginx/nginx.conf", controller_configuration_folder),
    );
    env_vars.insert(
        TFC_REVERSE_PROXY_CONTAINER_NAME.to_string(),
        nginx_container_name,
    );
    env_vars.insert(
        TFC_REVERSE_PROXY_SERVICE_NAME.to_string(),
        nginx_service_name,
    );

    env_vars
}

fn get_network_address(input_variables: &FederationControllerInstallInputVariables) -> String {
    match input_variables.environment {
        Environment::Dev => input_variables.network_address.to_string(),
        Environment::Production => input_variables.network_address.to_string(),
    }
}

fn get_ipfs_address(network_address: IpAddr, environment: Environment, port: &str) -> String {
    match environment {
        Environment::Dev => format!("/ip4/{}/tcp/{}", network_address, port),
        Environment::Production => format!("/dns4/ipfs.{}/tcp/{}", network_address, port),
    }
}

fn get_ipfs_cluster_address(network_address: IpAddr, environment: Environment, port: &str) -> String {
    match environment {
        Environment::Dev => format!("/ip4/{}/tcp/{}", network_address, port),
        Environment::Production => format!("/dns4/ipfs-cluster.{}/tcp/{}", network_address, port),
    }
}

fn generate_random_string(len: usize) -> String {
    use rand::Rng;
    rand::thread_rng()
        .sample_iter(&rand::distributions::Alphanumeric)
        .take(len)
        .map(char::from)
        .collect()
}

fn expand_home(input_path: &str) -> String {
    if input_path.starts_with("~") {
        let user_home = std::env::var("HOME").unwrap_or_else(|_| ".".to_string());
        format!("{}{}", user_home, &input_path[1..])
    } else {
        input_path.to_string()
    }
}