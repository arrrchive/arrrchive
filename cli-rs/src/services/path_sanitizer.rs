use crate::dtos::trile_cli::defaults;

pub fn expand_home(path: &str) -> String {
    if path.starts_with('~') {
        path.replacen("~", &defaults::user_home_absolute_path(), 1)
    } else {
        path.to_string()
    }
}