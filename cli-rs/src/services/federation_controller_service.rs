use std::thread;
use crate::dtos::aliases::P2pPeerId;
use crate::dtos::trile_cli::{environment_variables::TrileFederationEnvVars, Environment};
use reqwest::blocking::Response;
use reqwest::header::AUTHORIZATION;
use reqwest::StatusCode;
use std::time::Duration;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum FederationControllerServiceError {
    #[error("Request failed")]
    RequestFailed,
    #[error("Invalid response")]
    InvalidResponse,
}

pub fn get_fedvars(environment: Environment, network_address: &str, join_token: &str) -> Result<TrileFederationEnvVars, FederationControllerServiceError> {
    let uri = get_fedvars_uri(&environment, network_address);
    let response = trigger_fedvars_request(&uri, join_token)?;
    let result: TrileFederationEnvVars = response
        .split("\\n")
        .map(|line| {
            let mut parts = line.splitn(2, '=');
            let key = parts.next().unwrap_or("").to_string();
            let value = parts.next().unwrap_or("").to_string();
            (key, value)
        })
        .collect();

    Ok(result)
}

pub fn get_peer_id(environment: Environment, network_address: &str) -> Result<P2pPeerId, FederationControllerServiceError> {
    let uri = get_id_uri(&environment, network_address);
    get_p2p_peer_id_inner(&uri, 300)
}

fn get_fedvars_uri(environment: &Environment, network_address: &str) -> String {
    match environment {
        Environment::Dev => format!("http://{}:8078/api/federation/.fedvars", network_address),
        Environment::Production => format!("https://api.{}/api/federation/.fedvars", network_address),
    }
}

fn get_id_uri(environment: &Environment, network_address: &str) -> String {
    match environment {
        Environment::Dev => format!("http://{}:8078/api/p2p/id", network_address),
        Environment::Production => format!("https://api.{}/api/p2p/id", network_address),
    }
}

fn get_p2p_peer_id_inner(uri: &str, attempts_left: i32) -> Result<P2pPeerId, FederationControllerServiceError> {
    if attempts_left <= 0 {
        return Err(FederationControllerServiceError::RequestFailed);
    }

    match trigger_request(uri) {
        Ok(peer_id) => Ok(peer_id),
        Err(_) => {
            let _ = thread::sleep(Duration::from_secs(1));
            get_p2p_peer_id_inner(uri, attempts_left - 1)
        }
    }
}

fn trigger_fedvars_request(uri: &str, join_token: &str) -> Result<String, FederationControllerServiceError> {
    let client = reqwest::blocking::Client::new();
    let response: Response =client
        .get(uri)
        .header(AUTHORIZATION, format!("Bearer {}", join_token))
        .send()
        .map_err(|_| FederationControllerServiceError::RequestFailed)?;

    handle_response(response)
}

fn trigger_request(uri: &str) -> Result<String, FederationControllerServiceError> {
    let client = reqwest::blocking::Client::new();
    let response: Response = client.get(uri).send().map_err(|_| FederationControllerServiceError::RequestFailed)?;

    handle_response(response)
}

fn handle_response(response: Response) -> Result<String, FederationControllerServiceError> {
    if response.status() == StatusCode::OK {
        let body: String = response.text().map_err(|_| FederationControllerServiceError::InvalidResponse)?;
        Ok(body.trim_matches('"').to_string())
    } else {
        println!("{:?}", response);
        Err(FederationControllerServiceError::InvalidResponse)
    }
}
