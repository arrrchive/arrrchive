use super::exec_and_get_output::exec_and_get_stdout;
use std::io;

pub fn ed25519_key_generation() -> io::Result<String> {
    let command = "openssl genpkey -algorithm Ed25519";
    let output = exec_and_get_stdout(command, true)?;
    output.into_iter().nth(1).ok_or_else(|| io::Error::new(io::ErrorKind::Other, "Failed to generate key"))
}