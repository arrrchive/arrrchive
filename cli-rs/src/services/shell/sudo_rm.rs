use std::process::Command;
use super::sub_step;

const CMD_LABEL: &str = "Deleting folder";

pub fn sudo_rm(folder_path: &str) -> Result<(), Box<dyn std::error::Error>> {
    let message = format!("Up to delete {} folder as sudo", folder_path);
    let mut binding = Command::new("sh");
    let cmd: &mut Command = binding
            .arg("-c")
            .arg(format!(
            r#"if [ -d "{folder_path}" ]; then
    printf "{message}\n"
    sudo rm -rf "{folder_path}"
fi
"#,
            folder_path = folder_path,
            message = message
        ));

    let label: &String = &format!("{}: {}", CMD_LABEL, folder_path);
    sub_step::without_progress(cmd, label, message.len())
}