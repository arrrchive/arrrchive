use std::error::Error;
use std::io::{self, Write};
use colored::Colorize;
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};

pub struct PrintLn;

impl PrintLn {
    pub fn blue(string: &str) {
        Self::print_with_color(Color::Blue, string);
    }

    pub fn cyan(string: &str) {
        Self::print_with_color(Color::Cyan, string);
    }

    pub fn yellow(string: &str) {
        Self::print_with_color(Color::Yellow, string);
    }

    pub fn green(string: &str) {
        Self::print_with_color(Color::Green, string);
    }

    pub fn sub_step_attempt_tap<T>(label: &str, result: Result<T, Box<dyn Error>>, is_final: bool) {
        match result {
            Ok(_) => Self::sub_step_ok(label, is_final),
            Err(_) => Self::sub_step_error(label, is_final),
        }
    }


    pub fn sub_step_info(label: &str, is_final: bool) {
        let info = if is_final {
            format!("[i] {}", label)
        } else {
            format!("[i] {}", label)
        };
        Self::print_with_color(Color::Blue, &info);
    }

    pub fn sub_step_ok(label: &str, is_final: bool) {
        let msg: String = if is_final {
            format!("{} {}", "[✓]".green().bold(), label.green().bold())
        } else {
            format!("{} {}", "[✓]".green().bold(), label)
        };
        println!("{}", msg);
    }

    pub fn sub_step_error(label: &str, is_final: bool) {
        let msg = if is_final {
            format!("{} {}", "[x]".red().bold(), label.red().bold())
        } else {
            format!("{} {}", "[x]".red().bold(), label)
        };
        println!("{}", msg);
    }

    pub fn sub_step_replace_ok(label: &str, wiping_line_length: Option<usize>) {
        let padding = " ".repeat(wiping_line_length.unwrap_or_else(|| label.len()));
        print!("\r{} {} {}", "[✓]".green().bold(), label, padding);
        io::stdout().flush().unwrap();
        println!();
    }

    pub fn sub_step_replace_error(label: &str, wiping_line_length: Option<usize>) {
        let padding = " ".repeat(wiping_line_length.unwrap_or_else(|| label.len()));
        print!("\r{} {} {}", "[x]".red().bold(), label, padding);
        io::stdout().flush().unwrap();
        println!();
    }

    fn print_with_color(color: Color, string: &str) {
        let mut stdout = StandardStream::stdout(ColorChoice::Always);
        let mut color_spec = ColorSpec::new();
        color_spec.set_fg(Some(color)).set_bold(true);
        stdout.set_color(&color_spec).unwrap();
        writeln!(&mut stdout, "{}", string).unwrap();
        stdout.reset().unwrap();
    }
}
