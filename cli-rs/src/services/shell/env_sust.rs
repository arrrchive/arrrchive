use crate::services::shell::println::PrintLn;
use std::collections::HashMap;
use std::fs;
use std::path::Path;

pub fn env_sust(
    env_vars_map: HashMap<String, String>,
    template_content: &str,
    absolute_output_path: &str,
) -> Result<(), Box<dyn std::error::Error>> {
    let cmd_label = format!(
        "File generation: {}",
        Path::new(absolute_output_path)
            .file_name()
            .unwrap_or_else(|| Path::new(absolute_output_path).as_os_str())
            .to_string_lossy()
    );

    let result = apply_inner(&env_vars_map, template_content, absolute_output_path, true);
    match result {
        Ok(_) => PrintLn::sub_step_ok(&cmd_label, false),
        Err(_) => PrintLn::sub_step_error(&cmd_label, false),
    }

    result
}

fn apply_inner(
    env_vars_map: &HashMap<String, String>,
    template_content: &str,
    absolute_output_path: &str,
    quiet: bool,
)  -> Result<(), Box<dyn std::error::Error>> {
    if !quiet {
        println!("> envsust < .. > {}", absolute_output_path);
    }

    let replaced_content = env_vars_map.iter().fold(template_content.to_string(), |content, (key, value)| {
        content.replace(
            &format!("${{{}}}", key),
            value.trim_matches('"')
        )
    });

    fs::write(absolute_output_path, replaced_content.as_bytes())?;
    Ok(())
}
