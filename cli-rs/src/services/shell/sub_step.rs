use crate::services::shell::println::PrintLn;
use std::fmt::Debug;
use std::process::Command;
use std::thread;
use std::time::Duration;

pub fn without_progress(cmd: &mut Command, label: &str, wiping_line_length: usize) -> Result<(), Box<dyn std::error::Error>> {
    match cmd.status() {
        Ok(status) if status.success() => {
            PrintLn::sub_step_replace_ok(label, Some(wiping_line_length));
            Ok(())
        },
        Ok(_) => {
            PrintLn::sub_step_replace_error(label, None);
            Err(NonZeroExitCode.into())
        },
        Err(error) => {
            eprintln!("{}", error);
            PrintLn::sub_step_replace_error(label, None);
            Err(NonZeroExitCode.into())
        }
    }
}

pub fn with_progress(cmd: &mut Command, label: &str) -> Result<(), Box<dyn std::error::Error>> {
    let mut child = cmd.spawn().unwrap();
    let mut spinner = vec!['\\', '|', '/', '-'].into_iter().cycle();

    loop {
        match child.try_wait() {
            Ok(Some(status)) if status.success() => {
                PrintLn::sub_step_replace_ok(label, None);
                return Ok(());
            },
            Ok(None) => {
                if let Some(spin_char) = spinner.next() {
                    print!("\r\t\x1B[1;34m[{}]\x1B[0m {}", spin_char, label);
                }
                thread::sleep(Duration::from_millis(2));
            },
            Ok(Some(_)) | Err(_) => {
                print!("\r\t\x1B[1;34m[{}]\x1B[0m {}", spinner.next().unwrap_or('\\'), label);
                thread::sleep(Duration::from_millis(2));
                return Err(NonZeroExitCode.into());
            }
        }
    }
}

#[derive(Debug)]
pub struct NonZeroExitCode;

impl std::fmt::Display for NonZeroExitCode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Command exited with non-zero exit code")
    }
}

impl std::error::Error for NonZeroExitCode {}