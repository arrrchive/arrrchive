use std::process::Command;

pub fn clear() {
    let command = "clear -x";
    let _ = Command::new("sh")
        .arg("-c")
        .arg(command)
        .status()
        .expect("Failed to clear terminal");
}