use libc::getuid;

pub fn get_user_id() -> u32 {
    unsafe { getuid() }
}