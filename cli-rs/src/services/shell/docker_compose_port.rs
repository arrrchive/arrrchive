use super::exec_and_get_output::exec_and_get_stdout;
use std::io;

pub fn docker_compose_port(docker_compose_file: &str, service: &str, private_port: i32) -> io::Result<String> {
    let command = format!(
        "docker compose -f {} port {} {}",
        docker_compose_file, service, private_port
    );
    match exec_and_get_stdout(&command, true) {
        Ok(output) => {
            let parts: Vec<&str> = output.get(0).ok_or(io::Error::new(io::ErrorKind::Other, "No output"))?.split(':').collect();
            if let Some(port) = parts.get(1) {
                Ok(port.to_string())
            } else {
                Err(io::Error::new(io::ErrorKind::Other, "Failed to parse port"))
            }
        },
        Err(e) => Err(e),
    }
}