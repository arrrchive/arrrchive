use std::fs::File;
use std::io::{self, BufRead, BufReader};
use std::process::Command;

pub fn exec_and_get_stdout(command_str: &str, silent: bool) -> io::Result<Vec<String>> {
    if !silent {
        println!("> {}", command_str);
    }

    let output = Command::new("sh")
        .arg("-c")
        .arg(command_str)
        .output()?;

    if output.status.success() {
        let random_filename = generate_random_filename();
        std::fs::write(&random_filename, &output.stdout)?;
        let result = get_result(&random_filename)?;
        delete(&random_filename)?;
        Ok(result)
    } else {
        Err(io::Error::new(
            io::ErrorKind::Other,
            String::from_utf8_lossy(&output.stderr).to_string(),
        ))
    }
}

fn generate_random_filename() -> String {
    use rand::Rng;
    let random_string: String = rand::thread_rng()
        .sample_iter(&rand::distributions::Alphanumeric)
        .take(64)
        .map(char::from)
        .collect();
    format!("/tmp/{}", random_string)
}

fn delete(file_absolute_path: &str) -> io::Result<()> {
    std::fs::remove_file(file_absolute_path)?;
    Ok(())
}

fn get_result(file_absolute_path: &str) -> io::Result<Vec<String>> {
    let file = File::open(file_absolute_path)?;
    let reader = BufReader::new(file);
    let lines = reader.lines().collect::<Result<Vec<_>, _>>()?;
    Ok(lines)
}