use std::process::Command;
use super::sub_step;

const CMD_LABEL: &str = "Booting containers";

pub fn docker_compose_up(docker_compose_absolute_path: &str) -> Result<(), Box<dyn std::error::Error>> {
    // let cmd: &mut Command = &mut Command::new(get_cmd(docker_compose_absolute_path));

    let mut binding = Command::new("sh");
    let cmd: &mut Command = binding.arg("-c").arg(get_cmd(docker_compose_absolute_path));

    sub_step::with_progress(cmd, CMD_LABEL)
}

fn get_cmd(docker_compose_absolute_path: &str) -> String {
    format!(
        "docker compose --progress=quiet -f {} up --pull always --quiet-pull --force-recreate --detach --wait",
        docker_compose_absolute_path
    )
}