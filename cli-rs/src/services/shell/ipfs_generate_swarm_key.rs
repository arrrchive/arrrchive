use super::sub_step;
use std::path::Path;
use std::process::Command;

const CMD_LABEL: &str = "Creating IPFS swarm key";

pub fn ipfs_generate_swarm_key(swarm_key_absolute_path: &str) -> Result<(), Box<dyn std::error::Error>> {
    let label = format!(
        "{}: {}",
        CMD_LABEL,
        Path::new(swarm_key_absolute_path)
            .file_name()
            .unwrap_or_else(|| Path::new(swarm_key_absolute_path).as_os_str()).to_string_lossy());
    let mut binding = Command::new("sh");
    let cmd: &mut Command = binding
        .arg("-c")
        .arg(format!(
            r#"echo "/key/swarm/psk/1.0.0/\n/base16/\n$(hexdump -n 32 -e '16/1 "%02x"' /dev/urandom)" > {}"#,
            swarm_key_absolute_path
        ));

    sub_step::without_progress(cmd, &label, label.len())
}
