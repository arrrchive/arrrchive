use crate::dtos::trile_cli::environment_variables::TrileFederationEnvVars;
use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufRead, BufReader};

pub fn env_vars_file_loader(file_absolute_path: &str) -> TrileFederationEnvVars {
    load_fed_vars(file_absolute_path).unwrap_or_else(|_| HashMap::new())
}

fn load_fed_vars(file_path: &str) -> io::Result<TrileFederationEnvVars> {
    let file = File::open(file_path)?;
    let reader = BufReader::new(file);
    let lines = reader.lines();

    let fed_vars_map = load_fed_vars_map(lines)?;
    Ok(fed_vars_map)
}

fn load_fed_vars_map<I>(lines: I) -> io::Result<TrileFederationEnvVars>
where
    I: IntoIterator<Item = Result<String, io::Error>>,
{
    let mut map = HashMap::new();

    for line_result in lines {
        let line = line_result?;
        if let Some((key, value)) = parse_line(&line) {
            map.insert(key, value);
        }
    }

    Ok(map)
}

fn parse_line(line: &str) -> Option<(String, String)> {
    let parts: Vec<&str> = line.splitn(2, '=').collect();
    if parts.len() == 2 {
        Some((parts[0].to_string(), parts[1].to_string()))
    } else {
        None
    }
}