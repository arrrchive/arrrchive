use crate::dtos::input::federation_member_install::FederationMemberInstallInputVariables;
use crate::dtos::trile_cli::{
    defaults, environment_variables::*,
    Environment,
};
use crate::services::shell::ed25519_key_generation::ed25519_key_generation;
use crate::services::shell::group_id::get_group_id;
use crate::services::shell::user_id::get_user_id;
use std::env;
use std::net::Ipv4Addr;

pub fn federation_member_environment_variables(
    input_variables: FederationMemberInstallInputVariables,
    fedvars: TrileFederationEnvVars,
) -> TrileFederationEnvVars {
    let user_home_absolute_path = expand_home(&defaults::user_home_absolute_path());
    let configuration_path = format!("{}/.config/trile", user_home_absolute_path);
    let environment = input_variables.environment;
    let federation_name = fedvars.get(TRILE_FEDERATION_NAME).unwrap_or(&"".to_string()).clone();
    let member_nickname = input_variables.nickname.clone();
    let member_full_name = format!("{}-federation-member-{}", federation_name, member_nickname);
    let member_configuration_folder = format!("{}/federations/{}/members/{}", configuration_path, federation_name, member_nickname);
    let backend_container_name = format!("trile-{}-backend", member_full_name);
    let backend_service_name = backend_container_name.replace('-', "_");

    let mut env_vars = fedvars.clone();
    env_vars.insert(HOST_GID.to_string(), get_group_id().to_string());
    env_vars.insert(HOST_UID.to_string(), get_user_id().to_string());
    env_vars.insert(TRILE_ENVIRONMENT.to_string(), environment.clone().to_string());
    env_vars.insert(TRILE_CONFIGURATION_FOLDER.to_string(), configuration_path);
    env_vars.insert(TRILE_BACKEND_DOCKER_CONTAINER_NAME.to_string(), format!("trile-{}-backend", member_full_name));
    env_vars.insert(TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_DATA_DIRECTORY.to_string(), format!("{}/ipfs-cluster", member_configuration_folder));
    env_vars.insert(TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_NAME.to_string(), format!("trile-{}-ipfs-cluster", member_full_name));
    env_vars.insert(TRILE_IPFS_DOCKER_CONTAINER_DATA_DIRECTORY.to_string(), format!("{}/ipfs", member_configuration_folder));
    env_vars.insert(TRILE_IPFS_DOCKER_CONTAINER_NAME.to_string(), format!("trile-{}-ipfs", member_full_name));
    env_vars.insert(TFM_BACKEND_SERVICE_NAME.to_string(), backend_service_name);
    env_vars.insert(TFC_IPFS_CLUSTER_ADDRESS.to_string(), get_ipfs_cluster_address(&fedvars, environment.clone()));
    env_vars.insert(TFC_IPFS_ADDRESS.to_string(), get_ipfs_address(&fedvars, environment.clone()));
    env_vars.insert(TFM_CONFIGURATION_FOLDER.to_string(), member_configuration_folder);
    env_vars.insert(TFM_IPFS_CLUSTER_NAME.to_string(), member_full_name);
    env_vars.insert(TFM_MAX_DISK_SPACE_GB.to_string(), input_variables.shared_disk_space_gb.to_string());
    env_vars.insert(TFM_NICKNAME.to_string(), member_nickname);
    env_vars.insert(TFM_SHARING_FOLDER.to_string(), input_variables.sharing_folder.clone());
    env_vars.insert(TFM_P2P_PRIVATE_KEY.to_string(), ed25519_key_generation().unwrap_or("".to_string()));

    env_vars
}

fn get_ipfs_address(fedvars: &TrileFederationEnvVars, environment: Environment) -> String {
    let ipfs_port: String = fedvars.get(TFC_IPFS_SWARM_PORT).unwrap_or(&"0".to_string()).to_string();
    let ipfs_peer_id: String = fedvars.get(TFC_IPFS_PEER_ID).unwrap_or(&"".to_string()).to_string();
    let network_address = fedvars.get(TFC_NETWORK_ADDRESS).unwrap_or(&"".to_string()).parse::<Ipv4Addr>().unwrap_or(Ipv4Addr::new(0, 0, 0, 0));
    match environment {
        Environment::Dev => format!("/ip4/{}/tcp/{}/p2p/{}", network_address, ipfs_port, ipfs_peer_id),
        Environment::Production => format!("/dns4/ipfs.{}/tcp/{}/p2p/{}", network_address, ipfs_port, ipfs_peer_id),
    }
}

fn get_ipfs_cluster_address(fedvars: &TrileFederationEnvVars, environment: Environment) -> String {
    let ipfs_cluster_port: String = fedvars.get(TFC_IPFS_CLUSTER_SWARM_PORT).unwrap_or(&"0".to_string()).to_string();
    let ipfs_cluster_peer_id: String = fedvars.get(TFC_IPFS_CLUSTER_PEER_ID).unwrap_or(&"".to_string()).to_string();
    let network_address = fedvars.get(TFC_NETWORK_ADDRESS).unwrap_or(&"".to_string()).parse::<Ipv4Addr>().unwrap_or(Ipv4Addr::new(0, 0, 0, 0));
    match environment {
        Environment::Dev => format!("/ip4/{}/tcp/{}/p2p/{}", network_address, ipfs_cluster_port, ipfs_cluster_peer_id),
        Environment::Production => format!("/dns4/ipfs-cluster.{}/tcp/{}/p2p/{}", network_address, ipfs_cluster_port, ipfs_cluster_peer_id),
    }
}

fn expand_home(input_path: &str) -> String {
    if input_path.starts_with("~") {
        let user_home = env::var("HOME").unwrap_or_else(|_| ".".to_string());
        format!("{}{}", user_home, &input_path[1..])
    } else {
        input_path.to_string()
    }
}