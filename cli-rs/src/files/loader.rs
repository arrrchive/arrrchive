use crate::dtos::trile_cli::{Component, Environment, File};
use crate::files::templates::{self, dev, production};

pub fn get(file: File, component: Component, environment: Environment) -> String {
    match (file, component, environment) {
        (File::SwarmKey, _, _) => templates::swarm_key::swarm_key(),
        (File::DockerCompose, Component::FederationMember, Environment::Dev) => {
            dev::federation_member::docker_compose::docker_compose()
        }
        (File::DockerCompose, Component::FederationMember, Environment::Production) => {
            production::federation_member::docker_compose::docker_compose()
        }
        (File::DockerCompose, Component::FederationController, Environment::Dev) => {
            dev::federation_controller::docker_compose::docker_compose()
        }
        (File::DockerCompose, Component::FederationController, Environment::Production) => {
            production::federation_controller::docker_compose::docker_compose()
        }
        (File::NginxConf, Component::FederationController, Environment::Production) => {
            production::federation_controller::nginx_conf::nginx_conf()
        }
        (File::NginxPreSslConf, Component::FederationController, Environment::Production) => {
            production::federation_controller::nginx_pre_ssl_conf::nginx_conf()
        }
        (File::SslCertificate, Component::FederationController, Environment::Production) => {
            production::federation_controller::ssl_certificate::ssl_certificate()
        }
        _ => String::new(),
    }
}