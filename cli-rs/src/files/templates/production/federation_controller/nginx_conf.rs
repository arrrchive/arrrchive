pub fn nginx_conf() -> String {
    String::from(
        r#"
worker_processes 1;

events { worker_connections 1024; }

http {

    sendfile on;

    upstream backend-upstream {
        server ${TRILE_BACKEND_DOCKER_CONTAINER_NAME}:8078;
    }

    upstream ipfs-gateway-upstream {
        server ${TRILE_IPFS_DOCKER_CONTAINER_NAME}:8080;
    }

    upstream frontend-upstream {
        server ${TRILE_FRONTEND_DOCKER_CONTAINER_NAME}:3000;
    }

    server {
        listen 80;
        server_name ${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS};
        server_tokens off;

        location /.well-known/acme-challenge/ {
            root /var/www/certbot;
        }

        location / {
            return 301 https://${DOLLAR}host${DOLLAR}request_uri;
        }

    }

    server {
        listen 443 ssl;
        server_name api.${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS};
        server_tokens off;

        ssl_certificate /etc/letsencrypt/live/${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS}/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS}/privkey.pem;
        include /etc/letsencrypt/options-ssl-nginx.conf;
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

        location / {
            proxy_pass         http://backend-upstream;
            proxy_redirect     off;
            proxy_set_header   Host ${DOLLAR}host;
            proxy_set_header   X-Real-IP ${DOLLAR}remote_addr;
            proxy_set_header   X-Forwarded-For ${DOLLAR}proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host ${DOLLAR}server_name;
        }
    }

    server {
        listen 443 ssl;
        server_name gateway.ipfs.${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS};
        server_tokens off;

        ssl_certificate /etc/letsencrypt/live/${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS}/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS}/privkey.pem;
        include /etc/letsencrypt/options-ssl-nginx.conf;
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

        location / {
            proxy_pass         http://ipfs-gateway-upstream;
            proxy_redirect     off;
            proxy_set_header   Host ${DOLLAR}host;
            proxy_set_header   X-Real-IP ${DOLLAR}remote_addr;
            proxy_set_header   X-Forwarded-For ${DOLLAR}proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host ${DOLLAR}server_name;
        }
    }


    server {
        listen 443 ssl;
        server_name ${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS} www.${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS};
        server_tokens off;

        ssl_certificate /etc/letsencrypt/live/${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS}/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS}/privkey.pem;
        include /etc/letsencrypt/options-ssl-nginx.conf;
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

        location / {
            proxy_pass          http://frontend-upstream;
            proxy_set_header    Host                ${DOLLAR}http_host;
            proxy_set_header    X-Real-IP           ${DOLLAR}remote_addr;
            proxy_set_header    X-Forwarded-For     ${DOLLAR}proxy_add_x_forwarded_for;
        }
    }
}
        "#,
    )
}