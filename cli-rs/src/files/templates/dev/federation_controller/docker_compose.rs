pub fn docker_compose() -> String {
    String::from(
        r#"
services:

  trile_dev_federation_controller_frontend:
    container_name: trile-dev-federation-controller-frontend
    image: triledotlink/federation-controller-frontend:1.0
    depends_on:
      trile_dev_federation_controller_ipfs:
        condition: service_healthy
      trile_dev_federation_controller_ipfs_cluster:
        condition: service_started
    restart: unless-stopped
    deploy:
      resources:
        limits:
          memory: 1024M
        reservations:
          memory: 1024M
    ports:
      - "3000:3000"
    environment:
      REACT_APP_TRILE_BACKEND_URL: http://${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS}:8078
      REACT_APP_TRILE_IPFS_GATEWAY_URL: http://${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS}:8080
      REACT_APP_FEDERATION_NAME: '${TRILE_FEDERATION_NAME}'

  ${TRILE_FEDERATION_CONTROLLER_BACKEND_SERVICE_NAME}:
    container_name: ${TRILE_BACKEND_DOCKER_CONTAINER_NAME}
    image: triledotlink/federation-controller-backend:0.1.0-SNAPSHOT
    depends_on:
      trile_dev_federation_controller_ipfs:
        condition: service_healthy
      trile_dev_federation_controller_ipfs_cluster:
        condition: service_started
      trile_dev_federation_controller_redis_stack:
        condition: service_started
    deploy:
      resources:
        limits:
          memory: 2048M
        reservations:
          memory: 2048M
    restart: unless-stopped
    ports:
      - "8078:8078"
      - "7078:7078"
    environment:
      TRILE_FEDERATION_CONTROLLER_IPFS_API_URL: http://${TRILE_IPFS_DOCKER_CONTAINER_NAME}:5001/api/v0/
      TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_API_URL: http://${TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_NAME}:9094/
      TRILE_FEDERATION_CONTROLLER_IPFS_SWARM_KEY_VALUE: ${TRILE_FEDERATION_IPFS_SWARM_KEY_VALUE}
      TRILE_FEDERATION_CONTROLLER_FEDVARS_ABSOLUTE_PATH: /data/.fedvars
      TRILE_FEDERATION_CONTROLLER_P2P_PRIVATE_KEY: ${TRILE_FEDERATION_CONTROLLER_P2P_PRIVATE_KEY}
      TRILE_FEDERATION_CONTROLLER_REDIS_HOST: trile-dev-federation-controller-redis-stack
      TRILE_FEDERATION_CONTROLLER_JOIN_TOKEN: ${TRILE_FEDERATION_CONTROLLER_JOIN_TOKEN}
    volumes:
      - ${TRILE_FEDERATION_CONTROLLER_FEDVARS_ABSOLUTE_PATH}:/data/.fedvars
      - /var/run/docker.sock:/var/run/docker.sock

  trile_dev_federation_controller_ipfs_cluster:
    container_name: ${TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_NAME}
    image: ipfs/ipfs-cluster:v1.0.8
    depends_on:
      trile_dev_federation_controller_ipfs:
        condition: service_healthy
    deploy:
      resources:
        limits:
          memory: 256M
        reservations:
          memory: 256M
    restart: unless-stopped
    environment:
      CLUSTER_ANNOUNCEMULTIADDRESS: '${TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_ADDRESS}'
      CLUSTER_CRDT_CLUSTERNAME: 'dev'
      CLUSTER_CRDT_TRUSTEDPEERS: '*'
      CLUSTER_IPFSHTTP_NODEMULTIADDRESS: /dns4/${TRILE_IPFS_DOCKER_CONTAINER_NAME}/tcp/5001
      CLUSTER_MONITORPINGINTERVAL: 2s
      CLUSTER_PEERNAME: dev-controller
      CLUSTER_RESTAPI_HTTPLISTENMULTIADDRESS: /ip4/0.0.0.0/tcp/9094 # Expose API
      CLUSTER_PIN_ONLY_ON_TRUSTED_PEERS: 'false'
      CLUSTER_REPLICATIONFACTORMIN: 1
      CLUSTER_REPLICATIONFACTORMAX: ${TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_REPLICA_FACTOR_MAX}
    ports:
      - "9094:9094"
      - "9095:9095"
      - "${TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_SWARM_PORT}:9096"
    volumes:
      - ${TRILE_FEDERATION_CONTROLLER_CONFIGURATION_FOLDER}/ipfs-cluster:/data/ipfs-cluster

  trile_dev_federation_controller_ipfs:
    container_name: ${TRILE_IPFS_DOCKER_CONTAINER_NAME}
    image: triledotlink/federation-controller-ipfs:v0.27.0
    deploy:
      resources:
        limits:
          memory: 1024M
        reservations:
          memory: 1024M
    restart: unless-stopped
    environment:
      IPFS_LOGGING: INFO
      TRILE_FEDERATION_CONTROLLER_IPFS_ADDRESS: ${TRILE_FEDERATION_CONTROLLER_IPFS_ADDRESS}
      TRILE_FEDERATION_CONTROLLER_IPFS_MAX_DISK_SPACE_GB: ${TRILE_FEDERATION_CONTROLLER_IPFS_MAX_DISK_SPACE_GB}
    ports:
      - "${TRILE_FEDERATION_CONTROLLER_IPFS_SWARM_PORT}:4001"
      - "5001:5001"
      - "8080:8080"
    volumes:
      - ${TRILE_FEDERATION_CONTROLLER_CONFIGURATION_FOLDER}/ipfs:/data/ipfs

  trile_dev_federation_controller_redis_stack:
    image: redis/redis-stack:7.2.0-v9
    container_name: trile-dev-federation-controller-redis-stack
    restart: unless-stopped
    command: ["redis-stack-server", "--appendonly", "yes", "--protected-mode", "no", "--save", "20", "1", "--dir", "/data"]
    volumes:
      - ${TRILE_FEDERATION_CONTROLLER_CONFIGURATION_FOLDER}/redis-stack:/data
    ports:
      - "6379:6379"
    deploy:
      resources:
        limits:
          memory: 256M
        reservations:
          memory: 256M
        "#,
    )
}