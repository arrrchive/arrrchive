pub fn swarm_key() -> String {
    String::from(
        r#"/key/swarm/psk/1.0.0/
/base16/
${TRILE_FEDERATION_IPFS_SWARM_KEY_VALUE}
"#,
    )
}