use crate::program::commands::status_federation_member::{handle_status_federation_member_command, status_federation_member_command};
use clap::{ArgMatches, Command};

pub fn status_command() -> Command {
    Command::new("status")
        .about("How's it trile doing?")
        .subcommand(status_federation_member_command())
}

pub fn handle_status_command(matches: &ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
    if let Some(subcommand_matches) = matches.subcommand_matches("member") {
        handle_status_federation_member_command(subcommand_matches)?;
    }
    Ok(())
}