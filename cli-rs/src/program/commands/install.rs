use crate::program::commands::{
    install_federation_controller::install_federation_controller_command,
    install_federation_member::install_federation_member_command,
};
use clap::Command;

pub fn install_command() -> Command {
    Command::new("install")
        .about("Run the installation.")
        .subcommand(install_federation_member_command())
        .subcommand(install_federation_controller_command())
}