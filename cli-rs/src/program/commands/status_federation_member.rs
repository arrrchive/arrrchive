use crate::program::opts::{trile_opts_federation, trile_opts_federation_member};
use crate::services::command::status::status_command_handler::{run, run_watch};
use clap::{ArgMatches, Command};

pub fn status_federation_member_command() -> Command {
    Command::new("member")
        .about("How's the trile member doing?")
        .arg(trile_opts_federation::federation_name_opt())
        .arg(trile_opts_federation_member::nickname_opt())
        .arg(trile_opts_federation::watch_flag())
}

pub fn handle_status_federation_member_command(matches: &ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
    let federation_name: String = matches.get_one::<String>("federation-name").unwrap().to_string();
    let nickname: String = matches.get_one::<String>("nickname").unwrap().to_string();
    let watch: bool = *matches.get_one::<bool>("watch").unwrap();

    println!("{}", federation_name);
    println!("{}", nickname);
    println!("{}", watch);

    if watch {
        run_watch(federation_name.as_str(), nickname.as_str())?;
    } else {
        run(federation_name.as_str(), nickname.as_str())?;
    }

    Ok(())
}