use crate::dtos::input::federation_member_install::FederationMemberInstallInputVariables;
use crate::dtos::trile_cli::Environment;
use crate::program::opts::trile_opts_federation;
use crate::program::opts::trile_opts_federation_member;
use crate::services::command::install::member_command_handler::install_member_command_handler;
use crate::services::federation_controller_service::get_fedvars;
use crate::services::shell::confirm_and_run::confirm_and_run;
use crate::services::shell::greeter::greeter;
use clap::{ArgMatches, Command};
use std::io::{self, Write};
use crate::dtos::trile_cli::environment_variables::TRILE_FEDERATION_NAME;
use crate::services::shell::println::PrintLn;

pub fn install_federation_member_command() -> Command {
    Command::new("member")
        .about("Run the installation for a member.")
        .arg(trile_opts_federation::environment_opt())
        .arg(trile_opts_federation::assume_yes_flag())
        .arg(trile_opts_federation_member::nickname_opt())
        .arg(trile_opts_federation_member::shared_disk_space_gb_mandatory_opt())
        .arg(trile_opts_federation_member::sharing_folder_mandatory_opt())
        .arg(trile_opts_federation_member::federation_url())
        .arg(trile_opts_federation_member::join_token())
        .after_help("Run the installation for a Federation Member.")
}

pub fn handle_install_federation_member_command(matches: &ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
    let environment: Environment = matches.get_one::<Environment>("environment").unwrap().clone();
    let nickname: String = matches.get_one::<String>("nickname").unwrap().to_string();
    let shared_disk_space_gb_mandatory: Option<i32> = matches.get_one::<i32>("shared-disk-space-gb").copied();
    let sharing_folder_mandatory: Option<String> = matches.get_one::<String>("sharing-folder").map(|s| s.to_string());
    let federation_url: String = matches.get_one::<String>("federation-url").unwrap().to_string();
    let join_token: String = matches.get_one::<String>("join-token").unwrap().to_string();
    let assume_yes: bool = matches.contains_id("assume-yes");

    let fedvars_map = get_fedvars(environment.clone(), &federation_url, &join_token)?;

    greeter(&format!("join the '{}' federation", fedvars_map[TRILE_FEDERATION_NAME]));

    let shared_disk_space_gb = load_or_read_shared_disk_space_gb(shared_disk_space_gb_mandatory);
    let sharing_folder = load_or_read_sharing_folder(sharing_folder_mandatory);

    let input_variables = FederationMemberInstallInputVariables {
        environment,
        nickname: nickname.clone(),
        fedvars_map: fedvars_map.clone(),
        shared_disk_space_gb,
        sharing_folder: sharing_folder.clone(),
    };

    print_input_variables(&input_variables);

    Ok(confirm_and_run(assume_yes, || {
        let _ = install_member_command_handler(input_variables, fedvars_map);
    }))
        .inspect(|_| {
            println!();
            PrintLn::sub_step_ok("Member installation", true)
        })
        .inspect_err(|_| PrintLn::sub_step_error("Member installation", true))
}

fn print_input_variables(input_variables: &FederationMemberInstallInputVariables) {
    println!();
    println!("These are your input values. Please review them:");
    println!("- Environment: {:?}", input_variables.environment);
    println!("- Nickname: {}", input_variables.nickname);
    println!("- Shared disk space (GBs): {}", input_variables.shared_disk_space_gb);
    println!("- Shared folder: {}", input_variables.sharing_folder);
    println!("- .fedvars content: {:?}", input_variables.fedvars_map);
    println!();
}

fn load_or_read_sharing_folder(sharing_folder_mandatory: Option<String>) -> String {
    match sharing_folder_mandatory {
        Some(sharing_folder) => sharing_folder,
        None => read_sharing_folder(),
    }
}

fn read_sharing_folder() -> String {
    let prompt = "Sharing directory (absolute path): ";
    loop {
        println!();
        let mut sharing_folder = String::new();
        print!("{}", prompt);
        io::stdout().flush().unwrap();
        io::stdin().read_line(&mut sharing_folder).unwrap();
        let sharing_folder = sharing_folder.trim().to_string();
        if !sharing_folder.is_empty() {
            return sharing_folder;
        } else {
            PrintLn::yellow("Please provide an absolute path to your sharing folder.");
        }
    }
}

fn load_or_read_shared_disk_space_gb(shared_disk_space_gb_mandatory: Option<i32>) -> i32 {
    match shared_disk_space_gb_mandatory {
        Some(shared_disk_space_gb) => shared_disk_space_gb,
        None => read_shared_disk_space_gb(),
    }
}

fn read_shared_disk_space_gb() -> i32 {
    let prompt = "How much disk space do you want to let the cluster use? (GBs): ";
    loop {
        println!();
        let mut shared_disk_space_gb = String::new();
        print!("{}", prompt);
        io::stdout().flush().unwrap();
        io::stdin().read_line(&mut shared_disk_space_gb).unwrap();
        match shared_disk_space_gb.trim().parse() {
            Ok(value) => return value,
            Err(_) => PrintLn::yellow("Please provide a valid value (GBs)."),
        }
    }
}