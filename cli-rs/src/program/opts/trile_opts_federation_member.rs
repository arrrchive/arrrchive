use crate::dtos::trile_cli::defaults;
use clap::Arg;
use lazy_static::lazy_static;
use std::ffi::OsString;

lazy_static! {
    static ref NICKNAME_DEFAULT: OsString = OsString::from(defaults::nickname());
}

pub fn nickname_opt() -> Arg {
    Arg::new("nickname")
        .long("nickname")
        .short('n')
        .default_value_os(&**NICKNAME_DEFAULT)
        .help(&format!(
            "The name you want to use in the federation (default={})",
            &defaults::nickname()
        ))
}

pub fn shared_disk_space_gb_mandatory_opt() -> Arg {
    Arg::new("shared-disk-space-gb")
        .long("shared-disk-space-gb")
        .value_parser(clap::value_parser!(i32))
        .help("Amount of GBs the federation will use from your machine for replication purposes")
}

pub fn sharing_folder_mandatory_opt() -> Arg {
    Arg::new("sharing-folder")
        .long("sharing-folder")
        .help("Location of your sharing folder")
}

pub fn join_token() -> Arg {
    Arg::new("join-token")
        .long("join-token")
        .short('j')
        .help("The join token the federation controller shall provide")
}

pub fn federation_url() -> Arg {
    Arg::new("federation-url")
        .long("federation-url")
        .help("URL of the federation. For instance: demo.trile.link")
}