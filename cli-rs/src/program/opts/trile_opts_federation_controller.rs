use crate::dtos::trile_cli::defaults;
use clap::Arg;
use lazy_static::lazy_static;
use std::ffi::OsString;
use std::net::IpAddr;

lazy_static! {
    static ref IPFS_CLUSTER_REPLICA_FACTOR_MAX: OsString = OsString::from(defaults::IPFS_CLUSTER_REPLICA_FACTOR_MAX.to_string());
}

pub fn network_address_opt() -> Arg {
    Arg::new("network-address")
        .long("network-address")
        .required(true)
        .value_parser(clap::value_parser!(IpAddr))
        .default_value("127.0.0.1")
        // .value_parser(|v| v.parse::<IpAddr>().map(|_| ()).map_err(|_| String::from("Invalid IP address")))
        .help("Hostname where you are hosting the federation. Required for SSL certificates.")
}

pub fn ipfs_cluster_replica_factor_max_opt() -> Arg {
    Arg::new("ipfs-cluster-replica-factor-max")
        .long("ipfs-cluster-replica-factor-max")
        .value_parser(clap::value_parser!(i32))
        .default_value_os(&**IPFS_CLUSTER_REPLICA_FACTOR_MAX)
        .help("At most, how many soft copies should be kept in the federation.")
}

pub fn ssl_staging_flag() -> Arg {
    Arg::new("ssl-staging")
        .long("ssl-staging")
        .help("If set, SSL certificates will be generated for pre-production cases.")
}

pub fn certbot_folder_absolute_path() -> Arg {
    Arg::new("certbot-folder")
        .long("certbot-folder")
        .help("Define where your (let's encrypt) SSL certificates are located. If no value is given, trile will generate them for you.")
}

pub fn shared_disk_space_gb_opt() -> Arg {
    Arg::new("shared-disk-space-gb")
        .long("shared-disk-space-gb")
        .value_parser(clap::value_parser!(i32))
        .help(&format!(
            "Amount of GBs the federation will use from your machine for replication purposes (default={})",
            defaults::SHARED_DISK_SPACE_GB,
        ))
}