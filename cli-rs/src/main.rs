mod services;
mod program;
mod dtos;
mod files;

use clap::{ArgMatches, Command};
use program::commands::{install_federation_controller, status, update_federation};
use crate::program::commands::{install, install_federation_member, update, update_federation_controller, update_federation_member};

fn main() {
    let matches: ArgMatches = Command::new("trile")
        .about("Trile CLI. Complete docs version can be found at https://trile.link")
        .subcommand(install::install_command())
        .subcommand(update::update_command())
        .subcommand(status::status_command())
        .get_matches();

    match matches.subcommand() {
        Some(("install", sub_m)) => { handle_install_command(sub_m).unwrap();}
        Some(("update", sub_m)) => { let _ = handle_update_command(sub_m); }
        Some(("status", sub_m)) => { let _ = handle_status_command(sub_m);}
        _ => eprintln!("No valid subcommand was provided."),
    };
}

fn handle_install_command(matches: &ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
    match matches.subcommand() {
        Some(("controller", sub_m)) => {
            install_federation_controller::handle_install_federation_controller_command(sub_m)
        },
        Some(("member", sub_m)) => {
            install_federation_member::handle_install_federation_member_command(sub_m)
        },
        _ => {
            eprintln!("No valid subcommand was provided.");
            Ok(())
        },
    }
}

fn handle_update_command(matches: &ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
    match matches.subcommand() {
        Some(("controller", sub_m)) => {
            update_federation_controller::handle_update_federation_controller_command(sub_m)
        },
        Some(("member", sub_m)) => {
            update_federation_member::handle_update_federation_member_command(sub_m)
        },
        None => {
            update_federation::handle_update_federation_command(matches)
        }
        _ => {
            eprintln!("No valid subcommand was provided.");
            Ok(())
        },
    }
}

fn handle_status_command(matches: &ArgMatches) -> Result<(), Box<dyn std::error::Error>> {
    status::handle_status_command(matches)
}