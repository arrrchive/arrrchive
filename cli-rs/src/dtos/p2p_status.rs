use serde::{Deserialize, Serialize};
use std::fmt;
use thiserror::Error;

#[derive(Serialize, Deserialize, Debug, PartialEq)]
#[serde(rename_all = "PascalCase")]
pub enum SyncStatus {
    Syncing,
    Sync,
    OutOfSync,
}

impl fmt::Display for SyncStatus {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            SyncStatus::Sync => write!(f, "Synced"),
            SyncStatus::Syncing => write!(f, "Syncing..."),
            SyncStatus::OutOfSync => write!(f, "Out of sync message"),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum CheckInStatus {
    Unknown,
    Ack,
    Nack,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
#[serde(rename_all = "PascalCase")]
pub enum ControllerStatus {
    Unknown,
    Online,
    Offline,
}

// NackException struct
#[derive(Debug, Error)]
#[error("NACK exception")]
pub struct NackException;