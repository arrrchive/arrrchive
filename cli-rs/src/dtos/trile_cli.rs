use serde::Deserialize;
use std::fmt;
use std::str::FromStr;

// Convert Scala enums to Rust enums
#[derive(Deserialize, Debug, Clone)]
pub enum Environment {
    Dev,
    Production,
}

impl FromStr for Environment {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "dev" => Ok(Environment::Dev),
            "production" => Ok(Environment::Production),
            _ => Err("Unknown environment"),
        }
    }
}

impl fmt::Display for Environment {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Environment::Dev => write!(f, "dev"),
            Environment::Production => write!(f, "production"),
        }
    }
}

#[derive(Deserialize, Debug, Clone)]
pub enum File {
    SwarmKey,
    DockerCompose,
    NginxConf,
    NginxPreSslConf,
    SslCertificate,
}

#[derive(Deserialize, Debug, Clone)]
pub enum Component {
    FederationMember,
    FederationController,
}

// EnvironmentVariables module in Rust
pub mod environment_variables {
    use std::collections::HashMap;

    pub type TrileEnvironmentVariable = String;
    pub type TrileFederationEnvVars = HashMap<TrileEnvironmentVariable, String>;

    pub const DOLLAR: &str = "DOLLAR";
    pub const HOST_GID: &str = "HOST_GID";
    pub const HOST_UID: &str = "HOST_UID";
    pub const TRILE_ENVIRONMENT: &str = "TRILE_ENVIRONMENT";
    pub const TRILE_CONFIGURATION_FOLDER: &str = "TRILE_CONFIGURATION_FOLDER";
    pub const TRILE_FEDERATION_NAME: &str = "TRILE_FEDERATION_NAME";
    pub const TRILE_FEDERATION_IPFS_SWARM_KEY_VALUE: &str = "TRILE_FEDERATION_IPFS_SWARM_KEY_VALUE";
    pub const TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_DATA_DIRECTORY: &str =
        "TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_DATA_DIRECTORY";
    pub const TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_NAME: &str =
        "TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_NAME";
    pub const TRILE_IPFS_DOCKER_CONTAINER_DATA_CERTBOT_DIRECTORY: &str =
        "TRILE_IPFS_DOCKER_CONTAINER_DATA_CERTBOT_DIRECTORY";
    pub const TRILE_IPFS_DOCKER_CONTAINER_DATA_DIRECTORY: &str =
        "TRILE_IPFS_DOCKER_CONTAINER_DATA_DIRECTORY";
    pub const TRILE_IPFS_DOCKER_CONTAINER_NAME: &str = "TRILE_IPFS_DOCKER_CONTAINER_NAME";
    pub const TRILE_BACKEND_DOCKER_CONTAINER_NAME: &str = "TRILE_BACKEND_DOCKER_CONTAINER_NAME";
    pub const TRILE_FRONTEND_DOCKER_CONTAINER_NAME: &str = "TRILE_FRONTEND_DOCKER_CONTAINER_NAME";
    pub const TFC_CERTBOT_CONTAINER_NAME: &str = "TRILE_FEDERATION_CONTROLLER_CERTBOT_CONTAINER_NAME";
    pub const TFC_CERTBOT_SERVICE_NAME: &str = "TRILE_FEDERATION_CONTROLLER_CERTBOT_SERVICE_NAME";
    pub const TFC_CERTBOT_STAGING: &str = "TRILE_FEDERATION_CONTROLLER_CERTBOT_STAGING";
    pub const TFC_CONFIGURATION_FOLDER: &str = "TRILE_FEDERATION_CONTROLLER_CONFIGURATION_FOLDER";
    pub const TFC_FEDVARS_ABSOLUTE_PATH: &str = "TRILE_FEDERATION_CONTROLLER_FEDVARS_ABSOLUTE_PATH";
    pub const TFC_JOIN_TOKEN: &str = "TRILE_FEDERATION_CONTROLLER_JOIN_TOKEN";
    pub const TFC_IPFS_ADDRESS: &str = "TRILE_FEDERATION_CONTROLLER_IPFS_ADDRESS";
    pub const TFC_IPFS_CLUSTER_ADDRESS: &str = "TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_ADDRESS";
    pub const TFC_IPFS_CLUSTER_PEER_ID: &str = "TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_PEER_ID";
    pub const TFC_IPFS_CLUSTER_SWARM_PORT: &str = "TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_SWARM_PORT";
    pub const TFC_IPFS_CLUSTER_SECRET: &str = "TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_SECRET";
    pub const TFC_IPFS_CLUSTER_REPLICA_FACTOR_MAX: &str =
        "TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_REPLICA_FACTOR_MAX";
    pub const TFC_IPFS_MAX_DISK_SPACE_GB: &str = "TRILE_FEDERATION_CONTROLLER_IPFS_MAX_DISK_SPACE_GB";
    pub const TFC_IPFS_PEER_ID: &str = "TRILE_FEDERATION_CONTROLLER_IPFS_PEER_ID";
    pub const TFC_IPFS_SWARM_PORT: &str = "TRILE_FEDERATION_CONTROLLER_IPFS_SWARM_PORT";
    pub const TFC_NETWORK_ADDRESS: &str = "TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS";
    pub const TFC_P2P_PRIVATE_KEY: &str = "TRILE_FEDERATION_CONTROLLER_P2P_PRIVATE_KEY";
    pub const TFC_P2P_PEER_ID: &str = "TRILE_FEDERATION_CONTROLLER_P2P_PEER_ID";
    pub const TFC_P2P_PORT: &str = "TRILE_FEDERATION_CONTROLLER_P2P_PORT";
    pub const TFC_REVERSE_PROXY_CONFIGURATION: &str =
        "TRILE_FEDERATION_CONTROLLER_REVERSE_PROXY_CONFIGURATION";
    pub const TFC_REVERSE_PROXY_CONTAINER_NAME: &str =
        "TRILE_FEDERATION_CONTROLLER_REVERSE_PROXY_CONTAINER_NAME";
    pub const TFC_REVERSE_PROXY_SERVICE_NAME: &str = "TRILE_FEDERATION_CONTROLLER_REVERSE_PROXY_SERVICE_NAME";
    pub const TFC_BACKEND_SERVICE_NAME: &str = "TRILE_FEDERATION_CONTROLLER_BACKEND_SERVICE_NAME";
    pub const TFM_CONFIGURATION_FOLDER: &str = "TRILE_FEDERATION_MEMBER_CONFIGURATION_FOLDER";
    pub const TFM_IPFS_CLUSTER_NAME: &str = "TRILE_FEDERATION_MEMBER_IPFS_CLUSTER_NAME";
    pub const TFM_MAX_DISK_SPACE_GB: &str = "TRILE_FEDERATION_MEMBER_MAX_DISK_SPACE_GB";
    pub const TFM_NICKNAME: &str = "TRILE_FEDERATION_MEMBER_NICKNAME";
    pub const TFM_P2P_PRIVATE_KEY: &str = "TRILE_FEDERATION_MEMBER_P2P_PRIVATE_KEY";
    pub const TFM_SHARING_FOLDER: &str = "TRILE_FEDERATION_MEMBER_SHARING_FOLDER";
    pub const TFM_BACKEND_SERVICE_NAME: &str = "TRILE_FEDERATION_MEMBER_BACKEND_SERVICE_NAME";
}

pub mod defaults {
    use std::env;

    pub const IPFS_SWARM_PORT: &str = "4001";
    pub const IPFS_CLUSTER_SWARM_PORT: &str = "9096";
    pub const SHARED_DISK_SPACE_GB: i32 = 10;
    pub const ENVIRONMENT: &str = "production";
    pub const FEDERATION_MEMBER_BACKEND_PRIVATE_PORT: i32 = 9999;
    pub const IPFS_CLUSTER_REPLICA_FACTOR_MAX: i32 = 5;

    pub fn user_home_absolute_path() -> String {
        env::var("HOME").expect("Failed to get HOME directory")
    }

    pub fn nickname() -> String {
        let home_path: String = user_home_absolute_path();
        home_path.split('/').last().unwrap_or("unknown").strip_suffix(".trile").unwrap_or("unknown").to_string()
    }

    pub fn trile_configuration_absolute_path() -> String {
        format!("{}/.config/trile", user_home_absolute_path())
    }
}