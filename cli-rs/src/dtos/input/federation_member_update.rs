use serde::Deserialize;

#[derive(Deserialize, Debug, Clone)]
pub struct FederationMemberUpdateInputVariables {
    pub federation_name: String,
    pub nickname: String,
}