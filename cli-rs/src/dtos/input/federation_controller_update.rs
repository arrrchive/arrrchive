use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct FederationControllerUpdateInputVariables {
    pub federation_name: String,
}