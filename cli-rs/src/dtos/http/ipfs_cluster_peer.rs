use crate::dtos::aliases::{IpfsCid, IpfsPeerId};
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct IpfsClusterPins {
    pub pins: Vec<IpfsClusterPin>,
}

#[derive(Deserialize, Debug)]
pub struct IpfsClusterPin {
    pub cid: IpfsCid,
}

#[derive(Deserialize, Debug)]
pub struct IpfsClusterPeers {
    pub peers: Vec<IpfsClusterPeer>,
}

#[derive(Deserialize, Debug)]
pub struct IpfsClusterPeer {
    pub id: String,
    pub addresses: Vec<String>,
    #[serde(rename = "clusterPeers")]
    pub cluster_peers: Vec<String>,
    #[serde(rename = "clusterPeersAddresses")]
    pub cluster_peers_addresses: Vec<String>,
    pub version: String,
    pub commit: String,
    #[serde(rename = "rpcProtocolVersion")]
    pub rpc_protocol_version: String,
    pub error: String,
    pub ipfs: IpfsClusterPeerIpfsInfo,
    pub peername: String,
}

#[derive(Deserialize, Debug)]
pub struct IpfsClusterPeerIpfsInfo {
    pub id: IpfsPeerId,
    pub addresses: Vec<String>,
    pub error: String,
}