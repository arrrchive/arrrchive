use crate::dtos::http::ipfs_cluster_peer::IpfsClusterPeers;
use crate::dtos::p2p_status::{CheckInStatus, ControllerStatus, SyncStatus};
use chrono::{DateTime, Utc};
use serde::Deserialize;

#[derive(Deserialize)]
pub struct FederationMemberStatus {

    pub nickname: String,
    #[serde(rename = "p2pPeerId")]
    pub p2p_peer_id: String,
    #[serde(rename = "sharedFolderPin")]
    pub shared_folder_pin: String,
    #[serde(rename = "sharedFolderFileCount")]
    pub shared_folder_file_count: i32,
    #[serde(rename = "sharedFolderSizeBytes")]
    pub shared_folder_size_bytes: i64,
    #[serde(rename = "checkInStatus")]
    pub check_in_status: CheckInStatus,
    #[serde(rename = "syncStatus")]
    pub sync_status: SyncStatus,
    #[serde(rename = "ipfsClusterPeers")]
    pub ipfs_cluster_peers: IpfsClusterPeers,
    #[serde(rename = "ipfsClusterPinsCount")]
    pub ipfs_cluster_pins_count: i32,
    #[serde(rename = "ipfsRepoSize")]
    pub ipfs_repo_size: i64,
    #[serde(rename = "ipfsStorageMax")]
    pub ipfs_storage_max: i64,
    #[serde(rename = "controllerStatus")]
    pub controller_status: ControllerStatus,
    #[serde(rename = "controllerLastSeen")]
    pub controller_last_seen: Option<DateTime<Utc>>,
}
