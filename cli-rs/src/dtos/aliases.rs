// Federation member types
pub type FederationMemberNickname = String;
pub type FederationMemberId = String;

// IPFS types
pub type IpfsCid = String;
pub type IpfsClusterPeerId = String;
pub type IpfsClusterNodeName = String;
pub type IpfsPeerId = String;

// P2P types
pub type P2pPeerId = String;
pub type P2pMessageKey = String;
pub type P2pKey = String;