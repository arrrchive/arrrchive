#!/bin/sh
set -ex

ipfs bootstrap rm all
ipfs config --json Bootstrap "[]"
ipfs config --json Experimental.FilestoreEnabled  true
ipfs config --json Experimental.GraphsyncEnabled  false
ipfs config --json Experimental.OptimisticProvide  false
ipfs config --json Gateway.Writable  false
ipfs config --json Ipns.UsePubsub true
ipfs config --json Peering.Peers "[{\"ID\": \"${TRILE_FEDERATION_CONTROLLER_IPFS_PEER_ID}\", \"Addrs\":[\"${TRILE_FEDERATION_CONTROLLER_IPFS_ADDRESS}\"]}]"
ipfs config --json PubSub.Enabled true
ipfs config --json Routing.AcceleratedDHTClient false
ipfs config Routing.Type dhtclient
ipfs config Datastore.StorageMax ${TRILE_FEDERATION_MEMBER_MAX_DISK_SPACE_GB}GB
ipfs config Swarm.ConnMgr.GracePeriod '1m'
ipfs config Swarm.ConnMgr.LowWater --json 20
ipfs config Swarm.ConnMgr.HighWater --json 40

ipfs config show
echo "Cleaning up the repo"
ipfs repo gc
echo "Adding /data/staging to IPFS..."
ipfs add --pin=true --recursive --nocopy --cid-version 1 /data/staging

