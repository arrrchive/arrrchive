object DependencyVersions {
  val directoryWatcher     = "0.18.0"
  val docker               = "3.2.14"
  val http4s               = "0.23.16"
  val logback              = "1.4.5"
  val redis                = "3.41"
  val sttp                 = "3.8.5"
  val typeSafeConfig       = "1.4.2"
  val typeSafeScalaLogging = "3.9.5"
}
