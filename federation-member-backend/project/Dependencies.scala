import sbt._

// Dependencies
// Alphabetical
object Dependencies {
  val directoryWatcher = "io.methvin"             % "directory-watcher"            % DependencyVersions.directoryWatcher
  val typeSafeConfig   = "com.typesafe"           % "config"                       % DependencyVersions.typeSafeConfig
  val docker           = "com.github.docker-java" % "docker-java-core"             % DependencyVersions.docker
  val dockerTransport  = "com.github.docker-java" % "docker-java-transport-okhttp" % DependencyVersions.docker
  val http4sEmber      = "org.http4s"            %% "http4s-ember-server"          % DependencyVersions.http4s
  val http4sDsl        = "org.http4s"            %% "http4s-dsl"                   % DependencyVersions.http4s
  val http4sCirce      = "org.http4s"            %% "http4s-circe"                 % DependencyVersions.http4s
  val libP2p           = "io.libp2p"              % "jvm-libp2p"                   % "1.1.0-RELEASE"
  val logback          = "ch.qos.logback"         % "logback-classic"              % DependencyVersions.logback
  val sttp                = "com.softwaremill.sttp.client3" %% "core"                  % DependencyVersions.sttp
  val trileBackendCommons = "acab.devcon0"                  %% "trile-backend-commons" % "0.1.0-SNAPSHOT"
  val typeSafeScalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % DependencyVersions.typeSafeScalaLogging
}
