import sbt._

object ProjectResolvers {

  val libP2pRepo = "libp2p JVM repository" at "https://dl.cloudsmith.io/public/libp2p/jvm-libp2p/maven/"
  val consenSys  = "ConsenSys Maven repository" at "https://artifacts.consensys.net/public/maven/maven/"
  val jitpack    = "jitpack" at "https://jitpack.io"
}
