package acab.devcon0.trile.configuration

import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import io.libp2p.core.pubsub.Topic

final case class P2pTopicsConfiguration(
    sync: Topic,
    nickname: Topic,
    sharingFolderUpdate: Topic,
    checkIn: Topic,
    heartbeat: Topic
)
final case class P2pConfiguration(
    controllerPeerId: String,
    controllerNetworkAddress: String,
    controllerPort: String,
    swarmKeyValue: String,
    privateKey: String,
    topics: P2pTopicsConfiguration
)
final case class IpfsConfiguration(
    apiUrl: String,
    sharedFolderIpfsPov: String,
    dockerContainerName: String
)
final case class HttpConfiguration(port: Integer)
final case class IpfsClusterConfiguration(apiUrl: String)
final case class Configuration(
    ipfs: IpfsConfiguration,
    ipfsCluster: IpfsClusterConfiguration,
    http: HttpConfiguration,
    sharedFolder: String,
    federationMemberNickname: String,
    p2pConfiguration: P2pConfiguration
)

object TrileFederationMemberBackendConfigFactory {

  private val rawConfig: Config            = ConfigFactory.load("application.conf")
  private val conf: Config                 = rawConfig.getConfig("app")
  private val ipfsConfig: Config           = conf.getConfig("ipfs")
  private val ipfsClusterConfig: Config    = conf.getConfig("ipfs-cluster")
  private val p2pFileConfiguration: Config = conf.getConfig("p2p")
  private val httpConfig: Config           = conf.getConfig("http")

  def build(): Configuration = {
    val httpConfiguration: HttpConfiguration = HttpConfiguration(
      port = httpConfig.getInt("port")
    )

    val ipfsConfiguration: IpfsConfiguration = IpfsConfiguration(
      apiUrl = ipfsConfig.getString("api-url"),
      sharedFolderIpfsPov = ipfsConfig.getString("shared-folder-ipfs-pov"),
      dockerContainerName = ipfsConfig.getString("docker-container-name")
    )
    val ipfsClusterConfiguration: IpfsClusterConfiguration = IpfsClusterConfiguration(
      apiUrl = ipfsClusterConfig.getString("api-url")
    )
    val swarmKeyValue = p2pFileConfiguration.getString("swarm-key-value")
    val p2pConfiguration: P2pConfiguration = P2pConfiguration(
      controllerPeerId = p2pFileConfiguration.getString("controller-peer-id"),
      controllerNetworkAddress = p2pFileConfiguration.getString("controller-network-address"),
      controllerPort = p2pFileConfiguration.getString("controller-port"),
      swarmKeyValue = swarmKeyValue,
      privateKey = p2pFileConfiguration.getString("private-key"),
      topics = P2pTopicsConfiguration(
        sync = TopicBuilder("FEDERATION_MEMBER_SYNC", swarmKeyValue),
        nickname = TopicBuilder("FEDERATION_MEMBER_NICKNAME", swarmKeyValue),
        sharingFolderUpdate = TopicBuilder("FEDERATION_MEMBER_SHARING_FOLDER_UPDATE", swarmKeyValue),
        checkIn = TopicBuilder("FEDERATION_MEMBER_CHECKIN", swarmKeyValue),
        heartbeat = TopicBuilder("FEDERATION_MEMBER_HEARTBEAT", swarmKeyValue)
      )
    )
    Configuration(
      http = httpConfiguration,
      ipfs = ipfsConfiguration,
      ipfsCluster = ipfsClusterConfiguration,
      sharedFolder = conf.getString("shared-folder"),
      federationMemberNickname = conf.getString("federation-member-nickname"),
      p2pConfiguration = p2pConfiguration
    )
  }
}
