package acab.devcon0.trile.configuration

import cats.effect.IO
import io.circe._
import io.circe.generic.semiauto.deriveEncoder
import io.circe.syntax.EncoderOps
import io.libp2p.core.pubsub.Topic

object CirceCodecs {
  object Encoders {
    import acab.devcon0.trile.domain.codecs.P2pCodecs.Encoders.topic

    implicit val p2pTopicsConfiguration: Encoder[P2pTopicsConfiguration] = deriveEncoder[P2pTopicsConfiguration]
    implicit val p2pConfiguration: Encoder[P2pConfiguration]             = deriveEncoder[P2pConfiguration]
    implicit val ipfsConfiguration: Encoder[IpfsConfiguration]           = deriveEncoder[IpfsConfiguration]
    implicit val configuration: Encoder[Configuration]                   = deriveEncoder[Configuration]

    object Configuration {
      def apply(dto: Configuration): IO[String] = IO(
        EncoderOps[Configuration](dto).asJson.toString
      )
    }
  }
}
