package acab.devcon0.trile.output

import acab.devcon0.trile.configuration.IpfsConfiguration
import acab.devcon0.trile.domain.codecs.IpfsCodecs
import acab.devcon0.trile.domain.dtos.IpfsAddResponse
import acab.devcon0.trile.domain.dtos.IpfsPeerRepoStat
import acab.devcon0.trile.domain.ports.output.IpfsClient
import cats.effect.IO
import com.typesafe.scalalogging.LazyLogging
import sttp.client3._
import sttp.model.Uri

class IpfsClientImpl(sttpBackend: SttpBackend[Identity, Any], ipfsConfiguration: IpfsConfiguration)
    extends IpfsClient
    with DockerClientWrapper
    with LazyLogging {

  private val apiUrl: String = ipfsConfiguration.apiUrl

  /** Tip: the Abspath header is given to the multipart, not to the root request. */
  override def addWithPinAndNoCopy(path: String): IO[IpfsAddResponse] = {
    addWithPinAndNoCopyInDocker(path)
      .map(result => IpfsAddResponse(result.head))
      .flatTap(result => IO(logger.debug(s"addWithPinAndNoCopy.result=$result")))
      .onError(logError)
  }

  override def repoGc: IO[Unit] = {
    repoGcRequest
      .flatTap(result => IO(logger.debug(s"repoGC.result=$result")))
      .onError(logError)
      .void
  }

  override def getRepoStat: IO[IpfsPeerRepoStat] = {
    repoStatRequest
      .flatTap(result => IO(logger.debug(s"repoStat.result=$result")))
      .flatMap(response => IpfsCodecs.Decoders.IpfsPeerRepoStat(response.body.getOrElse("")))
      .onError(logError)
  }

  private def addWithPinAndNoCopyInDocker(path: String): IO[List[String]] = {
    runExecAgainstDocker(
      ipfsConfiguration.dockerContainerName,
      "ipfs",
      "add",
      "--quieter",
      "--pin=true",
      "--recursive",
      "--nocopy",
      "--cid-version",
      "1",
      path
    )
  }

  private def repoGcRequest: IO[Identity[Response[Either[String, String]]]] = IO {
    val uri: Uri = uri"$apiUrl/repo/gc"
    basicRequest
      .post(uri = uri)
      .send(sttpBackend)
  }

  private def repoStatRequest: IO[Identity[Response[Either[String, String]]]] = IO {
    val uri: Uri = uri"$apiUrl/repo/stat"
    basicRequest
      .post(uri = uri)
      .send(sttpBackend)
  }

  private def logError(throwable: Throwable): IO[Unit] = IO {
    logger.error(s"throwable=$throwable")
  }
}
