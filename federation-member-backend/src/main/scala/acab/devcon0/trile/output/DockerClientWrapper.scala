package acab.devcon0.trile.output

import java.io.PipedInputStream
import java.io.PipedOutputStream
import java.lang

import acab.devcon0.trile.domain.dtos.DockerCommandExecutionException
import cats.effect.IO
import com.github.dockerjava.api.DockerClient
import com.github.dockerjava.api.async.ResultCallback
import com.github.dockerjava.api.command.ExecCreateCmdResponse
import com.github.dockerjava.api.model.Frame
import com.github.dockerjava.core.DefaultDockerClientConfig
import com.github.dockerjava.core.DockerClientConfig
import com.github.dockerjava.core.DockerClientImpl
import com.github.dockerjava.okhttp.OkDockerHttpClient
import com.github.dockerjava.transport.DockerHttpClient
import com.typesafe.scalalogging.LazyLogging

trait DockerClientWrapper extends LazyLogging {

  private val dockerSocket = "unix:///var/run/docker.sock"

  def runExecAgainstDocker(container: String, cmds: String*): IO[List[String]] = {
    runExecAgainstDockerInner(container, cmds*)
      .flatMap((exitCode, output) => {
        exitCode match {
          case 0 => IO.pure(output)
          case _ => IO.raiseError(DockerCommandExecutionException(exitCode))
        }
      })
  }

  private def runExecAgainstDockerInner(container: String, cmds: String*): IO[(Long, List[String])] = {
    IO {
      val dockerClient: DockerClient = getDockerClient

      val cmd: ExecCreateCmdResponse = dockerClient
        .execCreateCmd(container)
        .withCmd(cmds*)
        .withAttachStdout(true)
        .withAttachStderr(false)
        .exec()

      val outputStream = new PipedOutputStream
      new PipedInputStream(outputStream)

      val sb = new StringBuilder
      dockerClient
        .execStartCmd(cmd.getId)
        .exec(new ResultCallback.Adapter[Frame] {
          override def onNext(frame: Frame): Unit = {
            val payload = new String(frame.getPayload)
            logger.debug(s"Docker exec cmd output => $payload")
            sb.append(payload)
          }
        })
        .awaitCompletion()
      val exitCode: Long = dockerClient.inspectExecCmd(cmd.getId).exec.getExitCodeLong
      val output: List[String] = sb
        .toString()
        .split("\\n")
        .toList
        .filterNot(str => str.isBlank)
      (exitCode, output)
    }
  }

  private def getDockerClient: DockerClient = {
    val dockerClientConfig: DockerClientConfig = getDefaultDockerClientConfig
    val httpClient: DockerHttpClient           = getDockerHttpClientConfig(dockerClientConfig)
    val dockerClient: DockerClient             = DockerClientImpl.getInstance(dockerClientConfig, httpClient)
    dockerClient
  }

  private def getDockerHttpClientConfig(dockerClientConfig: DockerClientConfig): OkDockerHttpClient = {
    new OkDockerHttpClient.Builder()
      .dockerHost(dockerClientConfig.getDockerHost)
      .sslConfig(dockerClientConfig.getSSLConfig)
      .dockerHost(dockerClientConfig.getDockerHost)
      .build()
  }

  private def getDefaultDockerClientConfig: DefaultDockerClientConfig = {
    DefaultDockerClientConfig
      .createDefaultConfigBuilder()
      .withDockerHost(dockerSocket)
      .build()
  }
}
