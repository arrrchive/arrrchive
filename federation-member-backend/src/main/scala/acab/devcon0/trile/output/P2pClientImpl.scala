package acab.devcon0.trile.output

import acab.devcon0.trile.configuration.P2pConfiguration
import acab.devcon0.trile.domain.codecs.P2pCodecs
import acab.devcon0.trile.domain.codecs.P2pCodecs.Encoders._
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import acab.devcon0.trile.domain.dtos.pubsub.P2p
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberCheckIn
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberHeartbeat
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberSharingFolderUpdate
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberSync
import acab.devcon0.trile.domain.dtos.pubsub.P2p._
import acab.devcon0.trile.domain.ports.output.P2pClient
import acab.devcon0.trile.inputoutput.P2pBackendImpl
import cats.effect._

class P2pClientImpl(p2pBackend: P2pBackendImpl, p2pConfiguration: P2pConfiguration) extends P2pClient[IO] {

  private val p2pPeerId: String = p2pBackend.getHostPeerId.toBase58

  override def connect: IO[Unit] = {
    p2pBackend.init
  }

  override def getPeerId: IO[P2pPeerId] = {
    IO(p2pPeerId)
  }

  override def publish(data: FederationMemberCheckIn): IO[Unit] = {
    val metadata: Metadata = Metadata(MessageType.Request, p2pPeerId, Some(p2pConfiguration.controllerPeerId))
    val topic              = p2pConfiguration.topics.checkIn
    p2pBackend.publishMessage(data, topic, metadata)
  }

  override def publish(data: FederationMemberSync): IO[Unit] = {
    val metadata: Metadata = Metadata(MessageType.Request, p2pPeerId, Some(p2pConfiguration.controllerPeerId))
    val topic              = p2pConfiguration.topics.sync
    p2pBackend.publishMessage(data, topic, metadata)
  }

  override def publish(data: FederationMemberHeartbeat): IO[Unit] = {
    val metadata: Metadata = Metadata(MessageType.Event, p2pPeerId, None)
    val topic              = p2pConfiguration.topics.heartbeat
    p2pBackend.publishMessage(data, topic, metadata)
  }

  override def publish(data: FederationMemberSharingFolderUpdate): IO[Unit] = {
    val metadata: Metadata = Metadata(MessageType.Event, p2pPeerId, None)
    val topic              = p2pConfiguration.topics.sharingFolderUpdate
    p2pBackend.publishMessage(data, topic, metadata)
  }
}
