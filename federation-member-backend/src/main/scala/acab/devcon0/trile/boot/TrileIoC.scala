package acab.devcon0.trile.boot

import acab.devcon0.trile.configuration._
import acab.devcon0.trile.domain.adapters._
import acab.devcon0.trile.domain.ports.input.HeartbeatCommandHandler
import acab.devcon0.trile.domain.ports.input._
import acab.devcon0.trile.domain.ports.output.IpfsClient
import acab.devcon0.trile.domain.ports.output.IpfsClusterClient
import acab.devcon0.trile.domain.ports.output.P2pClient
import acab.devcon0.trile.domain.service._
import acab.devcon0.trile.input._
import acab.devcon0.trile.input.fs.SharedFolderWatcher
import acab.devcon0.trile.input.http.FederationMemberStatusRoute
import acab.devcon0.trile.input.http.Routes
import acab.devcon0.trile.input.p2p._
import acab.devcon0.trile.inputoutput.P2pBackendImpl
import acab.devcon0.trile.output.IpfsClientImpl
import acab.devcon0.trile.output.IpfsClusterClientImpl
import acab.devcon0.trile.output.P2pClientImpl
import cats.effect.IO
import sttp.client3.HttpURLConnectionBackend

object TrileIoC {
  object Configuration {
    val configuration: Configuration = TrileFederationMemberBackendConfigFactory.build()
  }

  object InputOutput {
    val p2pBackend: P2pBackendImpl = new P2pBackendImpl(Configuration.configuration.p2pConfiguration)
  }

  object Input {

    val statusRoute: FederationMemberStatusRoute = new FederationMemberStatusRoute(
      queryHandler = Domain.Port.federationMemberStatusQueryHandler
    )
    val routes: Routes = new Routes(
      statusRoute = statusRoute
    )

    val sharedFolderWatcher: SharedFolderWatcher = new SharedFolderWatcher(
      path = TrileIoC.Configuration.configuration.sharedFolder,
      advertiseSharedFolderUpdatesCommandHandler = Domain.Port.advertiseSharedFolderUpdatesCommandHandler,
      p2pService = Domain.Service.p2pService
    )

    val checkInAdvertiser: CheckInAdvertiser = new CheckInAdvertiser(
      checkInCommandHandler = Domain.Port.checkInCommandHandler,
      checkInAckQueryHandler = Domain.Port.checkInAckQueryHandler
    )

    val syncAdvertiser: SyncAdvertiser = new SyncAdvertiser(
      forceSyncCommandHandler = Domain.Port.forceSyncCommandHandler,
      syncStatusQueryHandler = Domain.Port.syncStatusQueryHandler
    )

    val p2pListener: P2pListener = new P2pListener(
      p2pBackend = InputOutput.p2pBackend
    )

    val checkInAckListener: CheckInAckListener = new CheckInAckListener(
      p2pListener = p2pListener,
      p2pConfiguration = Configuration.configuration.p2pConfiguration,
      checkInAckCommandHandler = Domain.Port.checkInAckCommandHandler
    )

    val checkInNackListener: CheckInNackListener = new CheckInNackListener(
      p2pListener = p2pListener,
      p2pConfiguration = Configuration.configuration.p2pConfiguration,
      checkInAckCommandHandler = Domain.Port.checkInAckCommandHandler
    )

    val syncAckListener: SyncAckListener = new SyncAckListener(
      p2pListener = p2pListener,
      p2pConfiguration = Configuration.configuration.p2pConfiguration,
      syncStatusCommandHandler = Domain.Port.syncStatusCommandHandler
    )
    val syncNackListener: SyncNackListener = new SyncNackListener(
      p2pListener = p2pListener,
      p2pConfiguration = Configuration.configuration.p2pConfiguration,
      syncStatusCommandHandler = Domain.Port.syncStatusCommandHandler
    )

    val heartbeatListener: HeartbeatListener = new HeartbeatListener(
      p2pListener = p2pListener,
      p2pConfiguration = Configuration.configuration.p2pConfiguration,
      heartbeatCommandHandler = Domain.Port.heartbeatCommandHandler
    )

    val ipfsClusterPinCountFeed: IpfsClusterPinCountFeed = new IpfsClusterPinCountFeed(
      ipfsClusterService = Domain.Service.ipfsClusterService
    )
  }

  object Domain {
    object Port {

      val syncStatusCommandHandler: SyncStatusCommandHandler = new SyncStatusCommandHandlerImpl(
        p2pService = Domain.Service.p2pService
      )

      val syncStatusQueryHandler: SyncStatusQueryHandler = new SyncStatusQueryHandlerImpl(
        p2pService = Domain.Service.p2pService
      )

      val checkInAckCommandHandler: CheckInAckCommandHandler = new CheckInAckCommandHandlerImpl(
        p2pService = Domain.Service.p2pService
      )

      val checkInAckQueryHandler: CheckInAckQueryHandler = new CheckInAckQueryHandlerImpl(
        p2pService = Domain.Service.p2pService
      )

      val checkInCommandHandler: CheckInCommandHandler = new CheckInCommandHandlerImpl(
        ipfsClusterService = Domain.Service.ipfsClusterService,
        ipfsService = Domain.Service.ipfsService,
        p2pService = Domain.Service.p2pService,
        sharedFolderService = Service.sharedFolderService,
        configuration = Configuration.configuration
      )

      val heartbeatCommandHandler: HeartbeatCommandHandler = new HeartbeatCommandHandlerImpl(
        ipfsClusterService = Service.ipfsClusterService,
        ipfsService = Service.ipfsService,
        p2pService = Service.p2pService
      )

      val advertiseSharedFolderUpdatesCommandHandler: AdvertiseSharedFolderUpdatesCommandHandler = {
        new AdvertiseSharedFolderUpdatesCommandHandlerImpl(
          ipfsService = Service.ipfsService,
          p2pService = Service.p2pService,
          sharedFolderService = Service.sharedFolderService
        )
      }

      val forceSyncCommandHandler: ForceSyncCommandHandler = new ForceSyncCommandHandlerImpl(
        ipfsService = Service.ipfsService,
        p2pService = Service.p2pService
      )

      val federationMemberStatusQueryHandler: FederationMemberStatusQueryHandler =
        new FederationMemberStatusQueryHandlerImpl(
          p2pService = Service.p2pService,
          configuration = Configuration.configuration,
          ipfsClusterService = Service.ipfsClusterService,
          ipfsService = Service.ipfsService,
          sharedFolderService = Service.sharedFolderService
        )
    }

    object Service {
      val sharedFolderService: SharedFolderService[IO] = new SharedFolderServiceImpl(
        configuration = Configuration.configuration
      )

      val ipfsService: IpfsService = new IpfsServiceImpl(
        ipfsClient = Output.ipfsClient,
        configuration = Configuration.configuration
      )

      val ipfsClusterService: IpfsClusterService[IO] = new IpfsClusterServiceImpl(
        ipfsClusterClient = Output.ipfsClusterClient
      )

      val p2pService: P2pService[IO] = new P2pServiceImpl(
        p2pClient = Output.p2pClient
      )
    }
  }

  object Output {
    val ipfsClient: IpfsClient = new IpfsClientImpl(
      sttpBackend = HttpURLConnectionBackend(),
      ipfsConfiguration = Configuration.configuration.ipfs
    )
    val ipfsClusterClient: IpfsClusterClient[IO] = new IpfsClusterClientImpl(
      sttpBackend = HttpURLConnectionBackend(),
      ipfsClusterConfiguration = Configuration.configuration.ipfsCluster
    )

    val p2pClient: P2pClient[IO] = new P2pClientImpl(
      p2pBackend = InputOutput.p2pBackend,
      p2pConfiguration = Configuration.configuration.p2pConfiguration
    )
  }
}
