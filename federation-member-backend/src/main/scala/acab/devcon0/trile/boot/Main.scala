package acab.devcon0.trile.boot

import scala.language.postfixOps

import acab.devcon0.trile.configuration.CirceCodecs
import acab.devcon0.trile.configuration.CirceCodecs.Encoders._
import acab.devcon0.trile.configuration.Configuration
import cats.effect.ExitCode
import cats.effect.IO
import cats.effect.IOApp
import cats.effect.std.Supervisor
import com.comcast.ip4s.Port
import com.comcast.ip4s.host
import org.http4s.ember.server.EmberServerBuilder
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

object Main extends IOApp.Simple {

  private val logger: Logger[IO]           = Slf4jLogger.getLogger[IO]
  private val configuration: Configuration = TrileIoC.Configuration.configuration

  override protected def blockedThreadDetectionEnabled: Boolean = true

  private val logConfigurationProgram: IO[Unit] = {
    CirceCodecs.Encoders
      .Configuration(configuration)
      .flatMap(str => logger.warn(s"Configuration=$str"))
  }

  private val serverProgram: IO[Unit] = {
    EmberServerBuilder
      .default[IO]
      .withHost(host"0.0.0.0")
      .withPort(Port.fromInt(configuration.http.port).get)
      .withHttpApp(TrileIoC.Input.routes.httpApp)
      .build
      .use(_ => IO.never)
      .as(ExitCode.Success)
      .void
  }

  val run: IO[Unit] = Supervisor[IO](await = true).use { supervisor =>
    (for
      _ <- logConfigurationProgram
      _ <- supervisor.supervise(serverProgram)
      _ <- TrileIoC.InputOutput.p2pBackend.init
      _ <- supervisor.supervise(TrileIoC.Input.checkInAckListener.run())
      _ <- supervisor.supervise(TrileIoC.Input.syncAckListener.run())
      _ <- supervisor.supervise(TrileIoC.Input.syncNackListener.run())
      _ <- TrileIoC.Input.checkInAdvertiser.run()
      _ <- supervisor.supervise(TrileIoC.Input.heartbeatListener.run())
      _ <- supervisor.supervise(TrileIoC.Input.ipfsClusterPinCountFeed.run())
      _ <- supervisor.supervise(TrileIoC.Input.sharedFolderWatcher.run())
      _ <- supervisor.supervise(TrileIoC.Input.syncAdvertiser.run())
    yield ()) >> IO.never.void
  }
}
