package acab.devcon0.trile.domain.adapters
import acab.devcon0.trile.configuration.Configuration
import acab.devcon0.trile.domain.dtos.http.FederationMemberStatus
import acab.devcon0.trile.domain.ports.input.FederationMemberStatusQuery
import acab.devcon0.trile.domain.ports.input.FederationMemberStatusQueryHandler
import acab.devcon0.trile.domain.service.IpfsClusterService
import acab.devcon0.trile.domain.service.IpfsService
import acab.devcon0.trile.domain.service.P2pService
import acab.devcon0.trile.domain.service.SharedFolderService
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class FederationMemberStatusQueryHandlerImpl(
    p2pService: P2pService[IO],
    ipfsClusterService: IpfsClusterService[IO],
    ipfsService: IpfsService,
    sharedFolderService: SharedFolderService[IO],
    configuration: Configuration
) extends FederationMemberStatusQueryHandler {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(query: FederationMemberStatusQuery): IO[FederationMemberStatus] = {
    for
      nickname              <- IO(configuration.federationMemberNickname)
      p2pPeerId             <- p2pService.getPeerId
      checkInStatus         <- p2pService.getCheckInStatus
      syncStatus            <- p2pService.getSyncStatus
      ipfsClusterPeers      <- ipfsClusterService.getPeers
      ipfsClusterPinsCount  <- ipfsClusterService.getPinsCount
      sharedFolderPin       <- ipfsService.getSharedFolderCid
      sharedFolderFileCount <- sharedFolderService.getSharedFolderFileCount
      sharedFolderSizeBytes <- sharedFolderService.getSharedFolderSize
      ipfsPeerRepoStat      <- ipfsService.getRepoStat
      controllerStatus      <- p2pService.getControllerStatus
    yield FederationMemberStatus(
      nickname = nickname,
      p2pPeerId = p2pPeerId,
      sharedFolderPin = sharedFolderPin,
      sharedFolderFileCount = sharedFolderFileCount,
      sharedFolderSizeBytes = sharedFolderSizeBytes,
      checkInStatus = checkInStatus,
      syncStatus = syncStatus._1,
      ipfsClusterPeers = ipfsClusterPeers,
      ipfsClusterPinsCount = ipfsClusterPinsCount,
      ipfsRepoSize = ipfsPeerRepoStat.repoSize,
      ipfsStorageMax = ipfsPeerRepoStat.storageMax,
      controllerStatus = controllerStatus._1,
      controllerLastSeen = controllerStatus._2
    )
  }
}
