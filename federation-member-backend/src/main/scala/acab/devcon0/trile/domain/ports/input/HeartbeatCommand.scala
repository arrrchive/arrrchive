package acab.devcon0.trile.domain.ports.input

import acab.devcon0.trile.cqrs.Command
import acab.devcon0.trile.cqrs.CommandHandler
import acab.devcon0.trile.cqrs.Event
import acab.devcon0.trile.domain.dtos.IpfsClusterPeer
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

case class HeartbeatCommand(peers: List[IpfsClusterPeer]) extends Command[String]

sealed abstract class HeartbeatEvent[T]              extends Event[T]
case class HeartbeatSuccessEvent()                   extends HeartbeatEvent[Unit]
case class HeartbeatErrorEvent(throwable: Throwable) extends HeartbeatEvent[Unit]

trait HeartbeatCommandHandler extends CommandHandler[HeartbeatCommand, String, HeartbeatEvent[?]]

object HeartbeatCommandImplicits {
  implicit class PingCommandEventFlattenOps(result: IO[HeartbeatEvent[?]]) {
    private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

    def flattenEvents: IO[Unit] = {
      result.flatMap {
        case HeartbeatSuccessEvent()        => IO.unit
        case HeartbeatErrorEvent(throwable) => handleErrorCase(throwable)
      }
    }

    private def handleErrorCase(exception: Throwable): IO[Unit] = {
      logger.error(s"exception=$exception") >>
        IO.raiseError[Unit](exception)
    }
  }
}
