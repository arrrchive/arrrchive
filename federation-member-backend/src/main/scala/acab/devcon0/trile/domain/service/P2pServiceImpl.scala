package acab.devcon0.trile.domain.service

import java.time.Instant
import java.util.UUID
import java.util.concurrent.atomic.AtomicReference

import acab.devcon0.trile.domain.dtos
import acab.devcon0.trile.domain.dtos.CheckInStatus
import acab.devcon0.trile.domain.dtos.CheckInStatus.Unknown
import acab.devcon0.trile.domain.dtos.ControllerStatus
import acab.devcon0.trile.domain.dtos.ControllerStatus.Online
import acab.devcon0.trile.domain.dtos.SyncStatus
import acab.devcon0.trile.domain.dtos.SyncStatus.Syncing
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message._
import acab.devcon0.trile.domain.dtos.pubsub.P2p._
import acab.devcon0.trile.domain.ports.output.P2pClient
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class P2pServiceImpl(p2pClient: P2pClient[IO]) extends P2pService[IO] {

  private val logger: Logger[IO]                                       = Slf4jLogger.getLogger[IO]
  private val checkInStatusReference: AtomicReference[CheckInStatus]   = new AtomicReference[CheckInStatus](Unknown)
  private val syncStatusReference: AtomicReference[(SyncStatus, UUID)] = initialSyncStatusReference

  private val controllerStatusReference: AtomicReference[Option[Instant]] =
    new AtomicReference[Option[Instant]](None)

  override def setControllerStatusOnline(): IO[Unit] = {
    IO(controllerStatusReference.set(Some(Instant.now())))
  }

  override def getControllerStatus: IO[(ControllerStatus, Option[Instant])] = {
    IO(controllerStatusReference.get())
      .map(timestamp => {
        val aMinuteAgo: Instant = Instant.now().minusSeconds(60)
        timestamp match
          case Some(lastSeen) if lastSeen.isBefore(aMinuteAgo) => (ControllerStatus.Offline, timestamp)
          case Some(_)                                         => (ControllerStatus.Online, timestamp)
          case None                                            => (ControllerStatus.Unknown, timestamp)
      })
      .flatTap {
        case (dtos.ControllerStatus.Online, _) => IO.unit
        case (_, _)                            => p2pClient.connect.start
      }
  }

  override def setCheckInStatus(checkInStatus: CheckInStatus): IO[Unit] = {
    IO(checkInStatusReference.set(checkInStatus))
  }

  override def getCheckInStatus: IO[CheckInStatus] = {
    IO(checkInStatusReference.get())
  }

  override def setSyncStatus(syncStatus: SyncStatus): IO[Unit] = {
    IO(syncStatusReference.set((syncStatus, UUID.randomUUID()))) >>
      logger.debug(s"Sync status is $syncStatus")
  }

  override def getSyncStatus: IO[(SyncStatus, UUID)] = {
    IO(syncStatusReference.get())
  }

  override def getPeerId: IO[P2pPeerId] = {
    p2pClient.getPeerId
  }

  override def publishCheckIn(message: FederationMemberCheckIn): IO[Unit] = {
    p2pClient.publish(message)
  }

  override def publishFederationMemberSync(message: FederationMemberSync): IO[Unit] = {
    p2pClient.publish(message)
  }

  override def publishFederationMemberHeartbeat(message: FederationMemberHeartbeat): IO[Unit] = {
    p2pClient.publish(message)
  }

  override def publishFederationMemberSharingFolderUpdate(
      message: FederationMemberSharingFolderUpdate
  ): IO[Unit] = {
    p2pClient.publish(message)
  }

  private def initialSyncStatusReference: AtomicReference[(SyncStatus, UUID)] = {
    new AtomicReference[(SyncStatus, UUID)]((Syncing, UUID.randomUUID()))
  }
}
