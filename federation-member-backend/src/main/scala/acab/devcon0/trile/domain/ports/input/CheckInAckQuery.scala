package acab.devcon0.trile.domain.ports.input

import acab.devcon0.trile.cqrs.Query
import acab.devcon0.trile.cqrs.QueryHandler
import acab.devcon0.trile.domain.dtos.CheckInStatus

final case class CheckInAckQuery() extends Query[CheckInStatus]

trait CheckInAckQueryHandler extends QueryHandler[CheckInAckQuery, CheckInStatus]
