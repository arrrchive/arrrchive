package acab.devcon0.trile.domain.ports.inputoutput
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Data
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Metadata
import cats.effect.Resource
import io.circe.Encoder
import io.libp2p.core.PeerId
import io.libp2p.core.pubsub.Topic
import io.libp2p.pubsub.gossip.Gossip

trait P2pBackend[F[_]] {
  def init: F[Unit]
  def getGossip: Resource[F, Gossip]
  def getHostPeerId: PeerId
  def publishMessage[T <: Data: Encoder](data: T, topic: Topic, metadata: Metadata): F[Unit]
}
