package acab.devcon0.trile.domain.service

import java.util.concurrent.atomic.AtomicReference

import acab.devcon0.trile.configuration.Configuration
import acab.devcon0.trile.domain.dtos.IpfsPeerRepoStat
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import acab.devcon0.trile.domain.ports.output.IpfsClient
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class IpfsServiceImpl(ipfsClient: IpfsClient, configuration: Configuration) extends IpfsService {

  private val logger: Logger[IO]                                     = Slf4jLogger.getLogger[IO]
  private val sharedFolderIpfsCidReference: AtomicReference[IpfsCid] = new AtomicReference[IpfsCid]()

  override def getSharedFolderCid: IO[IpfsCid] = {
    IO(sharedFolderIpfsCidReference.get())
  }

  override def triggerPinSharedFolder(): IO[IpfsCid] = {
    val path: String = configuration.ipfs.sharedFolderIpfsPov
    ipfsClient.repoGc
      .flatMap(_ => ipfsClient.addWithPinAndNoCopy(path = path))
      .map(_.ipfsCid)
      .flatTap(ipfsCid => IO(sharedFolderIpfsCidReference.set(ipfsCid)))
      .flatTap(ipfsCid => logger.debug(s"addWatchingDirectory.response=$ipfsCid"))
  }

  override def getRepoStat: IO[IpfsPeerRepoStat] = {
    ipfsClient.getRepoStat
  }
}
