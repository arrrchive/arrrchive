package acab.devcon0.trile.domain.dtos

import acab.devcon0.trile.domain.dtos.aliases.IpfsCid

final case class IpfsAddResponse(ipfsCid: IpfsCid)
