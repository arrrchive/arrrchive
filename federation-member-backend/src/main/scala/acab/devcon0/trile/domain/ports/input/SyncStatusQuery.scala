package acab.devcon0.trile.domain.ports.input

import java.util.UUID

import acab.devcon0.trile.cqrs.Query
import acab.devcon0.trile.cqrs.QueryHandler
import acab.devcon0.trile.domain.dtos.SyncStatus

final case class SyncStatusQuery() extends Query[(SyncStatus, UUID)]

trait SyncStatusQueryHandler extends QueryHandler[SyncStatusQuery, (SyncStatus, UUID)]
