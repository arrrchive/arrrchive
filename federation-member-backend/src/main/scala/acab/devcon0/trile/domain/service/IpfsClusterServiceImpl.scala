package acab.devcon0.trile.domain.service

import java.util.concurrent.atomic.AtomicReference

import acab.devcon0.trile.domain.dtos.IpfsClusterPeer
import acab.devcon0.trile.domain.dtos.IpfsClusterPeers
import acab.devcon0.trile.domain.ports.output.IpfsClusterClient
import acab.devcon0.trile.utils.IORetry
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class IpfsClusterServiceImpl(ipfsClusterClient: IpfsClusterClient[IO]) extends IpfsClusterService[IO] {

  implicit val logger: Logger[IO]                               = Slf4jLogger.getLogger[IO]
  private val defaultPeers: IpfsClusterPeers                    = IpfsClusterPeers(List())
  private val peersReference: AtomicReference[IpfsClusterPeers] = new AtomicReference[IpfsClusterPeers](defaultPeers)
  private val pinCountReference: AtomicReference[Int]           = new AtomicReference[Int](0)

  override def getPeers: IO[IpfsClusterPeers] = {
    IO(peersReference.get())
  }

  override def setPeers(peers: List[IpfsClusterPeer]): IO[Unit] = {
    IO(peersReference.set(IpfsClusterPeers(peers)))
  }

  override def setPinsCount(): IO[Int] = {
    getPinsCountInner
      .flatTap(count => IO(pinCountReference.set(count)))
  }

  private def getPinsCountInner: IO[Int] = {
    IORetry.fibonacci(ipfsClusterClient.getPinsCount)
  }

  override def getPinsCount: IO[Int] = {
    IO(pinCountReference.get())
  }

  override def getId: IO[IpfsClusterPeer] = {
    ipfsClusterClient.getId
  }
}
