package acab.devcon0.trile.domain.ports.input

import acab.devcon0.trile.cqrs.Command
import acab.devcon0.trile.cqrs.CommandHandler
import acab.devcon0.trile.cqrs.Event

case class CheckInCommand() extends Command[String]

sealed abstract class CheckInEvent[T]              extends Event[T]
case class CheckInSuccessEvent()                   extends CheckInEvent[Unit]
case class CheckInErrorEvent(throwable: Throwable) extends CheckInEvent[Unit]

trait CheckInCommandHandler extends CommandHandler[CheckInCommand, String, CheckInEvent[?]]
