package acab.devcon0.trile.domain.adapters

import acab.devcon0.trile.domain.dtos.CheckInStatus
import acab.devcon0.trile.domain.ports.input.CheckInAckQuery
import acab.devcon0.trile.domain.ports.input.CheckInAckQueryHandler
import acab.devcon0.trile.domain.service.P2pService
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class CheckInAckQueryHandlerImpl(p2pService: P2pService[IO]) extends CheckInAckQueryHandler {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(query: CheckInAckQuery): IO[CheckInStatus] = {
    p2pService.getCheckInStatus
  }
}
