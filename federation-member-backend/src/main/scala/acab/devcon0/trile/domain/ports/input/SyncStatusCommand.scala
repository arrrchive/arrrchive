package acab.devcon0.trile.domain.ports.input

import acab.devcon0.trile.cqrs.Command
import acab.devcon0.trile.cqrs.CommandHandler
import acab.devcon0.trile.cqrs.Event
import acab.devcon0.trile.domain.dtos.SyncStatus
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

final case class SyncStatusCommand(syncStatus: SyncStatus) extends Command[String]

sealed abstract class SyncStatusEvent[T]                    extends Event[T]
final case class SyncStatusErrorEvent(throwable: Throwable) extends SyncStatusEvent[Unit]
final case class SyncStatusSuccessEvent()                   extends SyncStatusEvent[Unit]

trait SyncStatusCommandHandler extends CommandHandler[SyncStatusCommand, String, SyncStatusEvent[?]]

object SyncStatusCommandImplicits {
  implicit class SyncStatusCommandEventFlattenOps(result: IO[SyncStatusEvent[?]]) {
    private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

    def flattenEvents: IO[Unit] = {
      result.flatMap {
        case SyncStatusSuccessEvent()        => IO.unit
        case SyncStatusErrorEvent(throwable) => handleErrorCase(throwable)
      }
    }

    private def handleErrorCase(exception: Throwable): IO[Unit] = {
      logger.error(s"SyncStatusCommandHandler exception=$exception") >>
        IO.raiseError[Unit](exception)
    }
  }
}
