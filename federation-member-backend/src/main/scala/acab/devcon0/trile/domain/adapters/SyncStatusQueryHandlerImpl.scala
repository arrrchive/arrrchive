package acab.devcon0.trile.domain.adapters

import java.util.UUID

import acab.devcon0.trile.domain.dtos.SyncStatus
import acab.devcon0.trile.domain.ports.input.SyncStatusQuery
import acab.devcon0.trile.domain.ports.input.SyncStatusQueryHandler
import acab.devcon0.trile.domain.service.P2pService
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class SyncStatusQueryHandlerImpl(p2pService: P2pService[IO]) extends SyncStatusQueryHandler {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(query: SyncStatusQuery): IO[(SyncStatus, UUID)] = {
    p2pService.getSyncStatus
  }
}
