package acab.devcon0.trile.domain.service

import cats.effect.IO

trait SharedFolderService[F[_]] {
  def updateSharedFolderInformation(): IO[Unit]
  def getSharedFolderFileCount: F[Int]
  def getSharedFolderSize: F[Long]
}
