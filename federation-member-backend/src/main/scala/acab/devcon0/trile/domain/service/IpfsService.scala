package acab.devcon0.trile.domain.service

import acab.devcon0.trile.domain.dtos.IpfsPeerRepoStat
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO

trait IpfsService {
  def getSharedFolderCid: IO[IpfsCid]
  def triggerPinSharedFolder(): IO[IpfsCid]
  def getRepoStat: IO[IpfsPeerRepoStat]
}
