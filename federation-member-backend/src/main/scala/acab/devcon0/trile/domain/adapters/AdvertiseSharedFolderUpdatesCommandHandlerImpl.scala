package acab.devcon0.trile.domain.adapters

import java.time.Instant

import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberSharingFolderUpdate
import acab.devcon0.trile.domain.ports.input._
import acab.devcon0.trile.domain.service.IpfsService
import acab.devcon0.trile.domain.service.P2pService
import acab.devcon0.trile.domain.service.SharedFolderService
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class AdvertiseSharedFolderUpdatesCommandHandlerImpl(
    ipfsService: IpfsService,
    p2pService: P2pService[IO],
    sharedFolderService: SharedFolderService[IO]
) extends AdvertiseSharedFolderUpdatesCommandHandler {

  private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(cmd: AdvertiseSharedFolderUpdatesCommand): IO[AdvertiseSharedFolderUpdatesEvent[?]] = {
    handleInner()
      .map(_ => AdvertiseSharedFolderUpdatesSuccessEvent())
      .handleError(throwable => AdvertiseSharedFolderUpdatesErrorEvent(throwable = throwable))
  }

  private def handleInner(): IO[Unit] = {
    val timestamp: Instant = Instant.now()
    ipfsService
      .triggerPinSharedFolder()
      .flatTap(_ => sharedFolderService.updateSharedFolderInformation())
      .flatTap(ipfsCid => logger.info(s"New root IPFS CID=$ipfsCid"))
      .map(ipfsCid => FederationMemberSharingFolderUpdate(ipfsCid, timestamp))
      .flatMap(p2pService.publishFederationMemberSharingFolderUpdate)
  }
}
