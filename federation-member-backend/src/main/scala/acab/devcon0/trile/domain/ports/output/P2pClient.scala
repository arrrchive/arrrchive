package acab.devcon0.trile.domain.ports.output

import acab.devcon0.trile.domain.dtos._
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message._
import acab.devcon0.trile.domain.dtos.pubsub.P2p._

trait P2pClient[F[_]] {
  def connect: F[Unit]
  def getPeerId: F[P2pPeerId]
  def publish(message: FederationMemberCheckIn): F[Unit]
  def publish(message: FederationMemberSync): F[Unit]
  def publish(message: FederationMemberHeartbeat): F[Unit]
  def publish(message: FederationMemberSharingFolderUpdate): F[Unit]
}
