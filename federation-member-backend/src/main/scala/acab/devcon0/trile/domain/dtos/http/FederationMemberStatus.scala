package acab.devcon0.trile.domain.dtos.http

import java.time.Instant

import acab.devcon0.trile.domain.dtos.CheckInStatus
import acab.devcon0.trile.domain.dtos.ControllerStatus
import acab.devcon0.trile.domain.dtos.IpfsClusterPeers
import acab.devcon0.trile.domain.dtos.SyncStatus
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId

final case class FederationMemberStatus(
    nickname: String,
    p2pPeerId: P2pPeerId,
    sharedFolderPin: IpfsCid,
    sharedFolderFileCount: Int,
    sharedFolderSizeBytes: Long,
    checkInStatus: CheckInStatus,
    syncStatus: SyncStatus,
    ipfsClusterPeers: IpfsClusterPeers,
    ipfsClusterPinsCount: Int,
    ipfsRepoSize: Long,
    ipfsStorageMax: Long,
    controllerStatus: ControllerStatus,
    controllerLastSeen: Option[Instant]
)
