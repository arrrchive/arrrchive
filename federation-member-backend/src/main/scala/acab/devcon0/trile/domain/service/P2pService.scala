package acab.devcon0.trile.domain.service

import java.time.Instant
import java.util.UUID

import acab.devcon0.trile.domain.dtos.CheckInStatus
import acab.devcon0.trile.domain.dtos.ControllerStatus
import acab.devcon0.trile.domain.dtos.SyncStatus
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message._
import acab.devcon0.trile.domain.dtos.pubsub.P2p._

trait P2pService[F[_]] {
  def setControllerStatusOnline(): F[Unit]
  def getControllerStatus: F[(ControllerStatus, Option[Instant])]
  def setCheckInStatus(checkInStatus: CheckInStatus): F[Unit]
  def getCheckInStatus: F[CheckInStatus]
  def setSyncStatus(syncStatus: SyncStatus): F[Unit]
  def getSyncStatus: F[(SyncStatus, UUID)]
  def getPeerId: F[P2pPeerId]
  def publishCheckIn(message: FederationMemberCheckIn): F[Unit]
  def publishFederationMemberSync(message: FederationMemberSync): F[Unit]
  def publishFederationMemberHeartbeat(message: FederationMemberHeartbeat): F[Unit]
  def publishFederationMemberSharingFolderUpdate(message: FederationMemberSharingFolderUpdate): F[Unit]
}
