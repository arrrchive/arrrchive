package acab.devcon0.trile.domain.ports.input

import acab.devcon0.trile.cqrs.Command
import acab.devcon0.trile.cqrs.CommandHandler
import acab.devcon0.trile.cqrs.Event

case class AdvertiseSharedFolderUpdatesCommand() extends Command[String]

sealed abstract class AdvertiseSharedFolderUpdatesEvent[T]              extends Event[T]
case class AdvertiseSharedFolderUpdatesSuccessEvent()                   extends AdvertiseSharedFolderUpdatesEvent[Unit]
case class AdvertiseSharedFolderUpdatesErrorEvent(throwable: Throwable) extends AdvertiseSharedFolderUpdatesEvent[Unit]

trait AdvertiseSharedFolderUpdatesCommandHandler
    extends CommandHandler[AdvertiseSharedFolderUpdatesCommand, String, AdvertiseSharedFolderUpdatesEvent[?]]
