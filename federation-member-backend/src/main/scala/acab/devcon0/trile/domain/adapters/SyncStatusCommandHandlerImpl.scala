package acab.devcon0.trile.domain.adapters

import acab.devcon0.trile.domain.dtos.SyncStatus
import acab.devcon0.trile.domain.ports.input._
import acab.devcon0.trile.domain.service.P2pService
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class SyncStatusCommandHandlerImpl(p2pService: P2pService[IO]) extends SyncStatusCommandHandler {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(cmd: SyncStatusCommand): IO[SyncStatusEvent[?]] = {
    p2pService
      .setSyncStatus(cmd.syncStatus)
      .flatTap(_ => setControllerStatusToOnline(cmd))
      .map(_ => SyncStatusSuccessEvent())
      .onError(throwable => IO(SyncStatusErrorEvent(throwable = throwable)))
  }

  private def setControllerStatusToOnline(cmd: SyncStatusCommand): IO[Unit] = {
    cmd.syncStatus match
      case SyncStatus.Sync => p2pService.setControllerStatusOnline()
      case _               => IO.unit
  }
}
