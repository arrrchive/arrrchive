package acab.devcon0.trile.domain.ports.input

import acab.devcon0.trile.cqrs.Query
import acab.devcon0.trile.cqrs.QueryHandler
import acab.devcon0.trile.domain.dtos.http.FederationMemberStatus

final case class FederationMemberStatusQuery() extends Query[FederationMemberStatus]

trait FederationMemberStatusQueryHandler extends QueryHandler[FederationMemberStatusQuery, FederationMemberStatus]
