package acab.devcon0.trile.domain.ports.output

import acab.devcon0.trile.domain.dtos.IpfsAddResponse
import acab.devcon0.trile.domain.dtos.IpfsPeerRepoStat
import cats.effect.IO

trait IpfsClient {
  def addWithPinAndNoCopy(path: String): IO[IpfsAddResponse]
  def repoGc: IO[Unit]
  def getRepoStat: IO[IpfsPeerRepoStat]
}
