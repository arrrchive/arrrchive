package acab.devcon0.trile.domain.adapters

import java.time.Instant

import acab.devcon0.trile.configuration.Configuration
import acab.devcon0.trile.domain.dtos.IpfsClusterPeer
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberCheckIn
import acab.devcon0.trile.domain.ports.input._
import acab.devcon0.trile.domain.service.IpfsClusterService
import acab.devcon0.trile.domain.service.IpfsService
import acab.devcon0.trile.domain.service.P2pService
import acab.devcon0.trile.domain.service.SharedFolderService
import cats.effect.IO
import org.typelevel.log4cats.slf4j.Slf4jLogger

class CheckInCommandHandlerImpl(
    ipfsClusterService: IpfsClusterService[IO],
    ipfsService: IpfsService,
    p2pService: P2pService[IO],
    sharedFolderService: SharedFolderService[IO],
    configuration: Configuration
) extends CheckInCommandHandler {

  Slf4jLogger.getLogger[IO]

  override def handle(cmd: CheckInCommand): IO[CheckInEvent[?]] = {
    handleInner()
      .map(_ => CheckInSuccessEvent())
      .handleError(throwable => CheckInErrorEvent(throwable = throwable))
  }

  private def handleInner(): IO[IpfsCid] = {
    for
      ipfsClusterPeer <- ipfsClusterService.getId
      ipfsRootCid     <- ipfsService.triggerPinSharedFolder()
      p2pPeerId       <- p2pService.getPeerId
      message         <- getMessage(ipfsClusterPeer, ipfsRootCid, p2pPeerId)
      _               <- sharedFolderService.updateSharedFolderInformation()
      _               <- p2pService.publishCheckIn(message)
    yield ipfsRootCid
  }

  private def getMessage(
      ipfsClusterPeer: IpfsClusterPeer,
      sharedFolderCid: IpfsCid,
      p2pPeerId: P2pPeerId
  ): IO[FederationMemberCheckIn] = IO {
    FederationMemberCheckIn(
      p2pPeerId = p2pPeerId,
      nickname = configuration.federationMemberNickname,
      ipfsPeerId = ipfsClusterPeer.ipfs.id,
      sharedFolderCid = sharedFolderCid,
      ipfsClusterPeerId = ipfsClusterPeer.id,
      ipfsClusterNodeName = ipfsClusterPeer.peername,
      timestamp = Instant.now()
    )
  }
}
