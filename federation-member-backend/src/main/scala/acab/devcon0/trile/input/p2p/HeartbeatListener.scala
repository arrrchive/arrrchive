package acab.devcon0.trile.input.p2p

import acab.devcon0.trile.configuration.P2pConfiguration
import acab.devcon0.trile.domain.codecs.P2pCodecs
import acab.devcon0.trile.domain.codecs.P2pCodecs.Decoders.federationControllerHeartbeat
import acab.devcon0.trile.domain.dtos.pubsub.P2p
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Listener._
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationControllerHeartbeat
import acab.devcon0.trile.domain.dtos.pubsub.P2p.MessageType.Event
import acab.devcon0.trile.domain.dtos.pubsub.P2p._
import acab.devcon0.trile.domain.ports.input.HeartbeatCommand
import acab.devcon0.trile.domain.ports.input.HeartbeatCommandHandler
import acab.devcon0.trile.domain.ports.input.HeartbeatCommandImplicits.PingCommandEventFlattenOps
import cats.effect._
import cats.effect.unsafe.IORuntime
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class HeartbeatListener(
    p2pListener: P2pListener,
    p2pConfiguration: P2pConfiguration,
    heartbeatCommandHandler: HeartbeatCommandHandler
) {

  private implicit val runtime: IORuntime = cats.effect.unsafe.IORuntime.global
  private val logger: Logger[IO]          = Slf4jLogger.getLogger[IO]

  def run(): IO[Unit] = {
    for
      topic <- IO(p2pConfiguration.topics.heartbeat)
      listenerParams = Params(topic = topic, messageTypes = List(MessageType.Event), mode = Mode.Broadcast)
      _ <- p2pListener.run(listenerParams, message => triggerCommandInBackground(message))
    yield ()
  }

  private def triggerCommandInBackground(message: Message[FederationControllerHeartbeat]): IO[Unit] = IO {
    val controllerHeartbeat = message.data
    (for
      _ <- heartbeatCommandHandler.handle(HeartbeatCommand(controllerHeartbeat.peers)).flattenEvents
      _ <- logger.debug(s"Heartbeat received over P2P pub/sub. p2pPeerId=${message.meta.from}}")
    yield ()).unsafeRunAndForget()
  }
}
