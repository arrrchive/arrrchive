package acab.devcon0.trile.input.p2p

import java.nio.charset.StandardCharsets
import java.util.function.Consumer

import acab.devcon0.trile.domain.codecs.P2pCodecs
import acab.devcon0.trile.domain.codecs.P2pCodecs.Decoders._
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import acab.devcon0.trile.domain.dtos.pubsub.P2p
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Data
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Listener.Mode
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Listener.Params
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message
import acab.devcon0.trile.domain.dtos.pubsub.P2p.MessageType
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Metadata
import acab.devcon0.trile.domain.ports.inputoutput.P2pBackend
import cats.effect._
import cats.effect.std.Supervisor
import cats.effect.unsafe.IORuntime
import io.circe.Decoder
import io.libp2p.core.pubsub.MessageApi
import io.libp2p.core.pubsub.PubsubSubscription
import io.libp2p.core.pubsub.Topic
import io.libp2p.pubsub.gossip.Gossip
import io.netty.buffer.ByteBuf
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class P2pListener(p2pBackend: P2pBackend[IO]) {

  private implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]
  private implicit val runtime: IORuntime = cats.effect.unsafe.IORuntime.global
  private val hostPeerId: P2pPeerId       = p2pBackend.getHostPeerId.toBase58

  def run[T <: Data: Decoder](listenerParams: Params, callback: Message[T] => IO[Unit]): IO[Unit] = {
    logger.info(
      s"Creating ${listenerParams.mode} listener for ${listenerParams.topic.getTopic} key=${listenerParams.messageTypes}"
    ) >>
      p2pBackend.getGossip
        .evalMap(gossip => subscribe(gossip, listenerParams, callback))
        .useForever
        .void
  }

  private def subscribe[T <: Data: Decoder](
      gossip: Gossip,
      listenerParams: Params,
      callback: Message[T] => IO[Unit]
  ): IO[PubsubSubscription] = {
    IO(
      gossip.subscribe(
        new Consumer[MessageApi] {
          override def accept(messageApi: MessageApi): Unit = {
            processMessage(messageApi, listenerParams, callback)
          }
        },
        listenerParams.topic
      )
    )
  }

  private def processMessage[T <: Data: Decoder](
      messageApi: MessageApi,
      listenerParams: Params,
      callback: Message[T] => IO[Unit]
  ): Unit = {
    Supervisor[IO](await = true)
      .use(supervisor => {
        (for message: Message[T] <- P2pCodecs.Decoders.Message[T](getString(messageApi.getData))
        yield {
          lazy val matchingKey: Boolean          = isMatchingKey(listenerParams.messageTypes, message.meta)
          lazy val shouldConsumeMessage: Boolean = isShouldConsumeMessage(message.meta, listenerParams.mode)
          if matchingKey && shouldConsumeMessage then callback(message).unsafeRunAndForget()
        }).void
      })
      .unsafeRunAndForget()(runtime)
  }

  private def isShouldConsumeMessage(metadata: Metadata, mode: Mode): Boolean = {
    val maybeP2pPeerId: Option[P2pPeerId] = metadata.to

    lazy val notMyMessage: Boolean = isNotMyMessage(metadata)
    if mode.equals(Mode.Broadcast) && notMyMessage then true
    else if mode.equals(Mode.Directed) && maybeP2pPeerId.contains(hostPeerId) then true
    else false
  }

  private def isNotMyMessage(metadata: Metadata): Boolean = {
    !hostPeerId.equals(metadata.from)
  }

  private def isMatchingKey(messageTypes: List[MessageType], metadata: Metadata): Boolean = {
    messageTypes.contains(metadata.messageType)
  }

  private def getString(byteBuf: ByteBuf): String = {
    new String(byteBuf.array, StandardCharsets.UTF_8)
  }
}
