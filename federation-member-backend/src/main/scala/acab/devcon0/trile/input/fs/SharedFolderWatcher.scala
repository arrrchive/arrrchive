package acab.devcon0.trile.input.fs

import java.io.File
import java.nio.file.Path
import java.time.Instant
import java.util.concurrent.atomic.AtomicReference

import scala.concurrent.duration.DurationInt

import acab.devcon0.trile.domain.dtos.SyncStatus
import acab.devcon0.trile.domain.dtos.SyncStatus.Syncing
import acab.devcon0.trile.domain.ports.input.AdvertiseSharedFolderUpdatesCommand
import acab.devcon0.trile.domain.ports.input.AdvertiseSharedFolderUpdatesCommandHandler
import acab.devcon0.trile.domain.service.P2pService
import cats.effect.IO
import cats.effect.std.Supervisor
import cats.effect.unsafe.implicits.global
import io.methvin.watcher.DirectoryWatcher
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class SharedFolderWatcher(
    path: String,
    advertiseSharedFolderUpdatesCommandHandler: AdvertiseSharedFolderUpdatesCommandHandler,
    p2pService: P2pService[IO]
) {

  private val logger: Logger[IO]                                    = Slf4jLogger.getLogger[IO]
  private val lastUpdateReference: AtomicReference[Option[Instant]] = new AtomicReference[Option[Instant]](None)
  private val directoryToWatch: Path                                = new File(path).toPath
  private val directoryWatcher: DirectoryWatcher = io.methvin.watcher.DirectoryWatcher
    .builder()
    .path(directoryToWatch)
    .listener(_ => setSharedFolderAsUpdated())
    .build()

  def run(): IO[Unit] = {
    logger.info(s"Starting the shared folder watcher") >>
      Supervisor[IO](await = true)
        .use { supervisor =>
          (for
            _ <- supervisor.supervise(processSharedFolderUpdates())
            _ <- supervisor.supervise(IO.interruptible(directoryWatcher.watch()))
          yield ()) >> IO.never.void
        }
        .handleErrorWith(_ => run())
  }

  private def setSharedFolderAsUpdated(): Unit = {
    (for
      _ <- IO(lastUpdateReference.set(Some(Instant.now())))
      _ <- setSyncingStatus()
    yield {}).onError(logError).unsafeRunAndForget()
  }

  private def setSyncingStatus(): IO[Unit] = {
    p2pService.getSyncStatus.flatMap {
      case (SyncStatus.Syncing, _) => IO.unit
      case _                       => p2pService.setSyncStatus(Syncing)
    }.void
  }

  private def processSharedFolderUpdates(): IO[Unit] = {
    logger.info(s"Watching changes") >>
      IO.sleep(1.seconds)
        .flatTap(_ => logger.debug(s"lastUpdateReference=${lastUpdateReference.get()}"))
        .flatTap(_ => {
          if lastUpdateWasOverFiveSecondsAgo then
            lastUpdateReference.set(None)
            advertiseSharedFolderUpdatesCommandHandler.handle(AdvertiseSharedFolderUpdatesCommand()) >>
              logger.info(s"Processing shared folder updates them after some grace period that bulks updates together")
          else if lastUpdateReference.get().isDefined then
            logger.debug(s"Watching changes, but not yet the moment to process")
          else IO.unit
        })
        .foreverM
  }

  private def lastUpdateWasOverFiveSecondsAgo: Boolean = {
    lastUpdateReference.get().exists(instant => Instant.now().minusSeconds(5).isAfter(instant))
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }
}
