package acab.devcon0.trile.input.p2p

import acab.devcon0.trile.configuration.P2pConfiguration
import acab.devcon0.trile.domain.codecs.P2pCodecs.Decoders.syncNack
import acab.devcon0.trile.domain.dtos.SyncStatus
import acab.devcon0.trile.domain.dtos.pubsub.P2p
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Listener._
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberSyncNack
import acab.devcon0.trile.domain.dtos.pubsub.P2p._
import acab.devcon0.trile.domain.ports.input.SyncStatusCommand
import acab.devcon0.trile.domain.ports.input.SyncStatusCommandHandler
import acab.devcon0.trile.domain.ports.input.SyncStatusCommandImplicits.SyncStatusCommandEventFlattenOps
import cats.effect._
import cats.effect.unsafe.IORuntime
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class SyncNackListener(
    p2pListener: P2pListener,
    p2pConfiguration: P2pConfiguration,
    syncStatusCommandHandler: SyncStatusCommandHandler
) {

  private implicit val runtime: IORuntime = cats.effect.unsafe.IORuntime.global
  private val logger: Logger[IO]          = Slf4jLogger.getLogger[IO]

  def run(): IO[Unit] = {
    for
      topic <- IO(p2pConfiguration.topics.sync)
      listenerParams = Params(topic = topic, messageTypes = List(MessageType.Nack), mode = Mode.Directed)
      _ <- p2pListener.run[FederationMemberSyncNack](listenerParams, message => triggerCommandInBackground(message))
    yield ()
  }

  private def triggerCommandInBackground(Message: Message[FederationMemberSyncNack]): IO[Unit] = IO {
    (for
      _ <- updateSyncStatus()
      _ <- logger.warn(s"Sync NACK received over P2P pub/sub. p2pPeerId=${Message.meta.from}}")
    yield ()).unsafeRunAndForget()
  }

  private def updateSyncStatus(): IO[Unit] = {
    val command: SyncStatusCommand = SyncStatusCommand(SyncStatus.OutOfSync)
    syncStatusCommandHandler.handle(command).flattenEvents
  }
}
