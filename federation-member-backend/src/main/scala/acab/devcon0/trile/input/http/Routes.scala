package acab.devcon0.trile.input.http

import cats.effect.IO
import org.http4s.Http
import org.http4s.server.Router
import org.http4s.server.middleware.CORS

class Routes(statusRoute: FederationMemberStatusRoute) {

  val httpApp: Http[IO, IO] = CORS.policy.withAllowOriginAll(
    Router(
      "/api/status" -> statusRoute.routes
    ).orNotFound
  )

}
