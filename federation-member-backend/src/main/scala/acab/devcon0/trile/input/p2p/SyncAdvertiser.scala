package acab.devcon0.trile.input.p2p

import java.util.UUID
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

import scala.concurrent.duration.Duration

import acab.devcon0.trile.domain.dtos.NackException
import acab.devcon0.trile.domain.dtos.SyncStatus
import acab.devcon0.trile.domain.dtos.SyncStatus.Sync
import acab.devcon0.trile.domain.dtos.SyncStatus.Syncing
import acab.devcon0.trile.domain.ports.input.ForceSyncCommand
import acab.devcon0.trile.domain.ports.input.ForceSyncCommandHandler
import acab.devcon0.trile.domain.ports.input.ForceSyncCommandImplicits.PingCommandEventFlattenOps
import acab.devcon0.trile.domain.ports.input.SyncStatusQuery
import acab.devcon0.trile.domain.ports.input.SyncStatusQueryHandler
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class SyncAdvertiser(forceSyncCommandHandler: ForceSyncCommandHandler, syncStatusQueryHandler: SyncStatusQueryHandler) {

  private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  def run(): IO[Unit] = {
    waitWhileSyncing() >>
      logger.info(s"Sync detector starting") >>
      waitWhileInSync()
        .flatTap(syncStatus => logger.info(s"Sync status is '$syncStatus', keep an eye on it"))
        .flatMap(sync => triggerSync(sync._2))
        .foreverM
        .void
  }

  private def triggerSync(knownSyncTaskId: UUID): IO[Unit] = {
    getSyncStatusAfter(Duration(30, TimeUnit.SECONDS))
      .flatMap {
        case (SyncStatus.Sync, _)                                                   => IO.unit
        case (SyncStatus.Syncing, syncTaskId) if knownSyncTaskId.equals(syncTaskId) => forceSync(knownSyncTaskId)
        case (SyncStatus.Syncing, syncTaskId) if !knownSyncTaskId.equals(syncTaskId) =>
          logger.info("Newer sync tasks are running, skip this one")
        case (SyncStatus.Syncing, _)   => IO.unit
        case (SyncStatus.OutOfSync, _) => forceSync(knownSyncTaskId)
      }
  }

  private def forceSync(knownSyncTaskId: UUID): IO[Unit] = {
    forceSyncCommandHandler
      .handle(ForceSyncCommand())
      .flattenEvents
      .flatTap(_ => logger.info(s"Forced sync request sent"))
      .flatMap(_ => waitForSync())
      .handleErrorWith(_ => triggerSync(knownSyncTaskId))
  }

  private def waitWhileSyncing(): IO[(SyncStatus, UUID)] = waitWhile(Syncing)
  private def waitWhileInSync(): IO[(SyncStatus, UUID)]  = waitWhile(Sync)
  private def waitWhile(expectedSyncStatus: SyncStatus): IO[(SyncStatus, UUID)] = {
    getSyncStatusAfter(Duration(1, TimeUnit.SECONDS))
      .iterateWhile(syncStatus => syncStatus._1.equals(expectedSyncStatus))
  }

  private def waitForSync(): IO[Unit] = syncStatusAttempt(300)

  private def syncStatusAttempt(attemptsLeft: Int): IO[Unit] = {
    if attemptsLeft <= 0 then IO.raiseError(new TimeoutException("Reached max attempts"))
    else
      getSyncStatusAfter(Duration(1, TimeUnit.SECONDS))
        .flatMap(syncStatus => {
          if syncStatus.equals(SyncStatus.Sync) then IO.unit
          else if syncStatus.equals(SyncStatus.OutOfSync) then IO.raiseError(NackException())
          else syncStatusAttempt(attemptsLeft - 1)
        })
  }

  private def getSyncStatusAfter(duration: Duration): IO[(SyncStatus, UUID)] = {
    syncStatusQueryHandler
      .handle(SyncStatusQuery())
      .delayBy(duration)
      .flatTap(syncStatus => logger.debug(s"syncStatus=$syncStatus"))
  }
}
