package acab.devcon0.trile.inputoutput

import java.nio.charset.StandardCharsets
import java.util.Base64
import java.util.UUID
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

import scala.concurrent.duration.Duration
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.duration.SECONDS
import scala.util.Try

import acab.devcon0.trile.configuration.P2pConfiguration
import acab.devcon0.trile.domain.codecs.P2pCodecs
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Data
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Metadata
import acab.devcon0.trile.domain.ports.inputoutput.P2pBackend
import cats.effect._
import com.google.protobuf.ByteString
import io.circe.Encoder
import io.libp2p.core.Host
import io.libp2p.core.PeerId
import io.libp2p.core.crypto.PrivKey
import io.libp2p.core.dsl.BuilderJ
import io.libp2p.core.dsl.HostBuilder
import io.libp2p.core.multiformats.Multiaddr
import io.libp2p.core.mux.StreamMuxerProtocol
import io.libp2p.core.pubsub.Topic
import io.libp2p.crypto.keys.Ed25519Kt
import io.libp2p.pubsub.DefaultPubsubMessage
import io.libp2p.pubsub.gossip.Gossip
import io.libp2p.pubsub.gossip.GossipParams
import io.libp2p.pubsub.gossip.GossipRouter
import io.libp2p.pubsub.gossip.builders.GossipRouterBuilder
import io.libp2p.security.noise.NoiseXXSecureChannel
import io.libp2p.transport.tcp.TcpTransport
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger
import pubsub.pb.Rpc
import pubsub.pb.Rpc.Message

class P2pBackendImpl(p2pConfiguration: P2pConfiguration) extends P2pBackend[IO] {

  private val privateKey: PrivKey = Ed25519Kt.unmarshalEd25519PrivateKey(
    Base64.getDecoder.decode(p2pConfiguration.privateKey)
  )

  private val p2pControllerPeerId: PeerId = PeerId.fromBase58(p2pConfiguration.controllerPeerId)

  private val p2pControllerHost: String = p2pConfiguration.controllerNetworkAddress
  private val p2pControllerPort: String = p2pConfiguration.controllerPort
  private val p2pMultiAddressString     = s"/dns4/$p2pControllerHost/tcp/$p2pControllerPort/p2p/$p2pControllerPeerId"
  private val p2pControllerAddress: Multiaddr = Multiaddr.fromString(p2pMultiAddressString)

  private val maxConnectionDuration: FiniteDuration = Duration(5, TimeUnit.SECONDS)
  private val logger: Logger[IO]                    = Slf4jLogger.getLogger[IO]
  private val gossipRouter: GossipRouter            = getRouter
  val gossip: Gossip                                = Gossip(gossipRouter)
  private val host: Host                            = getHost
  private val hostPeerIdBytes: ByteString = ByteString.copyFrom(host.getPeerId.toBase58, StandardCharsets.US_ASCII)

  override def init: IO[Unit] = {
    (for
      _ <- initHost
      _ <- connectToControllerHost
      _ <- waitForGossipConnected
    yield ()).handleErrorWith(_ => init.delayBy(Duration(1, TimeUnit.SECONDS)))
  }

  override def getGossip: Resource[IO, Gossip] = {
    Resource.make(IO(gossip))(_ => IO.unit)
  }

  override def getHostPeerId: PeerId = {
    host.getPeerId
  }

  override def publishMessage[T <: Data: Encoder](data: T, topic: Topic, metadata: Metadata): IO[Unit] = {
    publishMessageInner[T](data, topic, metadata)
      .handleErrorWith(_ =>
        connectToControllerHost >> waitForGossipConnected >> publishMessageInner[T](data, topic, metadata)
      )
  }

  private def buildRcpMessage(stringMessage: String, topic: Topic): Message = {
    Rpc.Message
      .newBuilder()
      .setFrom(hostPeerIdBytes)
      .addTopicIDs(topic.getTopic)
      .setData(ByteString.copyFrom(stringMessage, StandardCharsets.US_ASCII))
      .setSeqno(ByteString.copyFrom(UUID.randomUUID().toString, StandardCharsets.US_ASCII))
      .build()
  }

  private def publishMessageInner[T <: Data: Encoder](data: T, topic: Topic, metadata: Metadata): IO[Unit] = {
    for
      message <- P2pCodecs.Encoders.Message[T](data, metadata)
      rpcMessage = buildRcpMessage(message, topic)
      _ <- IO.fromCompletableFuture(IO(gossipRouter.publish(new DefaultPubsubMessage(rpcMessage)))).void
    yield ()
  }

  private def initHost: IO[Unit] = {
    IO(host.start().join())
      .timeout(maxConnectionDuration)
      .flatTap(_ => logger.info(s"P2P host started for peerId=${host.getPeerId}"))
      .onError(throwable => logger.error(s"Failed to start p2p host throwable=$throwable"))
      .void
  }

  private def connectToControllerHost: IO[Unit] = {
    IO.fromCompletableFuture(IO(host.getNetwork.connect(p2pControllerAddress)))
      .timeout(Duration(10, SECONDS))
      .flatTap(connection => logger.debug(s"connection=$connection"))
      .recoverWith(_ => connectToControllerHost.delayBy(Duration(10, TimeUnit.SECONDS)))
      .void
  }

  private def waitForGossipConnected: IO[Unit] = {
    waitForBooleanIO(isGossipConnected, 20)
  }

  private def waitForBooleanIO(io: IO[Boolean], attemptsLeft: Int): IO[Unit] = {
    if attemptsLeft <= 0 then IO.raiseError(new TimeoutException("Reached max attempts"))
    else
      io
        .flatTap(booleanResult => logger.debug(s"waitForBooleanIO booleanResult=$booleanResult"))
        .delayBy(Duration(500, TimeUnit.MILLISECONDS))
        .flatMap(isConnected => {
          if isConnected then IO.unit
          else waitForBooleanIO(io, attemptsLeft - 1)
        })
  }

  private def getRouter: GossipRouter = {
    val gossipRouterBuilder = GossipRouterBuilder()
    gossipRouterBuilder.setParams(GossipParams())
    gossipRouterBuilder.build()
  }

  private def getHost: Host = {
    new HostBuilder()
      .builderModifier((builderJ: BuilderJ) => {
        builderJ.identity(identityBuilder => {
          identityBuilder.setFactory(() => privateKey)
          kotlin.Unit.INSTANCE
        })
      })
      .transport(new TcpTransport(_))
      .secureChannel((privateKey, muxers) => new NoiseXXSecureChannel(privateKey, muxers))
      .muxer(() => StreamMuxerProtocol.getYamux)
      .protocol(gossip)
      .build
  }

  private def isGossipConnected: IO[Boolean] = IO {
    val hasPeers: Boolean            = Try(!gossipRouter.getPeers.isEmpty).getOrElse(false)
    val rootPeerHasInbound: Boolean  = Try(gossipRouter.getPeers.get(0).getInboundHandler).toOption.isDefined
    val rootPeerHasOutbound: Boolean = Try(gossipRouter.getPeers.get(0).getOutboundHandler).toOption.isDefined
    hasPeers && rootPeerHasInbound && rootPeerHasOutbound
  }
}
