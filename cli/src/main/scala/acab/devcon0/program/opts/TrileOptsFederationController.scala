package acab.devcon0.program.opts

import acab.devcon0.dtos.TrileCli.Defaults
import com.monovore.decline.*

import java.net.InetAddress

object TrileOptsFederationController {

  val networkAddressOpt: Opts[InetAddress] = Opts
    .option[String](
      "network-address",
      help = "Hostname where you are hosting the federation. Required for SSL certificates."
    )
    .map(InetAddress.getByName)

  val ipfsClusterReplicaFactorMaxOpt: Opts[Int] = Opts
    .option[Int](
      "ipfs-cluster-replica-factor-max",
      help = s"At most, how many soft copies should be keep in the federation."
    )
    .withDefault(Defaults.ipfsClusterReplicaFactorMax)

  val SslStagingFlag: Opts[Boolean] = Opts
    .flag(
      long = "ssl-staging",
      help = s"If set, SSL certificates will be generated for pre-production cases."
    )
    .orFalse

  val certbotFolderAbsolutePath: Opts[Option[String]] =
    Opts
      .option[String](
        long = "certbot-folder",
        help =
          "Define where are your (let's encrypt) SSL certificates located. If no value is given, trile will generate them for you."
      )
      .orNone

  val sharedDiskSpaceGbOpt: Opts[Option[Int]] =
    Opts
      .option[Int](
        long = "shared-disk-space-gb",
        help =
          s"Amount of GBs the federation will use from your machine for replication purposes (default=${Defaults.sharedDiskSpaceGb})"
      )
      .orNone

}
