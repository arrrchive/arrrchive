package acab.devcon0.program.commands

import acab.devcon0.TrileCommand
import acab.devcon0.dtos.FederationControllerUpdateInputVariables
import acab.devcon0.program.opts.TrileOptsFederation
import acab.devcon0.services.command.update.UpdateControllerCommandHandler
import acab.devcon0.services.shell.{ConfirmAndRun, Greeter, PrintLn}
import cats.effect.IO
import cats.implicits.{catsSyntaxMonadError, catsSyntaxTuple2Semigroupal}
import com.monovore.decline.*

object UpdateFederationControllerCommand {

  def apply(): Command[Unit] =
    Command(name = "controller", header = "Updates a controller installation.") {
      (
        TrileOptsFederation.federationNameOpt,
        TrileOptsFederation.assumeYesFlag
      ).mapN {
        (
            federationName,
            assumeYes
        ) =>
          Greeter(action = "update a controller")

          val inputVariables: FederationControllerUpdateInputVariables = FederationControllerUpdateInputVariables(
            federationName = federationName
          )
          printInputVariables(inputVariables)

          ConfirmAndRun(skipConfirmation = assumeYes, action = () => triggerUpdate(inputVariables))
      }
    }

  private def triggerUpdate(inputVariables: FederationControllerUpdateInputVariables): Unit = {
    UpdateControllerCommandHandler(inputVariables)
      .attemptTap(_.fold(_ => IO(println()), _ => IO(println())))
      .attemptTap(PrintLn.subStepAttemptTap("Controller update", _, true))
      .unsafeToFuture()(TrileCommand.trileRuntime)
      .wait()
  }

  private def printInputVariables(inputVariables: FederationControllerUpdateInputVariables): Unit = {
    println()
    println(s"These are you input values. Please review them: ")
    println(s"- Federation name: ${inputVariables.federationName}")
    println()
  }
}
