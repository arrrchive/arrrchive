package acab.devcon0.program.commands

import acab.devcon0.TrileCommand
import acab.devcon0.dtos.FederationControllerInstallInputVariables
import acab.devcon0.dtos.TrileCli.Environment
import acab.devcon0.dtos.TrileCli.Environment.*
import acab.devcon0.program.opts.{TrileOptsFederation, TrileOptsFederationController}
import acab.devcon0.services.command.install.InstallControllerCommandHandler
import acab.devcon0.services.shell.{ConfirmAndRun, Greeter, PrintLn}
import cats.effect.IO
import cats.implicits.*
import com.monovore.decline.*

object InstallFederationControllerCommand {

  def apply(): Command[Unit] =
    Command(name = "controller", header = "Run the installation for a controller.") {
      (
        TrileOptsFederation.environmentOpt.map(environmentString => {
          if environmentString == "dev" then Dev else Production
        }),
        TrileOptsFederation.federationNameOpt,
        TrileOptsFederationController.networkAddressOpt,
        TrileOptsFederation.assumeYesFlag,
        TrileOptsFederationController.SslStagingFlag,
        TrileOptsFederationController.certbotFolderAbsolutePath,
        TrileOptsFederationController.ipfsClusterReplicaFactorMaxOpt,
        TrileOptsFederationController.sharedDiskSpaceGbOpt
      ).mapN {
        (
            environment,
            federationName,
            networkAddress,
            assumeYes,
            sslStaging,
            certbotFolderAbsolutePathOptional,
            ipfsClusterReplicaFactorMax,
            sharedDiskSpaceGb
        ) =>

          Greeter(action = s"create the '$federationName' federation")

          val federationControllerInstallInputVariables: FederationControllerInstallInputVariables =
            FederationControllerInstallInputVariables(
              environment,
              federationName,
              networkAddress,
              generateSSL = environment.equals(Environment.Production) && certbotFolderAbsolutePathOptional.isEmpty,
              stagingSSL = if sslStaging then "1" else "0",
              certbotFolderAbsolutePathOptional = certbotFolderAbsolutePathOptional,
              ipfsClusterReplicaFactorMax,
              sharedDiskSpaceGb
            )
          printInputVariables(federationControllerInstallInputVariables)

          ConfirmAndRun(
            skipConfirmation = assumeYes,
            action = () => triggerInstallation(federationControllerInstallInputVariables)
          )
      }
    }

  private def triggerInstallation(
      federationControllerInstallInputVariables: FederationControllerInstallInputVariables
  ): Unit = {
    InstallControllerCommandHandler(federationControllerInstallInputVariables)
      .attemptTap(_.fold(_ => IO(println()), _ => IO(println())))
      .attemptTap(PrintLn.subStepAttemptTap("Controller installation", _, true))
      .unsafeToFuture()(TrileCommand.trileRuntime)
      .wait()
  }

  private def printInputVariables(inputVariables: FederationControllerInstallInputVariables): Unit = {
    println()
    println(s"These are you input values. Please review them: ")
    println(s"- Environment: ${inputVariables.environment}")
    println(s"- Federation name: ${inputVariables.federationName}")
    println(s"- Network address: ${inputVariables.networkAddress.getHostName}")
    println(s"- Ipfs Cluster replica factor maximum: ${inputVariables.ipfsClusterReplicaFactorMax}")
    println()
  }
}
