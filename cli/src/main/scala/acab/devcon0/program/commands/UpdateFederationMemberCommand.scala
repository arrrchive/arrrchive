package acab.devcon0.program.commands

import acab.devcon0.TrileCommand
import acab.devcon0.dtos.FederationMemberUpdateInputVariables
import acab.devcon0.program.opts.{TrileOptsFederation, TrileOptsFederationMember}
import acab.devcon0.services.command.update.UpdateMemberCommandHandler
import acab.devcon0.services.shell.{ConfirmAndRun, Greeter, PrintLn}
import cats.effect.IO
import cats.implicits.{catsSyntaxMonadError, catsSyntaxTuple3Semigroupal}
import com.monovore.decline.*

object UpdateFederationMemberCommand {

  def apply(): Command[Unit] =
    Command(name = "member", header = "Updates a member installation.") {
      (
        TrileOptsFederation.federationNameOpt,
        TrileOptsFederationMember.nicknameOpt,
        TrileOptsFederation.assumeYesFlag
      ).mapN {
        (
            federationName,
            nickname,
            assumeYes
        ) =>
          Greeter(action = "update a member")

          val inputVariables: FederationMemberUpdateInputVariables = FederationMemberUpdateInputVariables(
            federationName = federationName,
            nickname = nickname
          )
          printInputVariables(inputVariables)

          ConfirmAndRun(skipConfirmation = assumeYes, action = () => triggerUpdate(inputVariables))
      }
    }

  private def triggerUpdate(inputVariables: FederationMemberUpdateInputVariables): Unit = {
    UpdateMemberCommandHandler(inputVariables)
      .attemptTap(_.fold(_ => IO(println()), _ => IO(println())))
      .attemptTap(PrintLn.subStepAttemptTap("Member update", _, true))
      .unsafeToFuture()(TrileCommand.trileRuntime)
      .wait()
  }

  private def printInputVariables(inputVariables: FederationMemberUpdateInputVariables): Unit = {
    println()
    println(s"These are you input values")
    println(s"- Federation name: ${inputVariables.federationName}")
    println(s"- Nickname: ${inputVariables.nickname}")
    println()
  }
}
