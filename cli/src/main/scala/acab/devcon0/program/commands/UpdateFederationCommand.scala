package acab.devcon0.program.commands

import acab.devcon0.TrileCommand
import acab.devcon0.dtos.FederationUpdateInputVariables
import acab.devcon0.program.opts.TrileOptsFederation
import acab.devcon0.services.command.update.UpdateFederationCommandHandler
import acab.devcon0.services.shell.{ConfirmAndRun, Greeter, PrintLn}
import cats.effect.IO
import cats.implicits.{catsSyntaxMonadError, catsSyntaxTuple2Semigroupal}
import com.monovore.decline.*

object UpdateFederationCommand {

  def apply(): Command[Unit] =
    Command(name = "all", header = "Updates controllers & members present in the machine.") {
      (
        TrileOptsFederation.federationNameOpt,
        TrileOptsFederation.assumeYesFlag
      ).mapN {
        (
            federationName,
            assumeYes
        ) =>
          Greeter(action = "update a federation")
          ConfirmAndRun(skipConfirmation = assumeYes, action = () => triggerUpdate(federationName))
      }
    }

  private def triggerUpdate(federationName: String): Unit = {
    val inputVariables: FederationUpdateInputVariables = FederationUpdateInputVariables(
      federationName = federationName
    )
    UpdateFederationCommandHandler(inputVariables)
      .attemptTap(PrintLn.subStepAttemptTap("Federation update", _, true))
      .unsafeToFuture()(TrileCommand.trileRuntime)
      .wait()
  }
}
