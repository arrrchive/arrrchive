package acab.devcon0.program.commands

import acab.devcon0.program.opts.{TrileOptsFederation, TrileOptsFederationMember}
import acab.devcon0.services.command.status.StatusCommandHandler
import cats.implicits.catsSyntaxTuple3Semigroupal
import com.monovore.decline.*

object StatusFederationMemberCommand {

  def apply(): Command[Unit] = {
    Command(name = "member", header = "How's the trile member doing?") {
      (
        TrileOptsFederation.federationNameOpt,
        TrileOptsFederationMember.nicknameOpt,
        TrileOptsFederation.watchFlag
      ).mapN {
        (
            federationName,
            nickname,
            watch
        ) =>
          {
            if watch then StatusCommandHandler.runWatch(federationName, nickname)
            else StatusCommandHandler.run(federationName, nickname)
          }
      }
    }
  }
}
