package acab.devcon0.program.commands

import com.monovore.decline.*

object StatusCommand {

  val command: Command[Unit] =
    Command(name = "status", header = "How's it trile doing?") {
      Opts.subcommands(StatusFederationMemberCommand())
    }
}
