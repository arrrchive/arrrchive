package acab.devcon0.program.commands

import com.monovore.decline.*

object UpdateCommand {

  val command: Command[Unit] =
    Command(name = "update", header = "Updates a pre-existing installation") {
      Opts.subcommands(UpdateFederationControllerCommand(), UpdateFederationMemberCommand(), UpdateFederationCommand())
    }
}
