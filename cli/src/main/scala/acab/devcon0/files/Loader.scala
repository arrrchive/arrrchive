package acab.devcon0.files

import acab.devcon0.dtos.TrileCli.Component.{FederationController, FederationMember}
import acab.devcon0.dtos.TrileCli.Environment.*
import acab.devcon0.dtos.TrileCli.File.{SwarmKey, *}
import acab.devcon0.dtos.TrileCli.{Component, Environment, File}
import acab.devcon0.files.templates.*

object Loader {

  def get(file: File, component: Component, environment: Environment): String = {
    (file, component, environment) match
      case (SwarmKey, _, _)                                    => templates.SwarmKey()
      case (DockerCompose, FederationMember, Dev)              => dev.federationmember.DockerCompose()
      case (DockerCompose, FederationMember, Production)       => production.federationmember.DockerCompose()
      case (DockerCompose, FederationController, Dev)          => dev.federationcontroller.DockerCompose()
      case (DockerCompose, FederationController, Production)   => production.federationcontroller.DockerCompose()
      case (NginxConf, FederationController, Production)       => production.federationcontroller.NginxConf()
      case (NginxPreSslConf, FederationController, Production) => production.federationcontroller.NginxPreSslConf()
      case (SslCertificate, FederationController, Production)  => production.federationcontroller.SslCertificate()
      case _                                                   => ""
  }
}
