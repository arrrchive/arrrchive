package acab.devcon0.files.templates.production.federationmember

object DockerCompose {
  def apply(): String = """

services:

  ${TRILE_FEDERATION_MEMBER_BACKEND_SERVICE_NAME}:
    container_name: ${TRILE_BACKEND_DOCKER_CONTAINER_NAME}
    image: triledotlink/federation-member-backend:0.1.0-SNAPSHOT
    environment:
      TRILE_FEDERATION_CONTROLLER_P2P_PEER_ID: ${TRILE_FEDERATION_CONTROLLER_P2P_PEER_ID}
      TRILE_FEDERATION_CONTROLLER_P2P_PORT: ${TRILE_FEDERATION_CONTROLLER_P2P_PORT}
      TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS: ${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS}
      TRILE_FEDERATION_MEMBER_BACKEND_PORT: 9999
      TRILE_FEDERATION_MEMBER_IPFS_LOGGING: INFO
      TRILE_FEDERATION_MEMBER_IPFS_CLUSTER_API_URL: http://${TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_NAME}:9094/
      TRILE_FEDERATION_MEMBER_IPFS_API_URL: http://${TRILE_IPFS_DOCKER_CONTAINER_NAME}:5001/api/v0/
      TRILE_FEDERATION_MEMBER_IPFS_WATCHING_DIRECTORY_IPFS_POV: /data/staging
      TRILE_FEDERATION_MEMBER_IPFS_DOCKER_CONTAINER_NAME: ${TRILE_IPFS_DOCKER_CONTAINER_NAME}
      TRILE_FEDERATION_MEMBER_NICKNAME: '${TRILE_FEDERATION_MEMBER_NICKNAME}'
      TRILE_FEDERATION_MEMBER_P2P_PRIVATE_KEY: ${TRILE_FEDERATION_MEMBER_P2P_PRIVATE_KEY}
      TRILE_FEDERATION_MEMBER_SHARED_FOLDER: /data/watching-directory
      TRILE_FEDERATION_P2P_SWARM_KEY_VALUE: ${TRILE_FEDERATION_IPFS_SWARM_KEY_VALUE}
    restart: unless-stopped
    depends_on:
      trile_federation_member_${TRILE_FEDERATION_MEMBER_NICKNAME}_ipfs:
        condition: service_healthy
      trile_federation_member_${TRILE_FEDERATION_MEMBER_NICKNAME}_ipfs_cluster:
        condition: service_started
    ports:
      - "9999"
    deploy:
      resources:
        limits:
          memory: 512M
        reservations:
          memory: 512M
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ${TRILE_FEDERATION_MEMBER_SHARING_FOLDER}:/data/watching-directory
    user: '${HOST_UID}:${HOST_GID}'

  trile_federation_member_${TRILE_FEDERATION_MEMBER_NICKNAME}_ipfs_cluster:
    container_name: ${TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_NAME}
    image: ipfs/ipfs-cluster:v1.0.8
    restart: unless-stopped
    depends_on:
      trile_federation_member_${TRILE_FEDERATION_MEMBER_NICKNAME}_ipfs:
        condition: service_healthy
    deploy:
      resources:
        limits:
          memory: 256M
        reservations:
          memory: 256M
    environment:
      CLUSTER_CRDT_CLUSTERNAME: '${TRILE_FEDERATION_NAME}'
      CLUSTER_FOLLOWERMODE: 'true'
      CLUSTER_PEERNAME: '${TRILE_FEDERATION_MEMBER_IPFS_CLUSTER_NAME}'
      CLUSTER_RESTAPI_HTTPLISTENMULTIADDRESS: /ip4/0.0.0.0/tcp/9094 # Expose API
      CLUSTER_SECRET: '${TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_SECRET}'
      CLUSTER_IPFSHTTP_NODEMULTIADDRESS: '/dns4/${TRILE_IPFS_DOCKER_CONTAINER_NAME}/tcp/5001'
      CLUSTER_CRDT_TRUSTEDPEERS: '*'
      CLUSTER_MONITORPINGINTERVAL: 2s
      CLUSTER_PEERADDRESSES: '${TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_ADDRESS}'
    command:
      - "daemon --bootstrap ${TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_ADDRESS}"
    volumes:
      - ${TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_DATA_DIRECTORY}:/data/ipfs-cluster

  trile_federation_member_${TRILE_FEDERATION_MEMBER_NICKNAME}_ipfs:
    container_name: ${TRILE_IPFS_DOCKER_CONTAINER_NAME}
    image: triledotlink/federation-member-ipfs:v0.27.0
    restart: unless-stopped
    deploy:
      resources:
        limits:
          memory: 256M
        reservations:
          memory: 256M
    environment:
      TRILE_FEDERATION_CONTROLLER_IPFS_PEER_ID: "${TRILE_FEDERATION_CONTROLLER_IPFS_PEER_ID}"
      TRILE_FEDERATION_CONTROLLER_IPFS_ADDRESS: "${TRILE_FEDERATION_CONTROLLER_IPFS_ADDRESS}"
      TRILE_FEDERATION_MEMBER_MAX_DISK_SPACE_GB: ${TRILE_FEDERATION_MEMBER_MAX_DISK_SPACE_GB}
    volumes:
      - ${TRILE_IPFS_DOCKER_CONTAINER_DATA_DIRECTORY}:/data/ipfs
      - ${TRILE_FEDERATION_MEMBER_SHARING_FOLDER}:/data/staging
""".stripMargin

}
