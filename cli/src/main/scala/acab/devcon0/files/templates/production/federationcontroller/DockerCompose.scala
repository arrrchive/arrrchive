package acab.devcon0.files.templates.production.federationcontroller

object DockerCompose {
  def apply(): String = """
services:

  ${TRILE_FEDERATION_CONTROLLER_CERTBOT_SERVICE_NAME}:
    image: certbot/certbot
    container_name: ${TRILE_FEDERATION_CONTROLLER_CERTBOT_CONTAINER_NAME}
    restart: unless-stopped
    volumes:
      - ${TRILE_IPFS_DOCKER_CONTAINER_DATA_CERTBOT_DIRECTORY}/conf:/etc/letsencrypt
      - ${TRILE_IPFS_DOCKER_CONTAINER_DATA_CERTBOT_DIRECTORY}/www:/var/www/certbot
    entrypoint: "/bin/sh -c 'trap exit TERM; while :; do certbot renew; sleep 12h & wait $${!}; done;'"

  ${TRILE_FEDERATION_CONTROLLER_REVERSE_PROXY_SERVICE_NAME}:
    image: nginx:1.25.4-alpine
    container_name: ${TRILE_FEDERATION_CONTROLLER_REVERSE_PROXY_CONTAINER_NAME}
    depends_on:
      - trile_federation_controller_frontend
    ports:
        - "80:80"
        - "443:443"
    restart: always
    volumes:
      - ${TRILE_FEDERATION_CONTROLLER_REVERSE_PROXY_CONFIGURATION}:/etc/nginx/nginx.conf
      - ${TRILE_IPFS_DOCKER_CONTAINER_DATA_CERTBOT_DIRECTORY}/conf:/etc/letsencrypt
      - ${TRILE_IPFS_DOCKER_CONTAINER_DATA_CERTBOT_DIRECTORY}/www:/var/www/certbot
    command: "/bin/sh -c 'while :; do sleep 6h & wait $${!}; nginx -s reload; done & nginx -g \"daemon off;\"'"
    deploy:
      resources:
        limits:
          memory: 128M
        reservations:
          memory: 128M

  trile_federation_controller_frontend:
    image: triledotlink/federation-controller-frontend:1.0
    container_name: ${TRILE_FRONTEND_DOCKER_CONTAINER_NAME}
    depends_on:
      - ${TRILE_FEDERATION_CONTROLLER_BACKEND_SERVICE_NAME}
    deploy:
      resources:
        limits:
          memory: 1024M
        reservations:
          memory: 1024M
    restart: unless-stopped
    environment:
      REACT_APP_TRILE_BACKEND_URL: https://api.${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS}
      REACT_APP_TRILE_IPFS_GATEWAY_URL: https://gateway.ipfs.${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS}
      REACT_APP_FEDERATION_NAME: '${TRILE_FEDERATION_NAME}'

  ${TRILE_FEDERATION_CONTROLLER_BACKEND_SERVICE_NAME}:
    image: triledotlink/federation-controller-backend:0.1.0-SNAPSHOT
    container_name: ${TRILE_BACKEND_DOCKER_CONTAINER_NAME}
    depends_on:
      trile_federation_controller_ipfs:
        condition: service_healthy
      trile_federation_controller_ipfs_cluster:
        condition: service_started
      trile_federation_controller_redis_stack:
        condition: service_started
    deploy:
      resources:
        limits:
          memory: 1024M
        reservations:
          memory: 1024M
    restart: unless-stopped
    ports:
      - "${TRILE_FEDERATION_CONTROLLER_P2P_PORT}:${TRILE_FEDERATION_CONTROLLER_P2P_PORT}"
    environment:
      TRILE_FEDERATION_CONTROLLER_IPFS_API_URL: http://${TRILE_IPFS_DOCKER_CONTAINER_NAME}:5001/api/v0/
      TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_API_URL: http://${TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_NAME}:9094/
      TRILE_FEDERATION_CONTROLLER_IPFS_LOGGING: INFO
      TRILE_FEDERATION_CONTROLLER_IPFS_SWARM_KEY_VALUE: ${TRILE_FEDERATION_IPFS_SWARM_KEY_VALUE}
      TRILE_FEDERATION_CONTROLLER_P2P_PRIVATE_KEY: ${TRILE_FEDERATION_CONTROLLER_P2P_PRIVATE_KEY}
      TRILE_FEDERATION_CONTROLLER_REDIS_HOST: trile-federation-controller-redis-stack
      TRILE_FEDERATION_CONTROLLER_JOIN_TOKEN: ${TRILE_FEDERATION_CONTROLLER_JOIN_TOKEN}
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock

  trile_federation_controller_ipfs_cluster:
    container_name: ${TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_NAME}
    image: ipfs/ipfs-cluster:v1.0.8
    depends_on:
      trile_federation_controller_ipfs:
        condition: service_healthy
    deploy:
      resources:
        limits:
          memory: 256M
        reservations:
          memory: 256M
    restart: unless-stopped
    environment:
      CLUSTER_ANNOUNCEMULTIADDRESS: '${TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_ADDRESS}'
      CLUSTER_CRDT_CLUSTERNAME: '${TRILE_FEDERATION_NAME}'
      CLUSTER_CRDT_TRUSTEDPEERS: '*'
      CLUSTER_IPFSHTTP_NODEMULTIADDRESS: /dns4/${TRILE_IPFS_DOCKER_CONTAINER_NAME}/tcp/5001
      CLUSTER_MONITORPINGINTERVAL: 2s
      CLUSTER_PEERNAME: ${TRILE_FEDERATION_NAME}-controller
      CLUSTER_RESTAPI_HTTPLISTENMULTIADDRESS: /ip4/0.0.0.0/tcp/9094 # Expose API
      CLUSTER_PIN_ONLY_ON_TRUSTED_PEERS: 'false'
      CLUSTER_REPLICATIONFACTORMIN: 1
      CLUSTER_REPLICATIONFACTORMAX: ${TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_REPLICA_FACTOR_MAX}
    ports:
      - "${TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_SWARM_PORT}:9096"
    volumes:
      - ${TRILE_FEDERATION_CONTROLLER_CONFIGURATION_FOLDER}/ipfs-cluster:/data/ipfs-cluster

  trile_federation_controller_ipfs:
    container_name: ${TRILE_IPFS_DOCKER_CONTAINER_NAME}
    image: triledotlink/federation-controller-ipfs:v0.27.0
    deploy:
      resources:
        limits:
          memory: 1024M
        reservations:
          memory: 1024M
    restart: unless-stopped
    environment:
      IPFS_LOGGING: INFO
      TRILE_FEDERATION_CONTROLLER_IPFS_ADDRESS: ${TRILE_FEDERATION_CONTROLLER_IPFS_ADDRESS}
      TRILE_FEDERATION_CONTROLLER_IPFS_MAX_DISK_SPACE_GB: ${TRILE_FEDERATION_CONTROLLER_IPFS_MAX_DISK_SPACE_GB}
    ports:
      - "${TRILE_FEDERATION_CONTROLLER_IPFS_SWARM_PORT}:4001"
    volumes:
      - ${TRILE_FEDERATION_CONTROLLER_CONFIGURATION_FOLDER}/ipfs:/data/ipfs

  trile_federation_controller_redis_stack:
    image: redis/redis-stack:7.2.0-v9
    container_name: trile-federation-controller-redis-stack
    restart: unless-stopped
    command: ["redis-stack-server", "--appendonly", "yes", "--protected-mode", "no", "--save", "20", "1", "--dir", "/data"]
    volumes:
      - ${TRILE_FEDERATION_CONTROLLER_CONFIGURATION_FOLDER}/redis-stack:/data
    deploy:
      resources:
        limits:
          memory: 256M
        reservations:
          memory: 256M
""".stripMargin

}
