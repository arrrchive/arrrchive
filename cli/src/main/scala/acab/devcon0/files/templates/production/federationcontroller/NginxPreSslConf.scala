package acab.devcon0.files.templates.production.federationcontroller

object NginxPreSslConf {
  def apply(): String = """worker_processes 1;

events { worker_connections 1024; }

http {

    sendfile on;

    upstream backend-upstream {
        server ${TRILE_BACKEND_DOCKER_CONTAINER_NAME}:8078;
    }

    upstream ipfs-gateway-upstream {
        server ${TRILE_IPFS_DOCKER_CONTAINER_NAME}:8080;
    }

    upstream frontend-upstream {
        server ${TRILE_FRONTEND_DOCKER_CONTAINER_NAME}:3000;
    }

    server {
        listen 80;
        server_name ${TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS};
        server_tokens off;

        location /.well-known/acme-challenge/ {
            root /var/www/certbot;
        }

        location / {
            return 301 https://${DOLLAR}host${DOLLAR}request_uri;
        }

    }
}
""".stripMargin

}
