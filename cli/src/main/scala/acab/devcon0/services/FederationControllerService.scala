package acab.devcon0.services

import acab.devcon0.dtos.TrileCli
import acab.devcon0.dtos.TrileCli.Environment
import acab.devcon0.dtos.TrileCli.EnvironmentVariables.TrileFederationEnvVars
import acab.devcon0.dtos.aliases.P2pPeerId
import acab.devcon0.services.shell.PrintLn
import cats.effect.IO
import cats.implicits.catsSyntaxMonadError
import sttp.client4.curl.CurlBackend
import sttp.client4.{Response, SyncBackend, UriContext, quickRequest}
import sttp.model.{HeaderNames, StatusCode, Uri}

import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration

object FederationControllerService {
  private val backend: SyncBackend = CurlBackend()

  def getFedvars(environment: Environment, networkAddress: String, joinToken: String): IO[TrileFederationEnvVars] = {
    val uri: Uri = getFedvarsUri(environment, networkAddress)
    triggerFedvarsRequest(uri, joinToken)
      .map(
        _.split("\\\\n")
          .map { line =>
            val Array(key, value) = line.split("=", 2)
            key -> value
          }
          .toMap
      )
      .attemptTap(_.fold(throwable => IO(println(throwable.getMessage + uri)), result => IO(println(result))))
  }

  def getPeerId(environment: Environment, networkAddress: String): IO[P2pPeerId] = {
    val uri: Uri = getIdUri(environment, networkAddress)
    getP2pPeerIdInner(uri, 300)
      .attemptTap(PrintLn.subStepAttemptTap("Fetching P2P peer id", _))
  }

  private def getFedvarsUri(environment: Environment, networkAddress: P2pPeerId): Uri = {
    environment match
      case TrileCli.Environment.Dev        => uri"http://$networkAddress:8078/api/federation/.fedvars"
      case TrileCli.Environment.Production => uri"https://api.$networkAddress/api/federation/.fedvars"
  }

  private def getIdUri(environment: Environment, networkAddress: P2pPeerId): Uri = {
    environment match
      case TrileCli.Environment.Dev        => uri"http://$networkAddress:8078/api/p2p/id"
      case TrileCli.Environment.Production => uri"https://api.$networkAddress/api/p2p/id"
  }

  private def getP2pPeerIdInner(uri: Uri, attemptsLeft: Int): IO[String] = {
    if attemptsLeft <= 0 then IO.raiseError(new IllegalArgumentException())
    else {
      triggerRequest(uri)
        .recoverWith(_ => IO.unit.delayBy(Duration(1, TimeUnit.SECONDS)) >> getP2pPeerIdInner(uri, attemptsLeft - 1))
    }
  }

  private def triggerFedvarsRequest(uri: Uri, joinToken: String): IO[String] = {
    triggerRequest(IO(quickRequest.get(uri).header(HeaderNames.Authorization, s"Bearer $joinToken").send(backend)))
  }

  private def triggerRequest(uri: Uri): IO[String] = {
    triggerRequest(IO(quickRequest.get(uri).send(backend)))
  }

  private def triggerRequest(value: IO[Response[String]]): IO[String] = {
    value
      .flatMap(response => {
        if response.code.equals(StatusCode.Ok) then IO(response)
        else {
          println(response)
          IO.raiseError(new IllegalArgumentException())
        }
      })
      .map(_.body)
      .map(_.stripPrefix("\"").stripSuffix("\""))

  }
}
