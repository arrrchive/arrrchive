package acab.devcon0.services.command.install

import acab.devcon0.dtos.*
import acab.devcon0.dtos.TrileCli.Component.*
import acab.devcon0.dtos.TrileCli.EnvironmentVariables.*
import acab.devcon0.dtos.TrileCli.File.*
import acab.devcon0.dtos.TrileCli.{Component, Defaults, Environment, File}
import acab.devcon0.services.shell.*
import acab.devcon0.services.{FederationControllerEnvironmentVariables, FederationControllerService}
import acab.devcon0.{files, services}
import cats.effect.IO
import cats.implicits.catsSyntaxMonadError
import ujson.Value.Value

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}
import scala.io.Source

object InstallControllerCommandHandler {

  def apply(inputVariables: FederationControllerInstallInputVariables): IO[Unit] = {
    val environmentVariables: TrileFederationEnvVars = FederationControllerEnvironmentVariables(inputVariables)
    val configurationFolderAbsolutePath              = getConfigurationFolderAbsolutePath(environmentVariables)
    val dockerComposeAbsolutePath                    = getDockerComposeAbsolutePath(configurationFolderAbsolutePath)

    generateConfigurationFiles(inputVariables, environmentVariables) >>
      DockerComposeUp(dockerComposeAbsolutePath) >>
      IO.unit.flatMap(_ =>
        if inputVariables.generateSSL then generateSslCertificate(environmentVariables)
        else IO.unit
      ) >>
      generateConfigurationFilesPostSsl(inputVariables, environmentVariables) >>
      DockerComposeUp(dockerComposeAbsolutePath) >>
      generateFedVars(inputVariables, environmentVariables) >>
      generateInstallationEnvVars(environmentVariables) >>
      printJoinToken(environmentVariables)
  }

  private def printJoinToken(environmentVariables: TrileFederationEnvVars): IO[Unit] = {
    PrintLn.subStepInfo(s"Federation join token: ${environmentVariables(TFC_JOIN_TOKEN)}")
  }

  private def generateFedVars(
      inputVariables: FederationControllerInstallInputVariables,
      environmentVariables: TrileFederationEnvVars
  ): IO[Unit] = {
    val networkAddress: String   = environmentVariables(TFC_NETWORK_ADDRESS)
    val environment: Environment = inputVariables.environment
    for peerId <- FederationControllerService.getPeerId(environment, networkAddress)
    yield {
      val fedvarsAbsolutePath = environmentVariables(TFC_FEDVARS_ABSOLUTE_PATH)
      val map: TrileFederationEnvVars = Map(
        TRILE_FEDERATION_IPFS_SWARM_KEY_VALUE -> getIpfsSwarmKeyValue(environmentVariables),
        TFC_NETWORK_ADDRESS                   -> networkAddress,
        TRILE_FEDERATION_NAME                 -> environmentVariables(TRILE_FEDERATION_NAME),
        TFC_IPFS_PEER_ID                      -> getIpfsPeerId(environmentVariables),
        TFC_IPFS_CLUSTER_PEER_ID              -> getIpfsClusterPeerId(environmentVariables),
        TFC_IPFS_CLUSTER_SECRET               -> getIpfsClusterSecret(environmentVariables),
        TFC_IPFS_CLUSTER_SWARM_PORT           -> Defaults.ipfsClusterSwarmPort,
        TFC_IPFS_CLUSTER_SWARM_PORT           -> Defaults.ipfsClusterSwarmPort,
        TFC_IPFS_SWARM_PORT                   -> Defaults.ipfsSwarmPort,
        TFC_P2P_PEER_ID                       -> peerId,
        TFC_P2P_PORT                          -> "7078"
      )

      val fedVarsContent: String = map.view.toList.map { case (key, value) => s"$key=$value" }.mkString("\n")
      Files.write(Paths.get(fedvarsAbsolutePath), fedVarsContent.getBytes(StandardCharsets.UTF_8))
    }
  }.attemptTap(PrintLn.subStepAttemptTap("Saving .fedvars", _)).void

  private def generateInstallationEnvVars(envVars: TrileFederationEnvVars): IO[Unit] = IO {
    val configurationFolderAbsolutePath = getConfigurationFolderAbsolutePath(envVars)
    val installationEnvVarsAbsolutePath = s"$configurationFolderAbsolutePath/.installationEnvVars"

    val envVarsContent: String = envVars
      .updated(TRILE_FEDERATION_IPFS_SWARM_KEY_VALUE, getIpfsSwarmKeyValue(envVars))
      .view
      .toList
      .map { case (key, value) => s"$key=$value" }
      .mkString("\n")
    Files.write(Paths.get(installationEnvVarsAbsolutePath), envVarsContent.getBytes(StandardCharsets.UTF_8))
  }.attemptTap(PrintLn.subStepAttemptTap("Saving configuration", _)).void

  private def getIpfsPeerId(envVars: TrileFederationEnvVars): String = {
    val ipfsConfigurationFolderAbsolutePath = getIpfsConfigurationFolderAbsolutePath(envVars)
    val ipfsConfigSource                    = Source.fromFile(s"$ipfsConfigurationFolderAbsolutePath/config")
    val ipfsJsonString: String              = ipfsConfigSource.getLines.mkString
    val ipfsJson: Value                     = ujson.read(ipfsJsonString)
    ipfsConfigSource.close()
    ipfsJson("Identity")("PeerID").str
  }

  private def getIpfsClusterSecret(envVars: TrileFederationEnvVars): String = {
    val ipfsClusterConfigurationFolderAbsolutePath = getIpfsClusterConfigurationFolderAbsolutePath(
      envVars
    )
    val source               = Source.fromFile(s"$ipfsClusterConfigurationFolderAbsolutePath/service.json")
    val jsonAsString: String = source.getLines.mkString
    val json: Value          = ujson.read(jsonAsString)
    source.close()
    json("cluster")("secret").str
  }

  private def getIpfsClusterPeerId(envVars: TrileFederationEnvVars): String = {
    val ipfsClusterConfigurationFolderAbsolutePath = getIpfsClusterConfigurationFolderAbsolutePath(
      envVars
    )
    val source               = Source.fromFile(s"$ipfsClusterConfigurationFolderAbsolutePath/identity.json")
    val jsonAsString: String = source.getLines.mkString
    val json: Value          = ujson.read(jsonAsString)
    source.close()
    json("id").str
  }

  private def getIpfsSwarmKeyValue(envVars: TrileFederationEnvVars): String = {
    val ipfsConfigurationFolderAbsolutePath = getIpfsConfigurationFolderAbsolutePath(envVars)
    val source                              = Source.fromFile(s"${ipfsConfigurationFolderAbsolutePath}/swarm.key")
    val swarmKeyValue                       = source.getLines.toList.last
    source.close()
    swarmKeyValue
  }

  private def generateConfigurationFiles(
      inputVariables: FederationControllerInstallInputVariables,
      envVars: TrileFederationEnvVars
  ): IO[Unit] = {
    val environment                                = inputVariables.environment
    val configFolderAbsolutePath                   = getConfigurationFolderAbsolutePath(envVars)
    val ipfsConfigurationFolderAbsolutePath        = getIpfsConfigurationFolderAbsolutePath(envVars)
    val ipfsClusterConfigurationFolderAbsolutePath = getIpfsClusterConfigurationFolderAbsolutePath(envVars)
    val nginxFolderAbsolutePath                    = getNginxConfigurationFolderAbsolutePath(configFolderAbsolutePath)
    val certbotConfigurationFolderAbsolutePath     = getCertbotConfigurationFolderAbsolutePath(configFolderAbsolutePath)
    val sslCertificateGeneratorFileAbsolutePath    = getSslCertificateAbsolutePath(configFolderAbsolutePath)
    val nginxConfigurationFileAbsolutePath         = getNginxConfigurationFileAbsolutePath(nginxFolderAbsolutePath)
    val nginxPreSslConfigurationFileAbsolutePath = getNginxPreSslConfigurationFileAbsolutePath(nginxFolderAbsolutePath)
    val swarmKeyAbsolutePath                     = ipfsConfigurationFolderAbsolutePath + "/swarm.key"
    val dockerComposeAbsolutePath                = getDockerComposeAbsolutePath(configFolderAbsolutePath)
    val fedvarsAbsolutePath                      = geFedvarsAbsolutePath(envVars)

    val dockerComposeTemplateContent           = files.Loader.get(DockerCompose, FederationController, environment)
    val nginxConfTemplateContent               = files.Loader.get(NginxConf, FederationController, environment)
    val nginxPreSslConfTemplateContent         = files.Loader.get(NginxPreSslConf, FederationController, environment)
    val sslCertificateGeneratorTemplateContent = files.Loader.get(SslCertificate, FederationController, environment)

    for
      _ <- SudoRm(configFolderAbsolutePath)
      _ <- MkdirP(configFolderAbsolutePath)
      _ <- Touch(fedvarsAbsolutePath)
      _ <- MkdirP(certbotConfigurationFolderAbsolutePath)
      _ <- MkdirP(nginxFolderAbsolutePath)
      _ <- MkdirP(ipfsConfigurationFolderAbsolutePath)
      _ <- MkdirP(ipfsClusterConfigurationFolderAbsolutePath)
      _ <- IpfsGenerateSwarmKey(swarmKeyAbsolutePath)
      envVarsPreBoot = envVars
        .updated(TFC_REVERSE_PROXY_CONFIGURATION, nginxPreSslConfigurationFileAbsolutePath)
        .updated(TRILE_FEDERATION_IPFS_SWARM_KEY_VALUE, getIpfsSwarmKeyValue(envVars))
      _ <- EnvSust(envVars, dockerComposeTemplateContent, dockerComposeAbsolutePath)
      _ <- EnvSust(envVars, nginxConfTemplateContent, nginxConfigurationFileAbsolutePath)
      _ <- EnvSust(envVars, nginxPreSslConfTemplateContent, nginxPreSslConfigurationFileAbsolutePath)
      _ <- EnvSust(envVarsPreBoot, dockerComposeTemplateContent, dockerComposeAbsolutePath)
      _ <- EnvSust(envVars, sslCertificateGeneratorTemplateContent, sslCertificateGeneratorFileAbsolutePath)
      _ <- ChmodX(sslCertificateGeneratorFileAbsolutePath)
    yield ()
  }

  private def generateConfigurationFilesPostSsl(
      inputVariables: FederationControllerInstallInputVariables,
      envVars: TrileFederationEnvVars
  ): IO[Unit] = {
    val environment                     = inputVariables.environment
    val configurationFolderAbsolutePath = getConfigurationFolderAbsolutePath(envVars)
    val dockerComposeAbsolutePath       = getDockerComposeAbsolutePath(configurationFolderAbsolutePath)
    val dockerComposeTemplateContent    = files.Loader.get(DockerCompose, FederationController, environment)
    val envVarsPostSsl = envVars.updated(TRILE_FEDERATION_IPFS_SWARM_KEY_VALUE, getIpfsSwarmKeyValue(envVars))

    EnvSust(envVarsPostSsl, dockerComposeTemplateContent, dockerComposeAbsolutePath)
  }

  private def generateSslCertificate(envVars: TrileFederationEnvVars): IO[Unit] = {
    val configurationFolderAbsolutePath         = getConfigurationFolderAbsolutePath(envVars)
    val sslCertificateGeneratorFileAbsolutePath = getSslCertificateAbsolutePath(configurationFolderAbsolutePath)

    Bash(sslCertificateGeneratorFileAbsolutePath)
  }

  private def getDockerComposeAbsolutePath(configurationFolderAbsolutePath: String): String = {
    configurationFolderAbsolutePath + "/docker-compose.yaml"
  }

  private def getSslCertificateAbsolutePath(configurationFolderAbsolutePath: String): String = {
    configurationFolderAbsolutePath + "/generate-lets-encrypt-ssl-certificates"
  }

  private def getCertbotConfigurationFolderAbsolutePath(configurationFolderAbsolutePath: String): String = {
    configurationFolderAbsolutePath + "/certbot/conf"
  }

  private def getNginxConfigurationFolderAbsolutePath(configurationFolderAbsolutePath: String): String = {
    configurationFolderAbsolutePath + "/nginx"
  }

  private def getNginxConfigurationFileAbsolutePath(nginxConfigurationFolderAbsolutePath: String): String = {
    nginxConfigurationFolderAbsolutePath + "/nginx.conf"
  }

  private def getNginxPreSslConfigurationFileAbsolutePath(nginxConfigurationFolderAbsolutePath: String): String = {
    nginxConfigurationFolderAbsolutePath + "/nginx-pre-ssl.conf"
  }

  private def geFedvarsAbsolutePath(envVars: TrileFederationEnvVars): String = {
    envVars(TFC_FEDVARS_ABSOLUTE_PATH)
  }

  private def getConfigurationFolderAbsolutePath(envVars: TrileFederationEnvVars): String = {
    envVars(TFC_CONFIGURATION_FOLDER)
  }

  private def getIpfsConfigurationFolderAbsolutePath(envVars: TrileFederationEnvVars): String = {
    envVars(TRILE_IPFS_DOCKER_CONTAINER_DATA_DIRECTORY)
  }

  private def getIpfsClusterConfigurationFolderAbsolutePath(envVars: TrileFederationEnvVars): String = {
    envVars(TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_DATA_DIRECTORY)
  }
}
