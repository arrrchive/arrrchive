package acab.devcon0.services.command.install

import acab.devcon0.dtos.TrileCli.Component.*
import acab.devcon0.dtos.TrileCli.EnvironmentVariables.*
import acab.devcon0.dtos.TrileCli.File.*
import acab.devcon0.dtos.TrileCli.{Component, File}
import acab.devcon0.dtos.{FederationMemberInstallInputVariables, TrileCli}
import acab.devcon0.services.FederationMemberEnvironmentVariables
import acab.devcon0.services.shell.{DockerComposeUp, EnvSust, MkdirP, SudoRm}
import acab.devcon0.{files, services}
import cats.effect.IO

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}

object InstallMemberCommandHandler {

  def apply(
      inputVariables: FederationMemberInstallInputVariables,
      fedvarsAsMap: TrileFederationEnvVars
  ): IO[Unit] = {
    val environmentVariables: TrileFederationEnvVars = FederationMemberEnvironmentVariables(
      inputVariables = inputVariables,
      fedvars = fedvarsAsMap
    )
    val environment                                = inputVariables.environment
    val configurationFolderAbsolutePath            = getConfigurationFolderAbsolutePath(environmentVariables)
    val ipfsConfigurationFolderAbsolutePath        = getIpfsConfigurationFolderAbsolutePath(environmentVariables)
    val ipfsClusterConfigurationFolderAbsolutePath = getIpfsClusterConfigurationFolderAbsolutePath(environmentVariables)
    val federationMemberSharingFolder              = getFederationMemberSharingFolder(environmentVariables)
    val dockerComposeAbsolutePath                  = configurationFolderAbsolutePath + "/docker-compose.yaml"
    val swarmKeyAbsolutePath                       = ipfsConfigurationFolderAbsolutePath + "/swarm.key"
    val swarmKeyTemplateContent                    = files.Loader.get(SwarmKey, FederationMember, environment)
    val dockerComposeTemplateContent               = files.Loader.get(DockerCompose, FederationMember, environment)

    SudoRm(configurationFolderAbsolutePath) >>
      MkdirP(configurationFolderAbsolutePath) >>
      MkdirP(ipfsConfigurationFolderAbsolutePath) >>
      MkdirP(ipfsClusterConfigurationFolderAbsolutePath) >>
      MkdirP(federationMemberSharingFolder) >>
      EnvSust(environmentVariables, swarmKeyTemplateContent, swarmKeyAbsolutePath) >>
      EnvSust(environmentVariables, dockerComposeTemplateContent, dockerComposeAbsolutePath) >>
      DockerComposeUp(dockerComposeAbsolutePath) >>
      generateInstallationEnvVars(environmentVariables)
  }

  private def generateInstallationEnvVars(envVars: TrileFederationEnvVars): IO[Unit] = IO {
    val configurationFolderAbsolutePath = getConfigurationFolderAbsolutePath(envVars)
    val installationEnvVarsAbsolutePath = s"$configurationFolderAbsolutePath/.installationEnvVars"

    val envVarsContent: String = envVars.view.toList.map { case (key, value) => s"$key=$value" }.mkString("\n")
    Files.write(Paths.get(installationEnvVarsAbsolutePath), envVarsContent.getBytes(StandardCharsets.UTF_8))
  }

  private def getFederationMemberSharingFolder(envVars: TrileFederationEnvVars): String = {
    envVars(TFM_SHARING_FOLDER)
  }

  private def getConfigurationFolderAbsolutePath(envVars: TrileFederationEnvVars): String = {
    envVars(TFM_CONFIGURATION_FOLDER)
  }

  private def getIpfsConfigurationFolderAbsolutePath(envVars: TrileFederationEnvVars): String = {
    envVars(TRILE_IPFS_DOCKER_CONTAINER_DATA_DIRECTORY)
  }

  private def getIpfsClusterConfigurationFolderAbsolutePath(envVars: TrileFederationEnvVars): String = {
    envVars(TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_DATA_DIRECTORY)
  }
}
