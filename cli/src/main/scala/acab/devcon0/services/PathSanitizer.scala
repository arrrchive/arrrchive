package acab.devcon0.services

import acab.devcon0.dtos.TrileCli.Defaults

object PathSanitizer {

  def expandHome(path: String): String = {
    if path.startsWith("~") then path.replaceFirst("~", Defaults.userHomeAbsolutePath)
    else path
  }
}
