package acab.devcon0.services

import acab.devcon0.dtos.FederationMemberInstallInputVariables
import acab.devcon0.dtos.TrileCli.{Defaults, Environment}
import acab.devcon0.dtos.TrileCli.Environment.*
import acab.devcon0.dtos.TrileCli.EnvironmentVariables.*
import acab.devcon0.services.shell.{Ed25519KeyGeneration, GroupId, UserId}

import java.net.InetAddress

object FederationMemberEnvironmentVariables {

  def apply(
      inputVariables: FederationMemberInstallInputVariables,
      fedvars: TrileFederationEnvVars
  ): TrileFederationEnvVars = {
    val userHomeAbsolutePath: String      = PathSanitizer.expandHome(Defaults.userHomeAbsolutePath)
    val configurationPath: String         = s"$userHomeAbsolutePath/.config/trile"
    val environment                       = inputVariables.environment
    val federationName: String            = fedvars(TRILE_FEDERATION_NAME)
    val memberNickname: String            = inputVariables.nickname
    val memberFullName: String            = s"$federationName-federation-member-$memberNickname"
    val memberConfigurationFolder: String = s"$configurationPath/federations/$federationName/members/$memberNickname"
    val backendContainerName              = s"trile-$memberFullName-backend"
    val backendServiceName: String        = backendContainerName.replace('-', '_')

    fedvars
      ++ Map(
        HOST_GID                                           -> GroupId().get,
        HOST_UID                                           -> UserId().toString,
        TRILE_ENVIRONMENT                                  -> inputVariables.environment.toString,
        TRILE_CONFIGURATION_FOLDER                         -> configurationPath,
        TRILE_BACKEND_DOCKER_CONTAINER_NAME                -> s"trile-$memberFullName-backend",
        TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_DATA_DIRECTORY -> s"$memberConfigurationFolder/ipfs-cluster",
        TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_NAME           -> s"trile-$memberFullName-ipfs-cluster",
        TRILE_IPFS_DOCKER_CONTAINER_DATA_DIRECTORY         -> s"$memberConfigurationFolder/ipfs",
        TRILE_IPFS_DOCKER_CONTAINER_NAME                   -> s"trile-$memberFullName-ipfs",
        TFM_BACKEND_SERVICE_NAME                           -> backendServiceName,
        TFC_IPFS_CLUSTER_ADDRESS                           -> getIpfsClusterAddress(fedvars, environment),
        TFC_IPFS_ADDRESS                                   -> getIpfsAddress(fedvars, environment),
        TFM_CONFIGURATION_FOLDER                           -> memberConfigurationFolder,
        TFM_IPFS_CLUSTER_NAME                              -> memberFullName,
        TFM_MAX_DISK_SPACE_GB                              -> s"${inputVariables.sharedDiskSpaceGB}",
        TFM_NICKNAME                                       -> memberNickname,
        TFM_SHARING_FOLDER                                 -> inputVariables.sharingFolder,
        TFM_P2P_PRIVATE_KEY                                -> Ed25519KeyGeneration()
      )
  }

  private def getIpfsAddress(fedvars: TrileFederationEnvVars, environment: Environment): String = {
    val ipfsPort       = fedvars(TFC_IPFS_SWARM_PORT)
    val ipfsPeerId     = fedvars(TFC_IPFS_PEER_ID)
    val networkAddress = InetAddress.getByName(fedvars(TFC_NETWORK_ADDRESS))
    environment match
      case Dev        => s"/ip4/${networkAddress.getHostAddress}/tcp/$ipfsPort/p2p/$ipfsPeerId"
      case Production => s"/dns4/ipfs.${networkAddress.getHostName}/tcp/$ipfsPort/p2p/$ipfsPeerId"
  }

  private def getIpfsClusterAddress(fedvars: TrileFederationEnvVars, environment: Environment): String = {
    val ipfsClusterPort   = fedvars(TFC_IPFS_CLUSTER_SWARM_PORT)
    val ipfsClusterPeerId = fedvars(TFC_IPFS_CLUSTER_PEER_ID)
    val networkAddress    = InetAddress.getByName(fedvars(TFC_NETWORK_ADDRESS))
    environment match
      case Dev        => s"/ip4/${networkAddress.getHostAddress}/tcp/$ipfsClusterPort/p2p/$ipfsClusterPeerId"
      case Production => s"/dns4/ipfs-cluster.${networkAddress.getHostName}/tcp/$ipfsClusterPort/p2p/$ipfsClusterPeerId"
  }
}
