package acab.devcon0.services

import acab.devcon0.codecs.CirceCodecs
import acab.devcon0.dtos.TrileCli
import acab.devcon0.dtos.TrileCli.Defaults
import acab.devcon0.dtos.TrileCli.EnvironmentVariables.TrileFederationEnvVars
import acab.devcon0.dtos.http.FederationMemberStatus
import acab.devcon0.services.shell.DockerComposePort
import cats.effect.IO
import sttp.client4.curl.CurlBackend
import sttp.client4.{SyncBackend, UriContext, quickRequest}
import sttp.model.Uri

object FederationMemberService {
  private val backend: SyncBackend = CurlBackend()

  def getStatus(federationName: String, nickname: String): IO[FederationMemberStatus] = {
    IO(getUri(federationName, nickname))
      .flatMap(triggerRequest)
      .flatMap(CirceCodecs.Decoders.FederationMemberStatus.apply)
  }

  private def triggerRequest(uri: Uri): IO[String] = {
    IO(quickRequest.get(uri).send(backend).body)
  }

  private def getUri(federationName: String, nickname: String): Uri = {
    val federationEnvVars: TrileFederationEnvVars = FederationMemberLoader(federationName, nickname)
    val configurationFolder: String = federationEnvVars(TrileCli.EnvironmentVariables.TFM_CONFIGURATION_FOLDER)
    val dockerComposeFile: String   = s"$configurationFolder/docker-compose.yaml"
    val service: String             = federationEnvVars(TrileCli.EnvironmentVariables.TFM_BACKEND_SERVICE_NAME)
    val privatePort: Int            = Defaults.federationMemberBackendPrivatePort
    val backendPort: String         = DockerComposePort(dockerComposeFile, service, privatePort).get
    uri"http://localhost:$backendPort/api/status"
  }
}
