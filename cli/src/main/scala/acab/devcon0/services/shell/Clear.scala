package acab.devcon0.services.shell

import scala.scalanative.libc.stdlib
import scala.scalanative.unsafe.Zone

object Clear {
  def apply(): Unit = {
    Zone { implicit z =>
      val command = scala.scalanative.unsafe.toCString(s"clear -x")
      stdlib.system(command)
    }
  }
}
