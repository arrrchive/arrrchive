package acab.devcon0.services.shell

import java.io.File
import scala.io.Source
import scala.scalanative.libc.stdlib
import scala.scalanative.unsafe.Zone
import scala.util.{Random, Try}

object ExecAndGetStdout {
  def apply(commandStr: String, silent: Boolean = true): Try[List[String]] = {
    Zone { implicit z =>
      val randomFilename: String = s"/tmp/${Random.alphanumeric.take(64).mkString}"
      Try(if !silent then println(s"> $commandStr"))
        .map(_ => scala.scalanative.unsafe.toCString(s"$commandStr > $randomFilename"))
        .map(command => stdlib.system(command))
        .flatMap(_ => getResult(randomFilename))
        .map(result => delete(randomFilename).fold(_ => result, _ => result))
    }
  }

  private def delete(fileAbsolutePath: String): Try[Unit] = {
    Try(new File(fileAbsolutePath).delete())
  }

  private def getResult(fileAbsolutePath: String): Try[List[String]] = {
    Try(Source.fromFile(fileAbsolutePath))
      .map(source => (source, source.getLines()))
      .flatMap(tuple =>
        Try {
          val body = tuple._2.toList
          tuple._1.close()
          body
        }
      )
  }
}
