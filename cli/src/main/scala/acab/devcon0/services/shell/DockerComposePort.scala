package acab.devcon0.services.shell

import scala.scalanative.unsafe.Zone
import scala.util.Try

object DockerComposePort {
  def apply(dockerComposeFile: String, service: String, privatePort: Int): Try[String] = {
    Zone { implicit z =>
      val command: String = s"docker compose -f $dockerComposeFile port $service $privatePort"
      ExecAndGetStdout(command)
        .map(_.head.split(":").tail.head)
    }
  }
}
