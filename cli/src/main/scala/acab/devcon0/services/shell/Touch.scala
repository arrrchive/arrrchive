package acab.devcon0.services.shell

import cats.effect.IO

object Touch {

  private val cmdLabel: String = "Ensuring file exists"

  def apply(filePath: String): IO[Unit] = {
    val cmd: String = getCmd(filePath)
    val label       = s"$cmdLabel: ${filePath.split('/').lastOption.getOrElse(filePath)}"
    SubStep.withProgress(cmd, label)
  }

  private def getCmd(filePath: String): String = {
    s"touch $filePath"
  }
}
