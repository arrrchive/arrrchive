package acab.devcon0.services.shell

import cats.effect.IO

import scala.scalanative.libc.stdlib
import scala.scalanative.unsafe.{Zone, toCString}

object IpfsGenerateSwarmKey {

  private val cmdLabel: String = "Creating IPFS swarm key"

  def apply(swarmKeyAbsolutePath: String): IO[Unit] = {
    val cmd: String = getCmd(swarmKeyAbsolutePath)
    val label       = s"$cmdLabel: ${swarmKeyAbsolutePath.split('/').lastOption.getOrElse(swarmKeyAbsolutePath)}"
    SubStep.withoutProgress(cmd, label, label.length)
  }

  private def getCmd(swarmKeyAbsolutePath: String): String = {
    s"echo \"/key/swarm/psk/1.0.0/\n/base16/\n$$(hexdump -n 32 -e '16/1 \"%02x\"' /dev/urandom)\" > $swarmKeyAbsolutePath"
  }
}
