package acab.devcon0.services.shell

import cats.effect.IO

object Bash {
  private val cmdLabel: String = "Executing shell command"

  def apply(commandInput: String): IO[Unit] = {
    val cmd: String = getCmd(commandInput)
    SubStep.withoutProgress(cmd, s"$cmdLabel: $commandInput", commandInput.length)
  }

  private def getCmd(commandInput: String): String = {
    s"bash -c \"$commandInput\""
  }
}
