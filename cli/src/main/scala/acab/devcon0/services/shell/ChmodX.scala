package acab.devcon0.services.shell

import cats.effect.IO

object ChmodX {

  private val cmdLabel: String = "Granting permissions"

  def apply(path: String): IO[Unit] = {
    val cmd: String = getCmd(path)
    val label       = s"$cmdLabel: ${path.split('/').lastOption.getOrElse(path)}"
    SubStep.withoutProgress(cmd, label, label.length)
  }

  private def getCmd(path: String): String = {
    s"chmod +x $path"
  }
}
