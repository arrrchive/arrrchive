package acab.devcon0.services.shell

import scala.io.Source
import scala.scalanative.libc.stdlib
import scala.scalanative.unsafe.Zone
import scala.util.{Random, Try}

object GroupId {
  def apply(): Try[String] = {
    Zone { implicit z =>
      val command: String = s"id -g"
      ExecAndGetStdout(command)
        .map(_.head)
    }
  }

}
