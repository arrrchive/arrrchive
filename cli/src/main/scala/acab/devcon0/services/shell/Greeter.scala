package acab.devcon0.services.shell

object Greeter {

  def apply(action: String): Unit = {
    Clear()
    PrintLn.cyan(s"You are up to $action.")
    PrintLn.cyan("Run this command with --help to see the list of options. Pay attention to the mandatory ones.")
    println()
  }
}
