package acab.devcon0.services.shell

import scala.scalanative.posix.unistd.*
import scala.scalanative.posix.{grp, unistd}

object UserId {
  def apply(): Int = {
    unistd.getuid().toInt
  }
}
