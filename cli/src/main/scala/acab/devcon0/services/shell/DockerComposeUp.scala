package acab.devcon0.services.shell

import cats.effect.IO

import scala.scalanative.unsafe

object DockerComposeUp {

  private val cmdLabel = "Booting containers"

  def apply(dockerComposeAbsolutePath: String): IO[Unit] = {
    val cmd: String = getCmd(dockerComposeAbsolutePath)
    SubStep.withProgress(cmd, cmdLabel)
  }

  private def getCmd(dockerComposeAbsolutePath: String): String = {
    s"docker compose --progress=quiet -f $dockerComposeAbsolutePath up --pull always --quiet-pull --force-recreate --detach --wait"
  }
}
