package acab.devcon0.dtos

object TrileCli {

  enum Environment:
    case Dev, Production
  enum File:
    case SwarmKey, DockerCompose, NginxConf, NginxPreSslConf, SslCertificate
  enum Component:
    case FederationMember, FederationController

  object EnvironmentVariables {

    private type TrileEnvironmentVariable = String
    type TrileFederationEnvVars           = Map[TrileEnvironmentVariable, String]

    // Used in controllers & Members
    val DOLLAR                                = "DOLLAR"
    val HOST_GID                              = "HOST_GID"
    val HOST_UID                              = "HOST_UID"
    val TRILE_ENVIRONMENT                     = "TRILE_ENVIRONMENT"
    val TRILE_CONFIGURATION_FOLDER            = "TRILE_CONFIGURATION_FOLDER"
    val TRILE_FEDERATION_NAME                 = "TRILE_FEDERATION_NAME"
    val TRILE_FEDERATION_IPFS_SWARM_KEY_VALUE = "TRILE_FEDERATION_IPFS_SWARM_KEY_VALUE"
    // Used in controllers & Members, but candidates for split
    val TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_DATA_DIRECTORY = "TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_DATA_DIRECTORY"
    val TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_NAME           = "TRILE_IPFS_CLUSTER_DOCKER_CONTAINER_NAME"
    val TRILE_IPFS_DOCKER_CONTAINER_DATA_CERTBOT_DIRECTORY = "TRILE_IPFS_DOCKER_CONTAINER_DATA_CERTBOT_DIRECTORY"
    val TRILE_IPFS_DOCKER_CONTAINER_DATA_DIRECTORY         = "TRILE_IPFS_DOCKER_CONTAINER_DATA_DIRECTORY"
    val TRILE_IPFS_DOCKER_CONTAINER_NAME                   = "TRILE_IPFS_DOCKER_CONTAINER_NAME"
    val TRILE_BACKEND_DOCKER_CONTAINER_NAME                = "TRILE_BACKEND_DOCKER_CONTAINER_NAME"
    val TRILE_FRONTEND_DOCKER_CONTAINER_NAME               = "TRILE_FRONTEND_DOCKER_CONTAINER_NAME"
    // Used in controllers
    val TFC_CERTBOT_CONTAINER_NAME          = "TRILE_FEDERATION_CONTROLLER_CERTBOT_CONTAINER_NAME"
    val TFC_CERTBOT_SERVICE_NAME            = "TRILE_FEDERATION_CONTROLLER_CERTBOT_SERVICE_NAME"
    val TFC_CERTBOT_STAGING                 = "TRILE_FEDERATION_CONTROLLER_CERTBOT_STAGING"
    val TFC_CONFIGURATION_FOLDER            = "TRILE_FEDERATION_CONTROLLER_CONFIGURATION_FOLDER"
    val TFC_FEDVARS_ABSOLUTE_PATH           = "TRILE_FEDERATION_CONTROLLER_FEDVARS_ABSOLUTE_PATH"
    val TFC_JOIN_TOKEN                      = "TRILE_FEDERATION_CONTROLLER_JOIN_TOKEN"
    val TFC_IPFS_ADDRESS                    = "TRILE_FEDERATION_CONTROLLER_IPFS_ADDRESS"
    val TFC_IPFS_CLUSTER_ADDRESS            = "TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_ADDRESS"
    val TFC_IPFS_CLUSTER_PEER_ID            = "TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_PEER_ID"
    val TFC_IPFS_CLUSTER_SWARM_PORT         = "TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_SWARM_PORT"
    val TFC_IPFS_CLUSTER_SECRET             = "TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_SECRET"
    val TFC_IPFS_CLUSTER_REPLICA_FACTOR_MAX = "TRILE_FEDERATION_CONTROLLER_IPFS_CLUSTER_REPLICA_FACTOR_MAX"
    val TFC_IPFS_MAX_DISK_SPACE_GB          = "TRILE_FEDERATION_CONTROLLER_IPFS_MAX_DISK_SPACE_GB"
    val TFC_IPFS_PEER_ID                    = "TRILE_FEDERATION_CONTROLLER_IPFS_PEER_ID"
    val TFC_IPFS_SWARM_PORT                 = "TRILE_FEDERATION_CONTROLLER_IPFS_SWARM_PORT"
    val TFC_NETWORK_ADDRESS                 = "TRILE_FEDERATION_CONTROLLER_NETWORK_ADDRESS"
    val TFC_P2P_PRIVATE_KEY                 = "TRILE_FEDERATION_CONTROLLER_P2P_PRIVATE_KEY"
    val TFC_P2P_PEER_ID                     = "TRILE_FEDERATION_CONTROLLER_P2P_PEER_ID"
    val TFC_P2P_PORT                        = "TRILE_FEDERATION_CONTROLLER_P2P_PORT"
    val TFC_REVERSE_PROXY_CONFIGURATION     = "TRILE_FEDERATION_CONTROLLER_REVERSE_PROXY_CONFIGURATION"
    val TFC_REVERSE_PROXY_CONTAINER_NAME    = "TRILE_FEDERATION_CONTROLLER_REVERSE_PROXY_CONTAINER_NAME"
    val TFC_REVERSE_PROXY_SERVICE_NAME      = "TRILE_FEDERATION_CONTROLLER_REVERSE_PROXY_SERVICE_NAME"
    val TFC_BACKEND_SERVICE_NAME            = "TRILE_FEDERATION_CONTROLLER_BACKEND_SERVICE_NAME"
    // Used in members
    val TFM_CONFIGURATION_FOLDER = "TRILE_FEDERATION_MEMBER_CONFIGURATION_FOLDER"
    val TFM_IPFS_CLUSTER_NAME    = "TRILE_FEDERATION_MEMBER_IPFS_CLUSTER_NAME"
    val TFM_MAX_DISK_SPACE_GB    = "TRILE_FEDERATION_MEMBER_MAX_DISK_SPACE_GB"
    val TFM_NICKNAME             = "TRILE_FEDERATION_MEMBER_NICKNAME"
    val TFM_P2P_PRIVATE_KEY      = "TRILE_FEDERATION_MEMBER_P2P_PRIVATE_KEY"
    val TFM_SHARING_FOLDER       = "TRILE_FEDERATION_MEMBER_SHARING_FOLDER"
    val TFM_BACKEND_SERVICE_NAME = "TRILE_FEDERATION_MEMBER_BACKEND_SERVICE_NAME"
  }

  object Defaults {

    private val cliDirectoryAbsolutePath: String = System.getProperty("user.dir")

    val userHomeAbsolutePath: String            = System.getProperty("user.home")
    val ipfsSwarmPort: String                   = "4001"
    val ipfsClusterSwarmPort: String            = "9096"
    val sharedDiskSpaceGb: Int                  = 10
    val environment                             = "production"
    val nickname: String                        = userHomeAbsolutePath.split("/").last
    val federationMemberBackendPrivatePort: Int = 9999
    val trileConfigurationAbsolutePath          = s"$userHomeAbsolutePath/.config/trile"
    val ipfsClusterReplicaFactorMax             = 5
  }
}
