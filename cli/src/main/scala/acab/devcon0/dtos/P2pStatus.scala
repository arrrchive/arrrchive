package acab.devcon0.dtos

enum SyncStatus:
  case Syncing, Sync, OutOfSync

enum CheckInStatus:
  case Unknown, ACK, NACK

enum ControllerStatus:
  case Unknown, Online, Offline

final case class NackException() extends Throwable
