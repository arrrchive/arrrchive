package acab.devcon0.dtos

import acab.devcon0.dtos.TrileCli.Environment

import java.net.InetAddress

case class FederationControllerInstallInputVariables(
    environment: Environment,
    federationName: String,
    networkAddress: InetAddress,
    generateSSL: Boolean,
    stagingSSL: String,
    certbotFolderAbsolutePathOptional: Option[String],
    ipfsClusterReplicaFactorMax: Int,
    sharedDiskSpaceGb: Option[Int]
)
