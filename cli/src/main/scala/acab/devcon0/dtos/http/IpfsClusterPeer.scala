package acab.devcon0.dtos.http

import acab.devcon0.dtos.aliases.{IpfsCid, IpfsClusterPeerId, IpfsPeerId}

final case class IpfsClusterPins(pins: List[IpfsClusterPin])
final case class IpfsClusterPin(cid: IpfsCid)
final case class IpfsClusterPeers(peers: List[IpfsClusterPeer])
final case class IpfsClusterPeer(
    id: IpfsClusterPeerId,
    addresses: List[String],
    clusterPeers: List[String],
    clusterPeersAddresses: List[String],
    version: String,
    commit: String,
    rpcProtocolVersion: String,
    error: String,
    ipfs: IpfsClusterPeerIpfsInfo,
    peername: String
)
final case class IpfsClusterPeerIpfsInfo(
    id: IpfsPeerId,
    addresses: List[String],
    error: String
)
