package acab.devcon0.dtos.http

import acab.devcon0.dtos.aliases.{IpfsCid, P2pPeerId}
import acab.devcon0.dtos.{CheckInStatus, ControllerStatus, SyncStatus}

import java.time.Instant

final case class FederationMemberStatus(
    nickname: String,
    p2pPeerId: P2pPeerId,
    sharedFolderPin: IpfsCid,
    sharedFolderFileCount: Int,
    sharedFolderSizeBytes: Long,
    checkInStatus: CheckInStatus,
    syncStatus: SyncStatus,
    ipfsClusterPeers: IpfsClusterPeers,
    ipfsClusterPinsCount: Int,
    ipfsRepoSize: Long,
    ipfsStorageMax: Long,
    controllerStatus: ControllerStatus,
    controllerLastSeen: Option[Instant]
)
