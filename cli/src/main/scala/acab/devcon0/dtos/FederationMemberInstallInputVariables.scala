package acab.devcon0.dtos

import acab.devcon0.dtos.TrileCli.Environment
import acab.devcon0.dtos.TrileCli.EnvironmentVariables.TrileFederationEnvVars

case class FederationMemberInstallInputVariables(
    environment: Environment,
    nickname: String,
    fedvarsMap: TrileFederationEnvVars,
    sharedDiskSpaceGB: Int,
    sharingFolder: String
)
