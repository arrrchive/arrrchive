package acab.devcon0.dtos.aliases

type FederationMemberNickname = String
type FederationMemberId       = String
