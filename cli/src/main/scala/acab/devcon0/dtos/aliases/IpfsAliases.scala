package acab.devcon0.dtos.aliases

type IpfsCid             = String
type IpfsClusterPeerId   = String
type IpfsClusterNodeName = String
type IpfsPeerId          = String
