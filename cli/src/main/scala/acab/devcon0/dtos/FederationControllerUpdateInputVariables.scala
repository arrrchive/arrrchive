package acab.devcon0.dtos

import acab.devcon0.dtos.TrileCli.Environment

import java.net.InetAddress

case class FederationControllerUpdateInputVariables(federationName: String)
