import scala.scalanative.build.*

scalaVersion := "3.4.0"
logLevel     := Level.Info

enablePlugins(ScalaNativePlugin)

libraryDependencies ++= Seq(
  "com.lihaoyi"                   %%% "fansi"         % "0.4.0",
  "com.monovore"                  %%% "decline"       % "2.4.1",
  "org.typelevel"                 %%% "cats-effect"   % "3.4.8",
  "io.circe"                      %%% "circe-core"    % "0.14.6",
  "io.circe"                      %%% "circe-generic" % "0.14.6",
  "io.circe"                      %%% "circe-parser"  % "0.14.6",
  "com.lihaoyi"                   %%% "upickle"       % "3.2.0",
  "com.softwaremill.sttp.client4" %%% "core"          % "4.0.0-M2"
)

val customMode = sys.env.getOrElse("SCALA_NATIVE_MODE", "debug")

lazy val root = (project in file("."))
  .settings(
    name                := "trile-cli",
    Compile / mainClass := Some("acab.devcon0.TrileCommandApp"),
    nativeConfig ~= { c =>
      c.withLTO(LTO.none)
        .withMode(Mode(customMode))
        .withGC(GC.immix)
    }
  )
  .enablePlugins(ScalaNativePlugin)
