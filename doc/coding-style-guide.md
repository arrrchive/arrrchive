# Coding style guide

# Architecture & Responsibilities

## (P2P) Listeners

+ Responsible for taking the message, sending the processing order and replying to the request
+ Processing the message might imply sending other messages over the pub/subs, but the listener only knows about its own channel and sticks to that one.


## (Redis) Listeners

+ Responsible for taking the message, sending the processing order and sending the next message in the stream.
+ Processing the message might imply sending other messages over the pub/subs, but the listener only knows about its own channel and sticks to that one.


## Services

+ Calls to clients/repos/end of pipe.
+ Retries
+ Error handling
+ Logging on debug level


## Listeners & Publishers

+ Direct communication? It's okay both on Redis & P2P when it's about their own channel/topic.
+ Listeners should not hold logic/knowledge over how the messages are internally processed.


## Commands

+ They should represent an action. Not a reaction on a event.
+ If I have a check in message, the action is "Process Check in message".
+ They should be granular and have a 1:1 relation with each input type. For instance, a listener and a http endpoint might call the same command handler, but they should have the same semantic meaning & purpose.