#!/bin/sh
set -ex

ipfs bootstrap rm all
ipfs config --json Addresses.Announce "[\"${TRILE_FEDERATION_CONTROLLER_IPFS_ADDRESS}\"]"
ipfs config --json Bootstrap "[]"
ipfs config --json Experimental.FilestoreEnabled  true
ipfs config --json Experimental.GraphsyncEnabled  false
ipfs config --json Experimental.OptimisticProvide  false
ipfs config --json Gateway.Writable  false
ipfs config --json Ipns.UsePubsub true
ipfs config --json PubSub.Enabled true
ipfs config --json Routing.AcceleratedDHTClient false
ipfs config Routing.Type dhtclient
ipfs config Datastore.StorageMax ${TRILE_FEDERATION_CONTROLLER_IPFS_MAX_DISK_SPACE_GB}GB
ipfs config Swarm.ResourceMgr.MaxMemory 1536MB
ipfs config Swarm.ConnMgr.GracePeriod '1m'
ipfs config Swarm.ConnMgr.LowWater --json 20
ipfs config Swarm.ConnMgr.HighWater --json 40

ipfs config show

