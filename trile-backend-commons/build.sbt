ThisBuild / organization := "acab.devcon0"
ThisBuild / scalaVersion := "3.4.0"
ThisBuild / version      := "0.1.0-SNAPSHOT"

ThisBuild / semanticdbEnabled := true
ThisBuild / scalafixOnCompile := true

ThisBuild / scalacOptions ++= Seq(
  "-deprecation",
  "-explain",
  "-explain-types",
  "-feature",
  "-new-syntax",
  "-print-lines",
  "-unchecked",
  "-Ykind-projector",
  "-Xmigration",
  "-rewrite",
  "-Wunused:all"
)

resolvers += ProjectResolvers.consenSys
resolvers += ProjectResolvers.jitpack
resolvers += ProjectResolvers.libP2pRepo

libraryDependencies ++= Seq(
  Dependencies.catsEffects,
  Dependencies.circeCore,
  Dependencies.circeGeneric,
  Dependencies.circeParser,
  Dependencies.libP2p,
  Dependencies.log4cats
)

lazy val trileFederationCommonsBackend = (project in file("."))
  .settings(name := "trile-backend-commons")
