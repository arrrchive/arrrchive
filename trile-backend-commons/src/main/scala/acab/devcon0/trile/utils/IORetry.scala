package acab.devcon0.trile.utils

import java.util.concurrent.TimeUnit

import scala.concurrent.duration.Duration

import cats.effect.IO

object IORetry {

  def fibonacci[T](io: IO[T], attemptsLeft: Int = 10, timeUnit: TimeUnit = TimeUnit.SECONDS): IO[T] = {
    io.recoverWith(_ => retry(io, attemptsLeft))
  }

  private def retry[T](
      io: IO[T],
      attemptsLeft: Int = 10,
      fibonacciWait: (Int, Int) = (0, 1),
      timeUnit: TimeUnit = TimeUnit.SECONDS
  ): IO[T] = {
    if attemptsLeft <= 0 then IO.raiseError(RetryException())
    else
      val delayValue: Int = fibonacciWait._1 + fibonacciWait._2
      IO.sleep(Duration(delayValue, timeUnit)) >>
        io.recoverWith(_ => retry(io, attemptsLeft - 1, (fibonacciWait._2, delayValue), timeUnit))
  }

}

final case class RetryException() extends Throwable
