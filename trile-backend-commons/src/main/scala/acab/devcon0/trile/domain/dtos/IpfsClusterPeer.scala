package acab.devcon0.trile.domain.dtos

import java.time.Instant

import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import acab.devcon0.trile.domain.dtos.aliases.IpfsClusterPeerId
import acab.devcon0.trile.domain.dtos.aliases.IpfsPeerId

final case class IpfsClusterPins(pins: List[IpfsClusterPin])
final case class IpfsClusterPin(cid: IpfsCid)
final case class IpfsClusterPeers(peers: List[IpfsClusterPeer])
final case class IpfsClusterPeer(
    id: IpfsClusterPeerId,
    addresses: List[String],
    clusterPeers: List[String],
    clusterPeersAddresses: List[String],
    version: String,
    commit: String,
    rpcProtocolVersion: String,
    error: String,
    ipfs: IpfsClusterPeerIpfsInfo,
    peername: String
)
final case class IpfsClusterPeerIpfsInfo(
    id: IpfsPeerId,
    addresses: List[String],
    error: String
)

case class IpfsClusterPinAllocation(
    replicationFactorMin: Int,
    replicationFactorMax: Int,
    name: String,
    mode: String,
    cid: IpfsCid,
    allocations: Set[IpfsClusterPeerId],
    timestamp: Instant
)
