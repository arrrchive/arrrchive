package acab.devcon0.trile.domain.dtos.aliases

type FederationMemberNickname = String
type FederationMemberId       = String
