package acab.devcon0.trile.domain.dtos.aliases

type IpfsCid             = String
type IpfsClusterPeerId   = String
type IpfsClusterNodeName = String
type IpfsPeerId          = String
