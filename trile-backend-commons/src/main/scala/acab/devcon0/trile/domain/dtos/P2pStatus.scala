package acab.devcon0.trile.domain.dtos

enum SyncStatus:
  case Syncing, Sync, OutOfSync

enum CheckInStatus:
  case Unknown, ACK, NACK

enum ControllerStatus:
  case Unknown, Online, Offline

final case class NackException() extends Throwable
