package acab.devcon0.trile.domain.codecs

import acab.devcon0.trile.domain.dtos.IpfsClusterPeer
import acab.devcon0.trile.domain.dtos.IpfsClusterPeerIpfsInfo
import acab.devcon0.trile.domain.dtos.IpfsClusterPeers
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import acab.devcon0.trile.domain.dtos.pubsub.P2p
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Data
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message._
import acab.devcon0.trile.domain.dtos.pubsub.P2p.MessageType
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Metadata
import cats.effect.IO
import io.circe._
import io.circe.generic.semiauto.deriveDecoder
import io.circe.generic.semiauto.deriveEncoder
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps
import io.libp2p.core.PeerId
import io.libp2p.core.pubsub.Topic

object P2pCodecs {

  object Decoders {
    implicit val peerId: Encoder[PeerId] = (peerId: PeerId) => Json.obj(("peerId", Json.fromString(peerId.toBase58)))

    implicit val checkIn: Decoder[FederationMemberCheckIn]                             = deriveDecoder
    implicit val checkInAck: Decoder[FederationMemberCheckInAck]                       = deriveDecoder
    implicit val checkInNack: Decoder[FederationMemberCheckInNack]                     = deriveDecoder
    implicit val sync: Decoder[FederationMemberSync]                                   = deriveDecoder
    implicit val syncAck: Decoder[FederationMemberSyncAck]                             = deriveDecoder
    implicit val syncNack: Decoder[FederationMemberSyncNack]                           = deriveDecoder
    implicit val sharingFolderUpdate: Decoder[FederationMemberSharingFolderUpdate]     = deriveDecoder
    implicit val ipfsClusterPeerIpfsInfo: Decoder[IpfsClusterPeerIpfsInfo]             = deriveDecoder
    implicit val ipfsClusterPeers: Decoder[IpfsClusterPeers]                           = deriveDecoder
    implicit val ipfsClusterPeer: Decoder[IpfsClusterPeer]                             = deriveDecoder
    implicit val federationControllerHeartbeat: Decoder[FederationControllerHeartbeat] = deriveDecoder
    implicit val metadata: Decoder[Metadata]                                           = deriveDecoder

    implicit def messageDecoder[T <: Data: Decoder]: Decoder[Message[T]] = deriveDecoder

    implicit val federationMemberHeartbeat: Decoder[FederationMemberHeartbeat] = deriveDecoder

    implicit val messageType: Decoder[MessageType] = (c: HCursor) => {
      c.value.as[String].map(MessageType.valueOf)
    }

    object Metadata {
      def apply(rawJson: String): IO[Metadata] =
        IO.fromEither(decode[Metadata](rawJson))
    }

    object Message {
      def apply[T <: Data: Decoder](rawJson: String): IO[Message[T]] =
        IO.fromEither(decode[Message[T]](rawJson))
    }

    object ControllerHeartbeat {
      def apply(rawJson: String): IO[FederationControllerHeartbeat] =
        IO.fromEither(decode[FederationControllerHeartbeat](rawJson))
    }
    object MemberHeartbeat {
      def apply(rawJson: String): IO[FederationMemberHeartbeat] =
        IO.fromEither(decode[FederationMemberHeartbeat](rawJson))
    }
    object CheckInMessage {
      def apply(rawJson: String): IO[FederationMemberCheckIn] =
        IO.fromEither(decode[FederationMemberCheckIn](rawJson))
    }

    object CheckInAck {
      def apply(rawJson: String): IO[FederationMemberCheckInAck] =
        IO.fromEither(decode[FederationMemberCheckInAck](rawJson))
    }

    object CheckInNack {
      def apply(rawJson: String): IO[FederationMemberCheckInNack] =
        IO.fromEither(decode[FederationMemberCheckInNack](rawJson))
    }

    object SyncMessage {
      def apply(rawJson: String): IO[FederationMemberSync] =
        IO.fromEither(decode[FederationMemberSync](rawJson))
    }

    object SyncAck {
      def apply(rawJson: String): IO[FederationMemberSyncAck] =
        IO.fromEither(decode[FederationMemberSyncAck](rawJson))
    }

    object SyncNack {
      def apply(rawJson: String): IO[FederationMemberSyncNack] =
        IO.fromEither(decode[FederationMemberSyncNack](rawJson))
    }

    object SharingFolderUpdateMessage {
      def apply(rawJson: String): IO[FederationMemberSharingFolderUpdate] =
        IO.fromEither(decode[FederationMemberSharingFolderUpdate](rawJson))
    }
  }

  object Encoders {
    implicit val ipfsClusterPeerIpfsInfo: Encoder[IpfsClusterPeerIpfsInfo] = deriveEncoder
    implicit val ipfsClusterPeers: Encoder[IpfsClusterPeers]               = deriveEncoder
    implicit val ipfsClusterPeer: Encoder[IpfsClusterPeer]                 = deriveEncoder
    implicit val checkIn: Encoder[FederationMemberCheckIn]                 = deriveEncoder
    implicit val checkInAck: Encoder[FederationMemberCheckInAck]           = deriveEncoder
    implicit val checkInNack: Encoder[FederationMemberCheckInNack]         = deriveEncoder
    implicit val topic: Encoder[Topic] = (topic: Topic) => Json.obj(("name", Json.fromString(topic.getTopic)))
    implicit val sync: Encoder[FederationMemberSync]                                   = deriveEncoder
    implicit val syncAck: Encoder[FederationMemberSyncAck]                             = deriveEncoder
    implicit val syncNack: Encoder[FederationMemberSyncNack]                           = deriveEncoder
    implicit val sharingFolderUpdate: Encoder[FederationMemberSharingFolderUpdate]     = deriveEncoder
    implicit val federationControllerHeartbeat: Encoder[FederationControllerHeartbeat] = deriveEncoder
    implicit val federationMemberHeartbeat: Encoder[FederationMemberHeartbeat]         = deriveEncoder
    implicit val metadata: Encoder[Metadata]                                           = deriveEncoder

    implicit def messageEncoder[T <: Data: Encoder]: Encoder[Message[T]] = deriveEncoder

    implicit val messageType: Encoder[MessageType] = (messageType: MessageType) => {
      Json.fromString(messageType.toString)
    }

    object Message {
      def apply[T <: Data: Encoder](message: Message[T]): IO[String] = IO {
        EncoderOps[Message[T]](message).asJson.noSpaces
      }

      def apply[T <: Data: Encoder](data: T, metadata: Metadata): IO[String] = IO {
        val message: Message[T] = P2p.Message(metadata, data)
        EncoderOps[Message[T]](message).asJson.noSpaces
      }
    }

    object MemberHeartbeatMessage {
      def apply(dto: FederationMemberHeartbeat): IO[String] = IO(
        EncoderOps[FederationMemberHeartbeat](dto).asJson.noSpaces
      )
    }
    object ControllerHeartbeatMessage {
      def apply(dto: FederationControllerHeartbeat): IO[String] = IO(
        EncoderOps[FederationControllerHeartbeat](dto).asJson.noSpaces
      )
    }
    object SharingFolderUpdateMessage {
      def apply(dto: FederationMemberSharingFolderUpdate): IO[String] = IO(
        EncoderOps[FederationMemberSharingFolderUpdate](dto).asJson.noSpaces
      )
    }

    object CheckInMessage {
      def apply(dto: FederationMemberCheckIn): IO[String] = IO(
        EncoderOps[FederationMemberCheckIn](dto).asJson.noSpaces
      )
    }

    object CheckInAck {
      def apply(dto: FederationMemberCheckInAck): IO[String] = IO(
        EncoderOps[FederationMemberCheckInAck](dto).asJson.noSpaces
      )
    }

    object CheckInNack {
      def apply(dto: FederationMemberCheckInNack): IO[String] = IO(
        EncoderOps[FederationMemberCheckInNack](dto).asJson.noSpaces
      )
    }

    object SyncMessage {
      def apply(dto: FederationMemberSync): IO[String] = IO(
        EncoderOps[FederationMemberSync](dto).asJson.noSpaces
      )
    }

    object SyncAck {
      def apply(dto: FederationMemberSyncAck): IO[String] = IO(
        EncoderOps[FederationMemberSyncAck](dto).asJson.noSpaces
      )
    }

    object SyncNack {
      def apply(dto: FederationMemberSyncNack): IO[String] = IO(
        EncoderOps[FederationMemberSyncNack](dto).asJson.noSpaces
      )
    }
  }
}
