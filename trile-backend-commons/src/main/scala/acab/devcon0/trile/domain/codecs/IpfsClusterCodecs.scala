package acab.devcon0.trile.domain.codecs

import java.time.Instant

import acab.devcon0.trile.domain.dtos
import acab.devcon0.trile.domain.dtos.IpfsClusterPeer
import acab.devcon0.trile.domain.dtos.IpfsClusterPeerIpfsInfo
import acab.devcon0.trile.domain.dtos.IpfsClusterPeers
import acab.devcon0.trile.domain.dtos.IpfsClusterPin
import acab.devcon0.trile.domain.dtos.IpfsClusterPinAllocation
import acab.devcon0.trile.domain.dtos.IpfsClusterPins
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import acab.devcon0.trile.domain.dtos.aliases.IpfsClusterPeerId
import cats.effect.IO
import io.circe._
import io.circe.generic.semiauto.deriveDecoder
import io.circe.generic.semiauto.deriveEncoder
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps

object IpfsClusterCodecs {

  object Decoders {
    implicit val ipfsClusterPins: Decoder[IpfsClusterPins] = deriveDecoder
    implicit val ipfsClusterPin: Decoder[IpfsClusterPin] = (c: HCursor) => {
      for cid <- c.downField("cid").as[String]
      yield dtos.IpfsClusterPin(cid)
    }
    implicit val peerIpfsInfo: Decoder[IpfsClusterPeerIpfsInfo] = deriveDecoder
    implicit val peers: Decoder[IpfsClusterPeers]               = deriveDecoder
    implicit val peer: Decoder[IpfsClusterPeer] = (c: HCursor) => {
      for
        id                    <- c.downField("id").as[String]
        addresses             <- c.downField("addresses").as[List[String]]
        clusterPeers          <- c.downField("cluster_peers").as[List[String]]
        clusterPeersAddresses <- c.downField("cluster_peers_addresses").as[List[String]]
        version               <- c.downField("version").as[String]
        commit                <- c.downField("commit").as[String]
        rpcProtocolVersion    <- c.downField("rpc_protocol_version").as[String]
        error                 <- c.downField("error").as[String]
        ipfs                  <- c.downField("ipfs").as[IpfsClusterPeerIpfsInfo]
        peername              <- c.downField("peername").as[String]
      yield {
        dtos.IpfsClusterPeer(
          id,
          addresses,
          clusterPeers,
          clusterPeersAddresses,
          version,
          commit,
          rpcProtocolVersion,
          error,
          ipfs,
          peername
        )
      }
    }

    implicit val allocation: Decoder[IpfsClusterPinAllocation] = (c: HCursor) => {
      for
        replicationFactorMin <- c.downField("replication_factor_min").as[Int]
        replicationFactorMax <- c.downField("replication_factor_max").as[Int]
        name                 <- c.downField("name").as[String]
        mode                 <- c.downField("mode").as[String]
        cid                  <- c.downField("cid").as[IpfsCid]
        allocations          <- c.downField("allocations").as[Set[IpfsClusterPeerId]]
        timestamp            <- c.downField("timestamp").as[Instant]
      yield {
        dtos.IpfsClusterPinAllocation(
          replicationFactorMin = replicationFactorMin,
          replicationFactorMax = replicationFactorMax,
          name = name,
          mode = mode,
          cid = cid,
          allocations = allocations,
          timestamp = timestamp
        )
      }
    }

    object IpfsClusterPinAllocation {
      def apply(rawJson: String): IO[IpfsClusterPinAllocation] =
        IO.fromEither(decode[IpfsClusterPinAllocation](rawJson))
    }

    object IpfsClusterPeerIpfsInfo {
      def apply(rawJson: String): IO[IpfsClusterPeerIpfsInfo] = IO.fromEither(decode[IpfsClusterPeerIpfsInfo](rawJson))
    }

    object IpfsClusterPeer {
      def apply(rawJson: String): IO[IpfsClusterPeer] = IO.fromEither(decode[IpfsClusterPeer](rawJson))
    }

    object IpfsClusterPeers {
      def apply(rawJson: String): IO[IpfsClusterPeers] = {
        val jsonAsString = "[" + rawJson.replace("}\n{", "},{") + "]"
        IO.fromEither(decode[List[IpfsClusterPeer]](jsonAsString))
          .map(dtos.IpfsClusterPeers.apply)
      }
    }

    object IpfsClusterPins {
      def apply(rawJson: String): IO[IpfsClusterPins] = {
        val jsonAsString = "[" + rawJson.replace("}\n{", "},{") + "]"
        IO.fromEither(decode[List[IpfsClusterPin]](jsonAsString))
          .map(dtos.IpfsClusterPins.apply)
      }
    }
  }

  object Encoders {
    implicit val peers: Encoder[IpfsClusterPeers]               = deriveEncoder
    implicit val peer: Encoder[IpfsClusterPeer]                 = deriveEncoder
    implicit val peerIpfsInfo: Encoder[IpfsClusterPeerIpfsInfo] = deriveEncoder

    object IpfsClusterPeerIpfsInfo {
      def apply(dto: IpfsClusterPeerIpfsInfo): IO[String] = IO(EncoderOps[IpfsClusterPeerIpfsInfo](dto).asJson.noSpaces)
    }

    object IpfsClusterPeer {
      def apply(dto: IpfsClusterPeer): IO[String] = IO(EncoderOps[IpfsClusterPeer](dto).asJson.noSpaces)
    }

    object IpfsClusterPeers {
      def apply(dto: IpfsClusterPeers): IO[String] = IO(EncoderOps[IpfsClusterPeers](dto).asJson.noSpaces)
    }
  }
}
