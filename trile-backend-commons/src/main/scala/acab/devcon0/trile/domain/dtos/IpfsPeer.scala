package acab.devcon0.trile.domain.dtos

final case class IpfsPeerRepoStat(
    repoSize: Long,
    storageMax: Long,
    numObjects: Long,
    repoPath: String,
    version: String
)
