import sbt.*

// Dependencies
// Alphabetical
object Dependencies {
  val catsEffects  = "org.typelevel" %% "cats-effect"    % DependencyVersions.cats
  val circeCore    = "io.circe"      %% "circe-core"     % DependencyVersions.circe
  val circeGeneric = "io.circe"      %% "circe-generic"  % DependencyVersions.circe
  val circeParser  = "io.circe"      %% "circe-parser"   % DependencyVersions.circe
  val libP2p       = "io.libp2p"      % "jvm-libp2p"     % DependencyVersions.libP2p
  val log4cats     = "org.typelevel" %% "log4cats-slf4j" % DependencyVersions.log4cats
}
