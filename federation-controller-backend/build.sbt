ThisBuild / version           := "0.1.0-SNAPSHOT"
ThisBuild / scalaVersion      := "3.4.0"
ThisBuild / semanticdbEnabled := true
ThisBuild / scalafixOnCompile := true
ThisBuild / scalacOptions ++= Seq(
  "-deprecation",
  "-explain",
  "-explain-types",
  "-feature",
  "-new-syntax",
  "-print-lines",
  "-unchecked",
  "-Ykind-projector",
  "-Xmigration",
  "-rewrite",
  "-Wunused:all"
)

resolvers += ProjectResolvers.akka
resolvers += ProjectResolvers.consenSys
resolvers += ProjectResolvers.jitpack
resolvers += ProjectResolvers.libP2pRepo

libraryDependencies ++= Seq(
  Dependencies.circeFs2,
  Dependencies.http4sEmber,
  Dependencies.http4sDsl,
  Dependencies.http4sCirce,
  Dependencies.logback,
  Dependencies.redis4CatsLogs,
  Dependencies.redis4CatsStreams,
  Dependencies.redis4CatsEffects,
  Dependencies.sttp,
  Dependencies.sttpFs2Backend,
  Dependencies.trileBackendCommons,
  Dependencies.typeSafeConfig,
  Dependencies.typeSafeScalaLogging
)

Docker / packageName := "triledotlink/federation-controller-backend"
dockerExposedPorts ++= Seq(8078, 7078)

lazy val trileFederationControllerBackend = (project in file("."))
  .settings(
    name                := "trile-federation-controller-backend",
    Compile / mainClass := Some("acab.devcon0.boot.Main"),
    dockerBaseImage     := "openjdk:21-jdk"
  )
  .enablePlugins(JavaAppPackaging)
  .enablePlugins(DockerPlugin)
