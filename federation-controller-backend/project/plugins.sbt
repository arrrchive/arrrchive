addSbtPlugin("org.jetbrains.scala" % "sbt-ide-settings"          % "1.1.1")
addSbtPlugin("com.typesafe.sbt"    % "sbt-native-packager"       % "1.8.0")
addSbtPlugin("com.eed3si9n"        % "sbt-assembly"              % "1.1.0")
addSbtPlugin("com.github.cb372"    % "sbt-explicit-dependencies" % "0.2.16")
addSbtPlugin("ch.epfl.scala"       % "sbt-scalafix"              % "0.12.0")
addSbtPlugin("ch.epfl.scala"       % "sbt-bloop"                 % "1.5.15")
// addDependencyTreePlugin
