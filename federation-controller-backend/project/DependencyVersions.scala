object DependencyVersions {
  val circe                = "0.14.6"
  val circeFs2             = "0.14.1"
  val http4s               = "0.23.16"
  val libP2p               = "1.1.0-RELEASE"
  val logback              = "1.4.14"
  val redis4Cats           = "1.5.2"
  val sttp                 = "4.0.0-M11"
  val typeSafeConfig       = "1.4.3"
  val typeSafeScalaLogging = "3.9.5"
}
