package acab.devcon0
package commons

import cats.effect.IO

abstract class Command[+RETURN_TYPE]
abstract class Query[+RETURN_TYPE]
abstract class Event[RETURN_TYPE]

abstract class CommandHandler[COMMAND <: Command[COMMAND_TARGET_TYPE], COMMAND_TARGET_TYPE, +EVENT <: Event[?]] {
  def handle(cmd: COMMAND): IO[EVENT]
}

trait QueryHandler[QUERY <: Query[TARGET_TYPE], TARGET_TYPE] {
  def handle(query: QUERY): IO[TARGET_TYPE]
}
