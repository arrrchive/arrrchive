package acab.devcon0.domain.codecs

import acab.devcon0.domain.dtos.IpfsCidDto
import acab.devcon0.domain.dtos.IpfsCidMetadataDto
import acab.devcon0.domain.dtos.IpfsCidSearchResponse
import cats.effect.IO
import io.circe._
import io.circe.generic.auto._
import io.circe.generic.semiauto.deriveDecoder
import io.circe.generic.semiauto.deriveEncoder
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps

object IpfsCidCodecs {
  object Decoders {

    implicit val metadata: Decoder[IpfsCidMetadataDto] = deriveDecoder
    implicit val codec: Decoder[IpfsCidDto]            = deriveDecoder

    object Dto {
      def apply(rawJson: String): IO[IpfsCidDto] = IO.fromEither(decode[IpfsCidDto](rawJson))
    }
  }

  object Encoders {
    implicit val searchResponse: Encoder[IpfsCidSearchResponse] = deriveEncoder
    implicit val metadata: Encoder[IpfsCidMetadataDto]          = deriveEncoder
    implicit val dto: Encoder[IpfsCidDto]                       = deriveEncoder

    object Dto {
      def apply(dto: IpfsCidDto): IO[String] = Json.Dto(dto).map(_.noSpaces)
    }

    object SearchResponse {
      def apply(dto: IpfsCidSearchResponse): IO[String] = Json.SearchResponse(dto).map(_.noSpaces)
    }

    object Metadata {
      def apply(dto: IpfsCidMetadataDto): IO[String]      = Json.Metadata(dto).map(_.noSpaces)
      def apply(dto: Set[IpfsCidMetadataDto]): IO[String] = Json.Metadata(dto).map(_.noSpaces)
    }

    object Json {
      object SearchResponse {
        def apply(dto: IpfsCidSearchResponse): IO[Json] = IO(EncoderOps[IpfsCidSearchResponse](dto).asJson)
      }

      object Dto {
        def apply(dto: IpfsCidDto): IO[Json] = IO(EncoderOps[IpfsCidDto](dto).asJson)
      }

      object Metadata {
        def apply(dto: IpfsCidMetadataDto): IO[Json] = IO(EncoderOps[IpfsCidMetadataDto](dto).asJson)

        def apply(dto: Set[IpfsCidMetadataDto]): IO[Json] = IO(EncoderOps[Set[IpfsCidMetadataDto]](dto).asJson)
      }
    }
  }
}
