package acab.devcon0.domain.codecs

import acab.devcon0.domain.dtos._
import cats.effect.IO
import io.circe._
import io.circe.generic.auto._
import io.circe.generic.semiauto.deriveEncoder
import io.circe.jawn.decode

object IpfsCodecs {

  object Decoders {

    implicit val ipfsLsLinkResponseDecoder: Decoder[IpfsLsLinkResponse] = (c: HCursor) =>
      for
        hash       <- c.downField("Hash").as[String]
        name       <- c.downField("Name").as[String]
        size       <- c.downField("Size").as[Int]
        target     <- c.downField("Target").as[String]
        binaryType <- c.downField("Type").as[Int]
      yield {
        IpfsLsLinkResponse(hash, name, size, target, binaryType)
      }
    implicit val ipfsLsResponseDecoder: Decoder[IpfsLsResponse] = (c: HCursor) =>
      for
        hash <- c.downField("Objects").downArray.downField("Hash").as[String]
        links <- c
          .downField("Objects")
          .downArray
          .downField("Links")
          .as[List[IpfsLsLinkResponse]]
      yield {
        IpfsLsResponse(hash, links)
      }
    implicit val ipfsNamePublishHttpResponseDecoder: Decoder[IpfsNamePublishHttpResponse] = (c: HCursor) =>
      for
        name  <- c.downField("Name").as[String]
        value <- c.downField("Value").as[String]
      yield {
        IpfsNamePublishHttpResponse(name, value)
      }

    object LsResponse {
      def apply(rawJson: String): IO[IpfsLsResponse] = IO.fromEither {
        decode[IpfsLsResponse](rawJson)
      }
    }

  }

  object Encoders {
    implicit val ipfsLsLinkResponse: Encoder[IpfsLsLinkResponse]                   = deriveEncoder
    implicit val ipfsLsResponse: Encoder[IpfsLsResponse]                           = deriveEncoder
    implicit val ipfsNamePublishHttpResponse: Encoder[IpfsNamePublishHttpResponse] = deriveEncoder
  }
}
