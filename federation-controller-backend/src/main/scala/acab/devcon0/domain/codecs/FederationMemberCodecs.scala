package acab.devcon0.domain.codecs

import acab.devcon0.domain.dtos._
import acab.devcon0.domain.dtos.pubsub.Redis
import acab.devcon0.domain.dtos.pubsub.Redis._
import cats.effect.IO
import io.circe._
import io.circe.generic.auto._
import io.circe.generic.semiauto.deriveDecoder
import io.circe.generic.semiauto.deriveEncoder
import io.circe.jawn.decode
import io.circe.syntax.EncoderOps

object FederationMemberCodecs {

  object Decoders {
    object ChangelogUpdateMessage {
      private implicit val codec: Decoder[FederationMemberChangelogUpdateMessage] = deriveDecoder
      def apply(rawJson: String): IO[FederationMemberChangelogUpdateMessage] = IO.fromEither {
        decode[FederationMemberChangelogUpdateMessage](rawJson)
      }
    }
    object IpfsCidsDeltaUpdateMessage {
      private implicit val codec: Decoder[FederationMemberIpfsCidsDeltaUpdateMessage] = deriveDecoder
      def apply(rawJson: String): IO[FederationMemberIpfsCidsDeltaUpdateMessage] = IO.fromEither {
        decode[FederationMemberIpfsCidsDeltaUpdateMessage](rawJson)
      }
    }

    object Information {
      private implicit val codec: Decoder[FederationMemberInformation] = deriveDecoder
      def apply(rawJson: String): IO[FederationMemberInformation] = IO.fromEither {
        decode[FederationMemberInformation](rawJson)
      }
    }

    object RedisSharingFolderUpdateMessage {
      private implicit val codec: Decoder[Redis.FederationMemberSharingFolderUpdateMessage] = deriveDecoder
      def apply(rawJson: String): IO[Redis.FederationMemberSharingFolderUpdateMessage] = IO.fromEither {
        decode[Redis.FederationMemberSharingFolderUpdateMessage](rawJson)
      }
    }

    object ChangelogItem {
      private implicit val codec: Decoder[FederationMemberChangelogItem] = deriveDecoder
      def apply(rawJson: String): IO[FederationMemberChangelogItem] = IO.fromEither {
        decode[FederationMemberChangelogItem](rawJson)
      }
    }
  }

  object Encoders {

    object ChangelogItem {
      private implicit val codec: Encoder[FederationMemberChangelogItem] = deriveEncoder
      def apply(dto: FederationMemberChangelogItem): IO[String] = IO {
        EncoderOps[FederationMemberChangelogItem](dto).asJson.noSpaces
      }
    }

    object IpfsCidsDeltaUpdateMessage {
      private implicit val codec: Encoder[FederationMemberIpfsCidsDeltaUpdateMessage] = deriveEncoder
      def apply(dto: FederationMemberIpfsCidsDeltaUpdateMessage): IO[String] = IO {
        EncoderOps[FederationMemberIpfsCidsDeltaUpdateMessage](dto).asJson.noSpaces
      }
    }

    object ChangelogUpdateMessage {
      private implicit val codec: Encoder[FederationMemberChangelogUpdateMessage] = deriveEncoder
      def apply(dto: FederationMemberChangelogUpdateMessage): IO[String] = IO {
        EncoderOps[FederationMemberChangelogUpdateMessage](dto).asJson.noSpaces
      }
    }

    object Information {
      implicit val codec: Encoder[FederationMemberInformation] = deriveEncoder
      def apply(dto: FederationMemberInformation): IO[String] = IO {
        EncoderOps[FederationMemberInformation](dto).asJson.noSpaces
      }
    }

    object RedisSharingFolderUpdateMessage {
      private implicit val codec: Encoder[Redis.FederationMemberSharingFolderUpdateMessage] = deriveEncoder
      def apply(dto: Redis.FederationMemberSharingFolderUpdateMessage): IO[String] = IO {
        val result = EncoderOps[FederationMemberSharingFolderUpdateMessage](dto).asJson.noSpaces
        result
      }
    }
  }
}
