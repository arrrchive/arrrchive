package acab.devcon0.domain.dtos

case class SearchParameters(term: String, page: Int)
case class SearchResult[T](parameters: SearchParameters, result: Set[T], totalCount: Long, pageSize: Long)
