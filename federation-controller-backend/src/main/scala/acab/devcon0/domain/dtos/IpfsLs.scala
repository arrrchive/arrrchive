package acab.devcon0.domain.dtos

final case class IpfsLsLinkResponse(hash: String, name: String, size: Int, target: String, binaryType: Int)
final case class IpfsLsResponse(hash: String, links: List[IpfsLsLinkResponse])
