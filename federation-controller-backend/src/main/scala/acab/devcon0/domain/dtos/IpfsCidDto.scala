package acab.devcon0.domain.dtos

import java.time.Instant

import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import acab.devcon0.trile.domain.dtos.aliases.IpfsPeerId

enum IpfsCidType:
  case File, Folder, Empty

final case class IpfsCidDto(
    cid: IpfsCid,
    size: Int,
    name: String,
    fileCount: Int,
    contents: List[IpfsCidDto],
    timestamp: Instant,
    `type`: IpfsCidType
)
final case class IpfsCidMetadataCopiesDto(hard: Int, soft: Int)

final case class IpfsCidMetadataDto(
    cid: IpfsCid,
    size: Int,
    originalName: String,
    fileCount: Int,
    contents: List[IpfsCidDto],
    timestamp: Instant,
    `type`: IpfsCidType,
    copies: IpfsCidMetadataCopiesDto,
    names: List[String]
)

case class IpfsPubSubPongMessage(ipfsPeerId: IpfsPeerId)

final case class IpfsCidSearchResponse(ipfsCids: List[IpfsCidMetadataDto], totalCount: Long, pageSize: Long)
