package acab.devcon0.domain.dtos.pubsub

import java.time.Instant

import acab.devcon0.domain.dtos.FederationIpfsCidDelta
import acab.devcon0.domain.dtos.FederationMemberChangelogItem
import acab.devcon0.domain.dtos.FederationMemberIpfsCidDelta
import acab.devcon0.trile.domain.dtos.aliases._

object Redis {
  final case class FederationMemberSharingFolderUpdateMessage(
      federationMemberId: FederationMemberId,
      ipfsCid: IpfsCid,
      timestamp: Instant
  )

  final case class FederationMemberChangelogUpdateMessage(
      federationMemberId: FederationMemberId,
      federationMemberChangelogItem: FederationMemberChangelogItem
  )

  final case class FederationMemberIpfsCidsDeltaUpdateMessage(
      delta: FederationMemberIpfsCidDelta
  )
  final case class FederationIpfsCidsDeltaUpdateMessage(delta: FederationIpfsCidDelta)
}
