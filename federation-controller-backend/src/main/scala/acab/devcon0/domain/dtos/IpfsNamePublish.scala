package acab.devcon0.domain.dtos

final case class IpfsNamePublishHttpResponse(name: String, value: String)
