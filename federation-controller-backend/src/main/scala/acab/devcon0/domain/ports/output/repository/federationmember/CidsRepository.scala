package acab.devcon0.domain.ports.output.repository.federationmember
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid

trait CidsRepository[F[_]] {
  def getAll(id: FederationMemberId, rootIpfsCid: IpfsCid): F[Set[IpfsCid]]
  def getDeleted(id: FederationMemberId, oldIpfsCid: IpfsCid, newIpfsCid: IpfsCid): F[Set[IpfsCid]]
  def getAdded(id: FederationMemberId, oldIpfsCid: IpfsCid, newIpfsCid: IpfsCid): F[Set[IpfsCid]]
  def add(id: FederationMemberId, rootIpfsCid: IpfsCid, ipfsCids: Set[IpfsCid]): F[Set[IpfsCid]]
  def exists(id: FederationMemberId, rootIpfsCid: IpfsCid, ipfsCids: Set[IpfsCid]): F[List[(IpfsCid, Boolean)]]
}
