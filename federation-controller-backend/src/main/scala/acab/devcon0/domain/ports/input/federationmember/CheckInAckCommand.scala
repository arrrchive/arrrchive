package acab.devcon0.domain.ports.input.federationmember

import acab.devcon0.commons.Command
import acab.devcon0.commons.CommandHandler
import acab.devcon0.commons.Event
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

sealed trait CheckInResponseCommand                       extends Command[String]
final case class CheckInAckCommand(p2pPeerId: P2pPeerId)  extends CheckInResponseCommand
final case class CheckInNAckCommand(p2pPeerId: P2pPeerId) extends CheckInResponseCommand

sealed abstract class CheckInAckEvent[T]                    extends Event[T]
final case class CheckInResponseErrorEvent(throwable: Throwable) extends CheckInAckEvent[Unit]
final case class CheckInResponseSuccessEvent()                   extends CheckInAckEvent[Unit]

trait CheckInAckCommandHandler extends CommandHandler[CheckInResponseCommand, String, CheckInAckEvent[?]]

object CheckInAckCommandImplicits {
  implicit class CheckInAckCommandEventFlattenOps(result: IO[CheckInAckEvent[?]]) {
    private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

    def flattenEvents: IO[Unit] = {
      result.flatMap {
        case CheckInResponseSuccessEvent()        => IO.unit
        case CheckInResponseErrorEvent(throwable) => handleErrorCase(throwable)
      }
    }

    private def handleErrorCase(exception: Throwable): IO[Unit] = {
      logger.error(s"RedisPubSubFederationMemberCheckInAckEventProcessor exception=$exception") >>
        IO.raiseError[Unit](exception)
    }
  }
}
