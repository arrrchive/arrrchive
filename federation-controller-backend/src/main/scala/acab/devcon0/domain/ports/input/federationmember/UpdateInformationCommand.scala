package acab.devcon0.domain.ports.input.federationmember

import acab.devcon0.commons.Command
import acab.devcon0.commons.CommandHandler
import acab.devcon0.commons.Event
import acab.devcon0.domain.dtos.FederationMemberInformation
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberCheckIn
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

sealed trait InformationCommand                                                              extends Command[String]
final case class InformationRefreshCommand(id: FederationMemberId, sharedFolderCid: IpfsCid) extends InformationCommand
final case class CheckInInformationCommand(checkInMessage: FederationMemberCheckIn)          extends InformationCommand

sealed abstract class InformationRefreshEvent[T]                    extends Event[T]
final case class InformationRefreshErrorEvent(throwable: Throwable) extends InformationRefreshEvent[Unit]
final case class InformationRefreshSuccessEvent(federationMemberInformation: FederationMemberInformation)
    extends InformationRefreshEvent[Unit]

trait UpdateInformationCommandHandler extends CommandHandler[InformationCommand, String, InformationRefreshEvent[?]]

object UpdateInformationCommandImplicits {
  implicit class UpdateInformationCommandEventFlattenOps(result: IO[InformationRefreshEvent[?]]) {
    private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

    def flattenEvents: IO[FederationMemberInformation] = {
      result.flatMap {
        case InformationRefreshSuccessEvent(federationMemberInformation) => IO.pure(federationMemberInformation)
        case InformationRefreshErrorEvent(throwable)                     => handleErrorCase(throwable)
      }
    }

    private def handleErrorCase(exception: Throwable): IO[FederationMemberInformation] = {
      logger.error(s"RedisPubSubFederationMemberInformationRefreshEventProcessor exception=$exception") >>
        IO.raiseError[FederationMemberInformation](exception)
    }
  }
}
