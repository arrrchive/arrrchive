package acab.devcon0.domain.ports.input.federationmembers

import acab.devcon0.commons.Query
import acab.devcon0.commons.QueryHandler
import acab.devcon0.domain.dtos.FederationMemberInformation

final case class InformationQuery() extends Query[List[FederationMemberInformation]]
trait InformationQueryHandler       extends QueryHandler[InformationQuery, List[FederationMemberInformation]]
