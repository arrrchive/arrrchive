package acab.devcon0.domain.ports.output.repository.federationmember

import acab.devcon0.domain.dtos._
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import acab.devcon0.trile.domain.dtos.aliases.IpfsClusterPeerId
import acab.devcon0.trile.domain.dtos.aliases.IpfsPeerId


trait InformationRepository[F[_]] {
  def getByIpfsClusterNodeId(ipfsClusterNodeId: IpfsClusterPeerId): F[Option[FederationMemberInformation]]
  def getByP2pPeerId(ipfsPeerId: IpfsPeerId): F[Option[FederationMemberInformation]]
  def getById(id: FederationMemberId): F[Option[FederationMemberInformation]]
  def getAll: F[List[FederationMemberInformation]]
  def save(federationMemberInformation: FederationMemberInformation): F[FederationMemberInformation]
}
