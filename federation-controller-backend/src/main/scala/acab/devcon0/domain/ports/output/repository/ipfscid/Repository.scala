package acab.devcon0.domain.ports.output.repository.ipfscid

import acab.devcon0.domain.dtos._
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid

trait Repository[F[_]] {
  def get(ipfsCid: IpfsCid): F[Option[IpfsCidDto]]
  def save(ipfsCidDto: IpfsCidDto): F[IpfsCidDto]
}
