package acab.devcon0.domain.ports.input.federation

import acab.devcon0.commons.Query
import acab.devcon0.commons.QueryHandler
import acab.devcon0.domain.dtos.FederationIpfsCidDelta
import acab.devcon0.domain.dtos.FederationMemberIpfsCidDelta

final case class IpfsCidDeltaQuery(
    delta: FederationMemberIpfsCidDelta
) extends Query[FederationIpfsCidDelta]

trait IpfsCidDeltaQueryHandler extends QueryHandler[IpfsCidDeltaQuery, FederationIpfsCidDelta]
