package acab.devcon0.domain.ports.input.ipfscid

import acab.devcon0.commons.Query
import acab.devcon0.commons.QueryHandler
import acab.devcon0.domain.dtos.IpfsCidSearchResponse
import acab.devcon0.domain.dtos.SearchParameters

final case class SearchQuery(searchParameters: SearchParameters) extends Query[IpfsCidSearchResponse]

trait SearchQueryHandler extends QueryHandler[SearchQuery, IpfsCidSearchResponse]
