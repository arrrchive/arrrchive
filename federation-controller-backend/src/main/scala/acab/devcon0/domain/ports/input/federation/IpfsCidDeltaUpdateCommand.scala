package acab.devcon0.domain.ports.input.federation

import acab.devcon0.commons.Command
import acab.devcon0.commons.CommandHandler
import acab.devcon0.commons.Event
import acab.devcon0.domain.dtos.FederationIpfsCidDelta

final case class IpfsCidDeltaUpdateCommand(delta: FederationIpfsCidDelta) extends Command[String]

sealed abstract class IpfsCidDeltaUpdateEvent[T]  extends Event[T]
final case class IpfsCidDeltaUpdateSuccessEvent() extends IpfsCidDeltaUpdateEvent[Unit]
final case class IpfsCidDeltaUpdateErrorEvent()   extends IpfsCidDeltaUpdateEvent[Unit]

trait IpfsCidDeltaUpdateCommandHandler
    extends CommandHandler[IpfsCidDeltaUpdateCommand, String, IpfsCidDeltaUpdateEvent[?]]
