package acab.devcon0.domain.ports.input.federationmember

import acab.devcon0.commons.Query
import acab.devcon0.commons.QueryHandler
import acab.devcon0.domain.dtos.FederationMemberInformation
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import cats.effect.IO

sealed trait InformationQuery                                                 extends Query[FederationMemberInformation]
final case class InformationByP2pPeerIdQuery(p2pPeerId: P2pPeerId)            extends InformationQuery
final case class InformationByIdQuery(federationMemberId: FederationMemberId) extends InformationQuery

trait InformationQueryHandler extends QueryHandler[InformationQuery, FederationMemberInformation] {
  def handleOptional(query: InformationQuery): IO[Option[FederationMemberInformation]]
}
