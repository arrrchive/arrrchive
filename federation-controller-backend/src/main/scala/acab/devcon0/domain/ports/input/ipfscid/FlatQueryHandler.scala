package acab.devcon0.domain.ports.input.ipfscid

import acab.devcon0.commons.Query
import acab.devcon0.commons.QueryHandler
import acab.devcon0.domain.dtos.IpfsCidDto
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid

final case class FlatQuery(ipfsCid: IpfsCid) extends Query[Option[IpfsCidDto]]

trait FlatQueryHandler extends QueryHandler[FlatQuery, Option[IpfsCidDto]]
