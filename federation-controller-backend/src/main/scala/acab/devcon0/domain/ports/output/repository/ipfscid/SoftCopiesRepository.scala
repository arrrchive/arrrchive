package acab.devcon0.domain.ports.output.repository.ipfscid

import acab.devcon0.trile.domain.dtos.aliases.IpfsCid

trait SoftCopiesRepository[F[_]] {
  def get(ipfsCid: IpfsCid): F[Int]

  def set(ipfsCid: IpfsCid, count: Int): F[Unit]

  def delete(ipfsCid: IpfsCid): F[Unit]
}
