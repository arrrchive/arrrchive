package acab.devcon0.domain.ports.input.ipfscid

import acab.devcon0.commons.Query
import acab.devcon0.commons.QueryHandler
import acab.devcon0.domain.dtos.IpfsCidMetadataDto
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid

final case class MetadataQuery(ipfsCid: IpfsCid) extends Query[Option[IpfsCidMetadataDto]]

trait MetadataQueryHandler extends QueryHandler[MetadataQuery, Option[IpfsCidMetadataDto]]
