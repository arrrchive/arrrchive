package acab.devcon0.domain.ports.output.client

import acab.devcon0.domain.dtos.IpfsLsResponse
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO

trait IpfsClient {
  def ls(ipfsCID: IpfsCid): IO[IpfsLsResponse]
}
