package acab.devcon0.domain.ports.input.federationmember

import acab.devcon0.commons.Command
import acab.devcon0.commons.CommandHandler
import acab.devcon0.commons.Event
import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberSharingFolderUpdateMessage
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

final case class BounceSharingFolderUpdateCommand(message: FederationMemberSharingFolderUpdateMessage)
    extends Command[FederationMemberSharingFolderUpdateMessage]

sealed abstract class BounceSharingFolderUpdateEvent[T]                    extends Event[T]
final case class BounceSharingFolderUpdateSuccessEvent()                   extends BounceSharingFolderUpdateEvent[Unit]
final case class BounceSharingFolderUpdateErrorEvent(throwable: Throwable) extends BounceSharingFolderUpdateEvent[Unit]

trait BounceSharingFolderUpdateCommandHandler
    extends CommandHandler[
      BounceSharingFolderUpdateCommand,
      FederationMemberSharingFolderUpdateMessage,
      BounceSharingFolderUpdateEvent[?]
    ]

object BounceSharingFolderUpdateCommandImplicits {
  implicit class BounceSharingFolderUpdateCommandEventFlattenOps(result: IO[BounceSharingFolderUpdateEvent[?]]) {
    private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

    def flattenEvents: IO[Unit] = {
      result.flatMap {
        case BounceSharingFolderUpdateSuccessEvent()        => IO.unit
        case BounceSharingFolderUpdateErrorEvent(throwable) => handleErrorCase(throwable)
      }
    }

    private def handleErrorCase(exception: Throwable): IO[Unit] = {
      logger.error(s"RedisPubSubFederationMemberBounceSharingFolderUpdateEventProcessor exception=$exception") >>
        IO.raiseError[Unit](exception)
    }
  }
}
