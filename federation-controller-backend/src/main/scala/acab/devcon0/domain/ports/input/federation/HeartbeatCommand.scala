package acab.devcon0.domain.ports.input.federation

import acab.devcon0.commons.Command
import acab.devcon0.commons.CommandHandler
import acab.devcon0.commons.Event
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

final case class HeartbeatCommand() extends Command[Unit]

sealed abstract class HeartbeatEvent[T]                    extends Event[T]
final case class HeartbeatErrorEvent(throwable: Throwable) extends HeartbeatEvent[Unit]
final case class HeartbeatSuccessEvent()                   extends HeartbeatEvent[Unit]

trait HeartbeatCommandHandler extends CommandHandler[HeartbeatCommand, Unit, HeartbeatEvent[?]]

object HeartbeatCommandImplicits {
  implicit class HeartbeatCommandEventFlattenOps(result: IO[HeartbeatEvent[?]]) {
    private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

    def flattenEvents: IO[Unit] = {
      result.flatMap {
        case HeartbeatSuccessEvent()        => IO.unit
        case HeartbeatErrorEvent(throwable) => handleErrorCase(throwable)
      }
    }

    private def handleErrorCase(exception: Throwable): IO[Unit] = {
      logger.error(s"RedisPubSubFederationMemberHeartbeatEventProcessor exception=$exception") >>
        IO.raiseError[Unit](exception)
    }
  }
}
