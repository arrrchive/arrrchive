package acab.devcon0.domain.ports.input.ipfscid

import acab.devcon0.commons.Query
import acab.devcon0.commons.QueryHandler
import acab.devcon0.domain.dtos.IpfsCidDto
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid

sealed trait TreeQuery                            extends Query[IpfsCidDto]
final case class FileTreeQuery(ipfsCid: IpfsCid)  extends TreeQuery
final case class NodesTreeQuery(ipfsCid: IpfsCid) extends TreeQuery

trait TreeQueryHandler extends QueryHandler[TreeQuery, IpfsCidDto]
