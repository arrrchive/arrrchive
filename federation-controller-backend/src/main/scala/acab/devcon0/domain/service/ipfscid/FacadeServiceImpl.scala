package acab.devcon0.domain.service.ipfscid

import acab.devcon0.domain.dtos.IpfsCidDto
import acab.devcon0.domain.ports.output.repository.ipfscid.Repository
import acab.devcon0.output.repository.ipfscid.FileTreeRepository
import acab.devcon0.output.repository.ipfscid.FlatRepository
import acab.devcon0.output.repository.ipfscid.NodeTreeRepository
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class FacadeServiceImpl(
    flatRepository: FlatRepository,
    fileTreeRepository: FileTreeRepository,
    nodeTreeRepository: NodeTreeRepository
) extends FacadeService[IO] {

  private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def getFileTree(ipfsCid: IpfsCid): IO[Option[IpfsCidDto]] = {
    getInner(ipfsCid, fileTreeRepository)
  }

  override def saveFileTree(ipfsCidDto: IpfsCidDto): IO[IpfsCidDto] = {
    saveInner(ipfsCidDto, fileTreeRepository)
  }

  override def getFlat(ipfsCid: IpfsCid): IO[Option[IpfsCidDto]] = {
    getInner(ipfsCid, flatRepository)
  }

  private def getInner(ipfsCid: IpfsCid, repository: Repository[IO]): IO[Option[IpfsCidDto]] = {
    repository.get(ipfsCid)
  }

  override def getFlats(ipfsCids: Set[IpfsCid]): IO[Set[IpfsCidDto]] = {
    if ipfsCids.isEmpty then IO(Set())
    else flatRepository.getAll(ipfsCids)
  }

  override def saveFlat(ipfsCidDto: IpfsCidDto): IO[IpfsCidDto] = {
    saveInner(ipfsCidDto, flatRepository)
  }

  override def saveFlats(ipfsCidDtos: Set[IpfsCidDto]): IO[Set[IpfsCidDto]] = {
    if ipfsCidDtos.isEmpty then IO(Set())
    else
      flatRepository
        .saveAll(ipfsCidDtos)
        .flatTap(_ => logger.debug(s"saveFlats for ipfsCids=${ipfsCidDtos.map(_.cid)} succeeded"))
        .onError(logError)
  }

  override def deleteFlats(ipfsCids: Set[IpfsCid]): IO[Unit] = {
    if ipfsCids.isEmpty then IO(List())
    else
      flatRepository
        .deleteAll(ipfsCids)
        .flatTap(_ => logger.debug(s"deleteFlats for ipfsCids=$ipfsCids succeeded"))
        .onError(logError)
  }

  override def getNodeTree(ipfsCid: IpfsCid): IO[Option[IpfsCidDto]] = {
    getInner(ipfsCid, nodeTreeRepository)
  }

  override def saveNodeTree(ipfsCidDto: IpfsCidDto): IO[IpfsCidDto] = {
    saveInner(ipfsCidDto, nodeTreeRepository)
  }

  private def saveInner(ipfsCidDto: IpfsCidDto, repository: Repository[IO]): IO[IpfsCidDto] = {
    repository
      .save(ipfsCidDto)
      .flatTap(_ => logger.debug(s"IpfsCidFileTreeService.save for ipfsCid=${ipfsCidDto.cid} succeeded"))
      .onError(logError)
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }
}
