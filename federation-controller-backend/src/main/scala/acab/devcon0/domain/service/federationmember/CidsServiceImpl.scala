package acab.devcon0.domain.service.federationmember

import acab.devcon0.domain.ports.output.repository.federationmember.CidsRepository
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO

class CidsServiceImpl(repository: CidsRepository[IO]) extends CidsService[IO] {

  override def getAll(id: FederationMemberId, rootIpfsCid: IpfsCid): IO[Set[IpfsCid]] = {
    repository.getAll(id, rootIpfsCid)
  }

  override def getDeleted(id: FederationMemberId, oldIpfsCid: IpfsCid, newIpfsCid: IpfsCid): IO[Set[IpfsCid]] = {
    repository.getDeleted(id, oldIpfsCid, newIpfsCid)
  }

  override def getAdded(id: FederationMemberId, oldIpfsCid: IpfsCid, newIpfsCid: IpfsCid): IO[Set[IpfsCid]] = {
    repository.getAdded(id, oldIpfsCid, newIpfsCid)
  }

  override def add(id: FederationMemberId, rootIpfsCid: IpfsCid, ipfsCids: Set[IpfsCid]): IO[Set[IpfsCid]] = {
    repository.add(id, rootIpfsCid, ipfsCids)
  }

  override def exists(
      id: FederationMemberId,
      rootIpfsCid: IpfsCid,
      ipfsCids: Set[IpfsCid]
  ): IO[List[(IpfsCid, Boolean)]] = {
    repository.exists(id, rootIpfsCid, ipfsCids)
  }
}
