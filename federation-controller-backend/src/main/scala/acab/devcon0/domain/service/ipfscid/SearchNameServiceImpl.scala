package acab.devcon0.domain.service.ipfscid

import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.output.repository.ipfscid.SearchNameRepository
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO

class SearchNameServiceImpl(repository: SearchNameRepository[IO]) extends SearchNameService[IO] {

  override def createIndex(): IO[Unit] = {
    repository.createIndex()
  }

  override def search(searchParameters: SearchParameters): IO[SearchResult[IpfsCid]] = {
    repository.search(searchParameters)
  }
}
