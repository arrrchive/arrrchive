package acab.devcon0.domain.service

import acab.devcon0.domain.dtos.IpfsCidDto
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO

trait IpfsService {
  def lsFileTree(ipfsCID: IpfsCid): IO[IpfsCidDto]
}
