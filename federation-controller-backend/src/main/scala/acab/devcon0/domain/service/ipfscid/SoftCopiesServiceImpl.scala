package acab.devcon0.domain.service.ipfscid
import acab.devcon0.domain.ports.output.repository.ipfscid.SoftCopiesRepository
import acab.devcon0.domain.service.ipfscid
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO

class SoftCopiesServiceImpl(repository: SoftCopiesRepository[IO]) extends SoftCopiesService[IO] {

  override def get(ipfsCid: IpfsCid): IO[Int] = {
    repository.get(ipfsCid)
  }

  override def set(ipfsCid: IpfsCid, count: Int): IO[Unit] = {
    repository.set(ipfsCid, count)
  }

  override def delete(ipfsCid: IpfsCid): IO[Unit] = {
    repository.delete(ipfsCid)
  }
}
