package acab.devcon0.domain.service.ipfscid

import acab.devcon0.trile.domain.dtos.aliases.IpfsCid

trait SoftCopiesService[F[_]] {
  def get(ipfsCid: IpfsCid): F[Int]
  def set(ipfsCid: IpfsCid, count: Int): F[Unit]
  def delete(ipfsCid: IpfsCid): F[Unit]
}
