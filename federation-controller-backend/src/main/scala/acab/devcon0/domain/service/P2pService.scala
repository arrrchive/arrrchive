package acab.devcon0.domain.service

import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message._

trait P2pService[F[_]] {
  def publish(message: FederationControllerHeartbeat): F[Unit]
  def publish(message: FederationMemberCheckInAck, to: P2pPeerId): F[Unit]
  def publish(message: FederationMemberCheckInNack, to: P2pPeerId): F[Unit]
  def publish(message: FederationMemberSyncAck, to: P2pPeerId): F[Unit]
  def publish(message: FederationMemberSyncNack, to: P2pPeerId): F[Unit]
}
