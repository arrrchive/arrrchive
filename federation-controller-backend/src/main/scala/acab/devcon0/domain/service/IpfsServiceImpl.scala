package acab.devcon0
package domain.service
import java.time.Duration
import java.time.Instant

import acab.devcon0.domain.dtos.IpfsCidType.Empty
import acab.devcon0.domain.dtos.IpfsCidType.File
import acab.devcon0.domain.dtos.IpfsCidType.Folder
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.output.client.IpfsClient
import acab.devcon0.output.repository.ipfscid.FlatRepository
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect._
import cats.implicits._
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class IpfsServiceImpl(ipfsClient: IpfsClient, flatRepository: FlatRepository) extends IpfsService {

  private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def lsFileTree(ipfsCid: IpfsCid): IO[IpfsCidDto] = {
    for
      startTimestamp <- IO(Instant.now())
      dtoList        <- getIpfsCidDto(Left(ipfsCid))
      endTimestamp   <- IO(Instant.now())
      _              <- logDuration(startTimestamp, endTimestamp)
    yield dtoList
  }

  private def logDuration(startTimestamp: Instant, endTimestamp: Instant): IO[Unit] = {
    logger.info(s"label=file-tree, duration=${Duration.between(startTimestamp, endTimestamp).toSeconds}(s)")
  }

  private def ipfsClientLs(ipfsCid: IpfsCid): IO[IpfsLsResponse] = {
    ipfsClient.ls(ipfsCid)
  }

  private def buildIpfsCidDto(
      ipfsLsResponse: IpfsLsResponse,
      maybeIpfsLsLinkResponse: Option[IpfsLsLinkResponse]
  ): IO[IpfsCidDto] = {
    for
      ipfsCidType <- IO(maybeIpfsLsLinkResponse.map(getIpfsCidType).getOrElse(getIpfsCidType(ipfsLsResponse)))
      contents    <- getIpfsCidDtos(ipfsLsResponse.links)
      fileCount =
        if ipfsCidType == Folder then
          contents.foldLeft(0)((acc, content) => {
            if content.`type`.eq(Folder) then acc + content.fileCount
            else acc + 1
          })
        else 0
      name = maybeIpfsLsLinkResponse.map(_.name).getOrElse("")
      size = maybeIpfsLsLinkResponse.filter(!isFolderType(_)).map(_.size).getOrElse(getIpfsCidDtoSize(contents))
    yield {
      IpfsCidDto(
        cid = ipfsLsResponse.hash,
        size = size,
        name = name,
        fileCount = fileCount,
        contents = if ipfsCidType.eq(Folder) then contents else List(),
        timestamp = Instant.now(),
        `type` =
          if ipfsCidType.eq(Folder) && fileCount == 0 then Empty
          else if ipfsCidType.eq(File) && size == 0 then Empty
          else ipfsCidType
      )
    }
  }

  // File = SUM of all blocks | Folder = SUM of all files | Empty = contents is emtpy, takes initial accumulator
  private def getIpfsCidDtoSize(contents: List[IpfsCidDto]): Int = {
    contents.foldLeft[Int](0)((acc, content) => acc + content.size)
  }

  private def getIpfsCidDto(eitherCidOrLink: Either[IpfsCid, IpfsLsLinkResponse]): IO[IpfsCidDto] = {
    eitherCidOrLink match
      case Left(ipfsCid) =>
        flatRepository
          .get(ipfsCid)
          .flatMap(maybeCacheHit => {
            maybeCacheHit match
              case Some(value) => IO(value)
              case None        => ipfsClientLs(ipfsCid).flatMap(buildIpfsCidDto(_, None))
          })
      case Right(link) =>
        flatRepository
          .get(link.hash)
          .flatMap(maybeCacheHit => {
            maybeCacheHit match
              case Some(value) => IO(value)
              case None        => ipfsClientLs(link.hash).flatMap(buildIpfsCidDto(_, Some(link)))
          })
  }

  private def getIpfsCidDtos(links: List[IpfsLsLinkResponse]): IO[List[IpfsCidDto]] = {
    links.traverse(link => getIpfsCidDto(Right(link)))
  }

  private def getIpfsCidType(ipfsLsResponse: IpfsLsResponse): IpfsCidType = {
    val links              = ipfsLsResponse.links
    val hasLinks           = ipfsLsResponse.links.nonEmpty
    val linksNamesNonEmpty = links.exists(!_.name.isBlank)
    (hasLinks, linksNamesNonEmpty) match
      case (false, _)    => Empty
      case (true, true)  => Folder
      case (true, false) => File
  }

  private def getIpfsCidType(linkResponse: IpfsLsLinkResponse): IpfsCidType = {
    if isFolderType(linkResponse) then Folder
    else File
  }

  private def isFolderType(linkResponse: IpfsLsLinkResponse): Boolean = {
    linkResponse.binaryType == 1
  }
}
