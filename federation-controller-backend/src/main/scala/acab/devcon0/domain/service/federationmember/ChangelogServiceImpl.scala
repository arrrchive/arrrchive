package acab.devcon0.domain.service.federationmember

import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.output.repository.federationmember.ChangelogRepository
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import cats.effect.IO

class ChangelogServiceImpl(val repository: ChangelogRepository[IO]) extends ChangelogService[IO] {

  override def getNewest(id: FederationMemberId): IO[Option[FederationMemberChangelogItem]] = {
    repository.getNewest(id)
  }

  override def getPrevious(
      id: FederationMemberId,
      federationMemberChangelogItem: FederationMemberChangelogItem
  ): IO[Option[FederationMemberChangelogItem]] = {
    repository
      .getPrevious(id, federationMemberChangelogItem.timestamp.minusMillis(1))
      .map(_.filter(!_.ipfsCid.equals(federationMemberChangelogItem.ipfsCid)))
      .map(_.lastOption)
  }

  override def add(
      id: FederationMemberId,
      federationMemberChangelogItem: FederationMemberChangelogItem
  ): IO[FederationMemberChangelogItem] = {
    repository.add(id, federationMemberChangelogItem)
  }
}
