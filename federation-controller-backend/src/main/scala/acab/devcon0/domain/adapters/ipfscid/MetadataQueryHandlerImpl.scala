package acab.devcon0.domain.adapters.ipfscid

import acab.devcon0.domain.dtos.IpfsCidDto
import acab.devcon0.domain.dtos.IpfsCidMetadataCopiesDto
import acab.devcon0.domain.dtos.IpfsCidMetadataDto
import acab.devcon0.domain.mappers.IpfsCidMapper
import acab.devcon0.domain.ports.input.ipfscid._
import acab.devcon0.domain.service.IpfsService
import acab.devcon0.domain.service.ipfscid.FacadeService
import acab.devcon0.domain.service.ipfscid.HardCopiesService
import acab.devcon0.domain.service.ipfscid.SoftCopiesService
import cats.effect.IO
import cats.implicits.catsSyntaxTuple2Parallel
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class MetadataQueryHandlerImpl(
    ipfsService: IpfsService,
    facadeService: FacadeService[IO],
    hardCopiesService: HardCopiesService[IO],
    softCopiesService: SoftCopiesService[IO]
) extends MetadataQueryHandler {

  private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(query: MetadataQuery): IO[Option[IpfsCidMetadataDto]] = {
    for
      maybeCacheHit            <- facadeService.getFlat(query.ipfsCid)
      (hardCopies, softCopies) <- (hardCopiesService.get(query.ipfsCid), softCopiesService.get(query.ipfsCid)).parTupled
      result <- maybeCacheHit match
        case Some(value) => IO(Some(buildIpfsCidMetadataDto(value, hardCopies, softCopies)))
        case None =>
          ipfsService
            .lsFileTree(query.ipfsCid)
            .map(IpfsCidMapper.Flat.to)
            .flatTap(facadeService.saveFlat)
            .map(dto => Some(buildIpfsCidMetadataDto(dto, hardCopies, softCopies)))
            .onError(logError)
    yield result
  }

  private def buildIpfsCidMetadataDto(dto: IpfsCidDto, hardCopies: Int, softCopies: Int): IpfsCidMetadataDto = {
    IpfsCidMetadataDto(
      cid = dto.cid,
      size = dto.size,
      originalName = dto.name,
      fileCount = dto.fileCount,
      contents = dto.contents,
      timestamp = dto.timestamp,
      `type` = dto.`type`,
      copies = IpfsCidMetadataCopiesDto(hard = hardCopies, soft = softCopies),
      names = List()
    )
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }
}
