package acab.devcon0.domain.adapters.federation

import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.input._
import acab.devcon0.domain.ports.input.federation.IpfsCidDeltaQuery
import acab.devcon0.domain.ports.input.federation.IpfsCidDeltaQueryHandler
import acab.devcon0.domain.service._
import acab.devcon0.domain.service.federationmember.CidsService
import acab.devcon0.domain.service.federationmember.InformationService
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO
import cats.implicits._
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class IpfsCidDeltaQueryHandlerImpl(
    federationMemberCidsService: CidsService[IO],
    federationMemberInformationService: InformationService[IO]
) extends IpfsCidDeltaQueryHandler {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(query: IpfsCidDeltaQuery): IO[FederationIpfsCidDelta] = {
    val removals: Set[IpfsCid] = query.delta.removals
    for
      existingInOtherMembers <- getExistsInOtherMembers(removals, query.delta.federationMemberId)
      orphanCids = getOrphanCids(removals, existingInOtherMembers)
    yield FederationIpfsCidDelta(additions = query.delta.additions, removals = orphanCids)
  }

  private def getOrphanCids(deletingIpfsCids: Set[IpfsCid], existingInOtherMembers: Set[IpfsCid]): Set[IpfsCid] = {
    deletingIpfsCids.diff(existingInOtherMembers)
  }

  private def getExistsInOtherMembers(ipfsCids: Set[IpfsCid], id: FederationMemberId): IO[Set[IpfsCid]] = {
    getOtherMembersSharedFolderCid(id)
      .flatMap(getExistingInOtherMembers(ipfsCids, _))
  }

  private def getExistingInOtherMembers(
      ipfsCids: Set[IpfsCid],
      membersRootIpfsCid: List[(FederationMemberId, IpfsCid)]
  ): IO[Set[IpfsCid]] = {
    for existsResult: List[(IpfsCid, Boolean)] <- membersRootIpfsCid
        .flatTraverse(tuple => federationMemberCidsService.exists(tuple._1, tuple._2, ipfsCids))
    yield existsResult.filter(_._2).map(_._1).toSet

  }

  private def getOtherMembersSharedFolderCid(id: FederationMemberId): IO[List[(FederationMemberId, IpfsCid)]] = {
    federationMemberInformationService.getAll
      .map(_.filter(_.id.equals(id)).map(information => (information.id, information.sharedFolderCid)))
  }
}
