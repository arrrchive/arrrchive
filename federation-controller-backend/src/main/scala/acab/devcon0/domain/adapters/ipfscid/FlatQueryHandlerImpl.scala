package acab.devcon0.domain.adapters.ipfscid

import acab.devcon0.domain.dtos.IpfsCidDto
import acab.devcon0.domain.mappers.IpfsCidMapper
import acab.devcon0.domain.ports.input.ipfscid._
import acab.devcon0.domain.service.IpfsService
import acab.devcon0.domain.service.ipfscid.FacadeService
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class FlatQueryHandlerImpl(
    ipfsService: IpfsService,
    facadeService: FacadeService[IO]
) extends FlatQueryHandler {

  private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(query: FlatQuery): IO[Option[IpfsCidDto]] = {
    for
      maybeCacheHit <- facadeService.getFlat(query.ipfsCid)
      result <- maybeCacheHit match
        case Some(value) => IO(Some(value))
        case None =>
          ipfsService
            .lsFileTree(query.ipfsCid)
            .map(IpfsCidMapper.Flat.to)
            .flatTap(facadeService.saveFlat)
            .map(Some(_))
            .onError(logError)
    yield result
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }
}
