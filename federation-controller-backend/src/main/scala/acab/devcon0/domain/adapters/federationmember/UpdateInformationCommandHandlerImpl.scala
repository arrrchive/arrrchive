package acab.devcon0.domain.adapters.federationmember

import java.time.Instant

import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.input._
import acab.devcon0.domain.ports.input.federationmember._
import acab.devcon0.domain.service.IpfsClusterService
import acab.devcon0.domain.service.federationmember.InformationService
import acab.devcon0.trile.domain.dtos.IpfsClusterPeer
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberCheckIn
import acab.devcon0.trile.utils.EffectsUtils
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class UpdateInformationCommandHandlerImpl(
    ipfsClusterService: IpfsClusterService[IO],
    informationService: InformationService[IO]
) extends UpdateInformationCommandHandler {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(cmd: InformationCommand): IO[InformationRefreshEvent[?]] =
    cmd match
      case command: InformationRefreshCommand => update(command).flatMap(postHandle)
      case command: CheckInInformationCommand => update(command).flatMap(postHandle)

  private def postHandle(information: FederationMemberInformation): IO[InformationRefreshEvent[?]] =
    IO(InformationRefreshSuccessEvent.apply(information))
      .onError(throwable => IO(InformationRefreshErrorEvent(throwable)))

  private def update(command: CheckInInformationCommand): IO[FederationMemberInformation] = {
    refreshInner(command.checkInMessage)
  }

  private def update(command: InformationRefreshCommand): IO[FederationMemberInformation] = {
    refreshInner(command.sharedFolderCid, command.id)
  }

  private def refreshInner(
      sharedFolderCid: IpfsCid,
      federationMemberId: FederationMemberId
  ): IO[FederationMemberInformation] = {
    for
      federationMemberInformation <- getFederationMemberInformation(federationMemberId)
      newInformation = federationMemberInformation.copy(sharedFolderCid = sharedFolderCid, lastSeen = now)
      _ <- informationService.save(newInformation)
      _ <- logResult(newInformation)
    yield newInformation
  }

  private def now: Instant = {
    Instant.now()
  }

  private def getFederationMemberInformation(
      federationMemberId: FederationMemberId
  ): IO[FederationMemberInformation] = {
    informationService
      .getById(federationMemberId)
      .flatMap(EffectsUtils.forceOptionExistence())
  }

  private def refreshInner(message: FederationMemberCheckIn): IO[FederationMemberInformation] = {
    for
      ipfsClusterPeer <- ipfsClusterService.getPeer(message.ipfsClusterPeerId)
      newFederationMemberInformation = build(ipfsClusterPeer, message: FederationMemberCheckIn)
      _ <- informationService.save(newFederationMemberInformation)
      _ <- logResult(newFederationMemberInformation)
    yield newFederationMemberInformation
  }

  private def logResult(newFederationMemberInformation: FederationMemberInformation): IO[Unit] = {
    logger.debug(s"Federation member information refreshed info=$newFederationMemberInformation")
  }

  private def build(
      ipfsClusterPeer: IpfsClusterPeer,
      message: FederationMemberCheckIn
  ): FederationMemberInformation = {
    FederationMemberInformation(
      id = List(message.nickname, message.p2pPeerId).mkString("-"),
      p2pPeerId = message.p2pPeerId,
      nickname = message.nickname,
      ipfsClusterPeerId = ipfsClusterPeer.id,
      ipfsClusterNodeName = ipfsClusterPeer.peername,
      ipfsPeerId = ipfsClusterPeer.ipfs.id,
      sharedFolderCid = message.sharedFolderCid,
      ipfsClusterAddresses = ipfsClusterPeer.addresses,
      ipfsIpfsAddresses = ipfsClusterPeer.ipfs.addresses,
      lastSeen = now
    )
  }
}
