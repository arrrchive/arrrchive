package acab.devcon0.domain.adapters.federationmember

import acab.devcon0.domain.ports.input._
import acab.devcon0.domain.ports.input.federationmember._
import acab.devcon0.domain.service.IpfsClusterService
import acab.devcon0.domain.service.P2pService
import acab.devcon0.trile.domain.dtos.aliases.P2pPeerId
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationControllerHeartbeat
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberCheckInAck
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberCheckInNack
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class CheckInAckCommandHandlerImpl(p2PService: P2pService[IO], ipfsClusterService: IpfsClusterService[IO])
    extends CheckInAckCommandHandler {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(cmd: CheckInResponseCommand): IO[CheckInAckEvent[?]] = {
    cmd match
      case CheckInAckCommand(p2pPeerId) =>
        ackCheckIn(p2pPeerId)

      case CheckInNAckCommand(p2pPeerId) =>
        p2PService
          .publish(FederationMemberCheckInNack(), p2pPeerId)
          .map(_ => CheckInResponseSuccessEvent())
          .onError(throwable => IO(CheckInResponseErrorEvent(throwable = throwable)))
  }

  private def ackCheckIn(p2pPeerId: P2pPeerId): IO[CheckInResponseSuccessEvent] = {
    (for
      _                <- p2PService.publish(FederationMemberCheckInAck(), p2pPeerId)
      ipfsClusterPeers <- ipfsClusterService.getPeers
      _                <- p2PService.publish(FederationControllerHeartbeat(ipfsClusterPeers.peers))
    yield CheckInResponseSuccessEvent()).onError(throwable => IO(CheckInResponseErrorEvent(throwable = throwable)))
  }
}
