package acab.devcon0.domain.adapters.federationmember

import java.time.Instant

import acab.devcon0.domain.dtos._
import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberSharingFolderUpdateMessage
import acab.devcon0.domain.ports.input._
import acab.devcon0.domain.ports.input.federationmember._
import acab.devcon0.domain.service.federationmember.ChangelogService
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class SharingFolderUpdateCommandHandlerImpl(
    changelogService: ChangelogService[IO]
) extends SharingFolderUpdateCommandHandler {

  private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(cmd: SharingFolderUpdateCommand): IO[SharingFolderEvent[?]] =
    handleInner(cmd.message, cmd.federationMemberInformation)
      .map {
        case Some(changelogItem) => SharingFolderSuccessEvent(changelogItem)
        case None                => SharingFolderKnownUpdateEvent()
      }
      .onError(throwable => IO(SharingFolderErrorEvent(throwable)))
      .flatTap(_ => logResult(cmd))

  private def handleInner(
      message: FederationMemberSharingFolderUpdateMessage,
      memberInformation: FederationMemberInformation
  ): IO[Option[FederationMemberChangelogItem]] = {
    buildChangelogItemConditionally(message, memberInformation)
      .flatMap {
        case Some(changelogItem) => saveChangelogItem(memberInformation, changelogItem).map(Some(_))
        case None                => IO.pure(None)
      }
  }

  private def saveChangelogItem(
      memberInformation: FederationMemberInformation,
      changelogItem: FederationMemberChangelogItem
  ): IO[FederationMemberChangelogItem] = {
    logger.info(s"saveChangelogItem cid=${changelogItem.ipfsCid}") >>
      changelogService.add(memberInformation.id, changelogItem)
  }

  private def buildChangelogItemConditionally(
      message: FederationMemberSharingFolderUpdateMessage,
      memberInformation: FederationMemberInformation
  ): IO[Option[FederationMemberChangelogItem]] = {
    getNewestChangeLogItem(memberInformation.id)
      .flatMap {
        case Some(changelogItem) if !isMessageNewerAndDifferent(changelogItem, message) =>
          logNotNewerNorDifferent(message, changelogItem.timestamp)
        case _ => IO.pure(Some(FederationMemberChangelogItem(message.ipfsCid, message.timestamp)))
      }
  }

  private def getNewestChangeLogItem(id: FederationMemberId): IO[Option[FederationMemberChangelogItem]] = {
    changelogService.getNewest(id)
  }

  private def isMessageNewerAndDifferent(
      changelogItem: FederationMemberChangelogItem,
      message: FederationMemberSharingFolderUpdateMessage
  ): Boolean = {
    message.timestamp.isAfter(changelogItem.timestamp) &&
    !message.ipfsCid.equals(changelogItem.ipfsCid)
  }

  private def logResult(cmd: SharingFolderUpdateCommand): IO[Unit] = {
    val nickname = cmd.federationMemberInformation.nickname
    logger.info(s"Federation member changelog added. nickname=$nickname ipfsCid=${cmd.message.ipfsCid}")
  }

  private def logNotNewerNorDifferent(
      message: FederationMemberSharingFolderUpdateMessage,
      timestamp: Instant
  ): IO[Option[FederationMemberChangelogItem]] = {
    logger.info(
      s"Received message is not newer nor different, request dropped. " +
        s"ipfsCid=${message.ipfsCid}, " +
        s"latestTimestamp=$timestamp vs incomingTimestamp=${message.timestamp}"
    ) >>
      IO.pure(None)
  }
}
