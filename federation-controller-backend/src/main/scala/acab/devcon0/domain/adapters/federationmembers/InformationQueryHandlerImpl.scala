package acab.devcon0.domain.adapters.federationmembers

import acab.devcon0.domain.dtos.FederationMemberInformation
import acab.devcon0.domain.ports.input._
import acab.devcon0.domain.ports.input.federationmembers.InformationQuery
import acab.devcon0.domain.ports.input.federationmembers.InformationQueryHandler
import acab.devcon0.domain.service.federationmember.InformationService
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class InformationQueryHandlerImpl(service: InformationService[IO]) extends InformationQueryHandler {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(query: InformationQuery): IO[List[FederationMemberInformation]] = {
    service.getAll
      .flatTap(logResult)
      .onError(logError)
  }

  private def logResult(federationMembersInformation: List[FederationMemberInformation]): IO[Unit] = {
    logger.debug(s"federationMembersInformation=$federationMembersInformation")
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }

}
