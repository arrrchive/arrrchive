package acab.devcon0.domain.adapters.federation

import acab.devcon0.domain.ports.input._
import acab.devcon0.domain.ports.input.federation._
import acab.devcon0.domain.service.IpfsClusterService
import acab.devcon0.domain.service.P2pService
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationControllerHeartbeat
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class HeartbeatCommandHandlerImpl(p2PService: P2pService[IO], ipfsClusterService: IpfsClusterService[IO])
    extends HeartbeatCommandHandler {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(cmd: HeartbeatCommand): IO[HeartbeatEvent[?]] = {
    for
      ipfsClusterPeers              <- ipfsClusterService.getPeers
      FederationControllerHeartbeat <- IO(FederationControllerHeartbeat(ipfsClusterPeers.peers))
      event                         <- publishMessage(FederationControllerHeartbeat)
    yield event
  }

  private def publishMessage(
      FederationControllerHeartbeat: FederationControllerHeartbeat
  ): IO[HeartbeatEvent[?]] = {
    p2PService
      .publish(FederationControllerHeartbeat)
      .map(_ => HeartbeatSuccessEvent())
      .onError(throwable => IO(HeartbeatErrorEvent(throwable)))
  }
}
