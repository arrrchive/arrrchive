package acab.devcon0.domain.adapters.ipfscid

import acab.devcon0.domain.dtos.IpfsCidDto
import acab.devcon0.domain.ports.input.ipfscid._
import acab.devcon0.domain.service.IpfsService
import acab.devcon0.domain.service.ipfscid.FacadeService
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO

class TreeQueryHandlerImpl(
    facadeService: FacadeService[IO],
    ipfsService: IpfsService
) extends TreeQueryHandler {

  override def handle(query: TreeQuery): IO[IpfsCidDto] = {
    query match
      case NodesTreeQuery(maybeIpfsCid) => getNodeTree(maybeIpfsCid)
      case FileTreeQuery(ipfsCid)       => getFileTree(ipfsCid)
  }

  private def getFileTree(ipfsCid: IpfsCid): IO[IpfsCidDto] = {
    for
      maybeCacheHit <- facadeService.getFileTree(ipfsCid)
      result <- maybeCacheHit match
        case Some(value) => IO(value)
        case None =>
          ipfsService
            .lsFileTree(ipfsCid)
            .flatTap(facadeService.saveFileTree)
    yield result
  }

  private def getNodeTree(ipfsCid: IpfsCid): IO[IpfsCidDto] = {
    for
      maybeCacheHit <- facadeService.getNodeTree(ipfsCid)
      result <- maybeCacheHit match
        case Some(value) => IO(value)
        case None =>
          ipfsService
            .lsFileTree(ipfsCid)
            .flatTap(facadeService.saveNodeTree)
    yield result
  }
}
