package acab.devcon0.domain.adapters.federationmember

import acab.devcon0.domain.dtos._
import acab.devcon0.domain.mappers.IpfsCidMapper
import acab.devcon0.domain.ports.input._
import acab.devcon0.domain.ports.input.federationmember._
import acab.devcon0.domain.service._
import acab.devcon0.domain.service.federationmember.ChangelogService
import acab.devcon0.domain.service.federationmember.CidsService
import acab.devcon0.domain.service.ipfscid.FacadeService
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class IpfsCidDeltaUpdateCommandHandlerImpl(
    ipfsCidFacadeService: FacadeService[IO],
    ipfsService: IpfsService,
    cidsService: CidsService[IO],
    changelogService: ChangelogService[IO]
) extends IpfsCidDeltaUpdateCommandHandler {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(cmd: IpfsCidDeltaUpdateCommand): IO[IpfsCidDeltaUpdateEvent[?]] = {
    val ipfsCid: IpfsCid = cmd.federationMemberChangelogItem.ipfsCid
    for
      ipfsCidFileTree <- getAndCacheIpfsCidFileTree(ipfsCid)
      _               <- IO.unit &> saveIpfsCidNodeTreeToCache(ipfsCidFileTree)
      _               <- IO.unit &> saveIpfsCidFlats(ipfsCidFileTree, cmd)
      delta           <- getFederationMemberIpfsCidDelta(cmd)
    yield IpfsCidDeltaUpdateSuccessEvent(delta)
  }

  private def getFederationMemberIpfsCidDelta(cmd: IpfsCidDeltaUpdateCommand): IO[FederationMemberIpfsCidDelta] = {
    for
      additions <- getIpfsCidsMemberAdds(cmd)
      removals  <- getIpfsCidsMemberDeletes(cmd)
    yield FederationMemberIpfsCidDelta(federationMemberId = cmd.id, additions = additions, removals = removals)
  }

  private def getIpfsCidsMemberDeletes(cmd: IpfsCidDeltaUpdateCommand): IO[Set[IpfsCid]] = {
    val ipfsCid: IpfsCid                                             = cmd.federationMemberChangelogItem.ipfsCid
    val federationMemberId: FederationMemberId                       = cmd.id
    val federationMemberChangelogItem: FederationMemberChangelogItem = cmd.federationMemberChangelogItem
    for
      maybePreviousRootIpfsCid <- getPreviousRootIpfsCid(federationMemberId, federationMemberChangelogItem)
      deletedCids <- maybePreviousRootIpfsCid match
        case None             => IO(Set())
        case Some(oldIpfsCid) => cidsService.getDeleted(federationMemberId, oldIpfsCid, ipfsCid)
    yield deletedCids
  }

  private def getPreviousRootIpfsCid(
      federationMemberId: FederationMemberId,
      federationMemberChangelogItem: FederationMemberChangelogItem
  ): IO[Option[IpfsCid]] = {
    changelogService
      .getPrevious(federationMemberId, federationMemberChangelogItem)
      .map(_.map(_.ipfsCid))
  }

  private def getIpfsCidsMemberAdds(cmd: IpfsCidDeltaUpdateCommand): IO[Set[IpfsCid]] = {
    val ipfsCid: IpfsCid                                             = cmd.federationMemberChangelogItem.ipfsCid
    val federationMemberId: FederationMemberId                       = cmd.id
    val federationMemberChangelogItem: FederationMemberChangelogItem = cmd.federationMemberChangelogItem
    for
      maybePreviousRootIpfsCid <- getPreviousRootIpfsCid(federationMemberId, federationMemberChangelogItem)
      addedCids <- maybePreviousRootIpfsCid match
        case None             => cidsService.getAll(federationMemberId, ipfsCid)
        case Some(oldIpfsCid) => cidsService.getAdded(federationMemberId, oldIpfsCid, ipfsCid)
    yield {
      addedCids
    }
  }

  private def getAndCacheIpfsCidFileTree(ipfsCid: IpfsCid): IO[IpfsCidDto] = {
    for
      maybeCacheHit <- getIpfsCidFileTreeFromCache(ipfsCid)
      ipfsCidDto    <- maybeCacheHit.map(IO(_)).getOrElse(getIpfsCidFileTreeFromService(ipfsCid))
    yield ipfsCidDto
  }

  private def getIpfsCidFileTreeFromCache(ipfsCid: IpfsCid): IO[Option[IpfsCidDto]] = {
    ipfsCidFacadeService
      .getFileTree(ipfsCid)
      .onError(logError)
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }

  private def getIpfsCidFileTreeFromService(ipfsCid: IpfsCid): IO[IpfsCidDto] = {
    ipfsService
      .lsFileTree(ipfsCid)
      .flatTap(saveIpfsCidFileTreeToCache)
      .onError(logError)
  }

  private def saveIpfsCidFileTreeToCache(ipfsCidDto: IpfsCidDto): IO[Unit] = {
    ipfsCidFacadeService
      .saveFileTree(ipfsCidDto)
      .onError(logError)
      .void
  }

  private def saveIpfsCidNodeTreeToCache(ipfsCidDto: IpfsCidDto): IO[Unit] = {
    for
      maybeCacheHit <- ipfsCidFacadeService.getNodeTree(ipfsCidDto.cid)
      _ <- maybeCacheHit match
        case Some(_) => IO.unit
        case None    => IO(IpfsCidMapper.NodeTree.to(ipfsCidDto)).flatMap(ipfsCidFacadeService.saveNodeTree).void
    yield {}
  }

  private def saveIpfsCidFlats(
      ipfsCidFileTree: IpfsCidDto,
      cmd: IpfsCidDeltaUpdateCommand
  ): IO[Set[IpfsCidDto]] = {
    for
      allIpfsCidDtos <- IO(IpfsCidMapper.FlatAll.to(ipfsCidFileTree))
      ipfsCid            = cmd.federationMemberChangelogItem.ipfsCid
      federationMemberId = cmd.id
      allIpfsCids <- IO(allIpfsCidDtos.map(_.cid))
      _           <- cidsService.add(federationMemberId, ipfsCid, allIpfsCids)
      _           <- saveIpfsCidFlatsInner(allIpfsCidDtos)
    yield allIpfsCidDtos
  }

  private def saveIpfsCidFlatsInner(allIpfsCids: Set[IpfsCidDto]): IO[Unit] = {
    ipfsCidFacadeService.saveFlats(allIpfsCids).onError(logError).void
  }
}
