package acab.devcon0.domain.adapters.federationmember

import acab.devcon0.domain.dtos.FederationMemberInformation
import acab.devcon0.domain.ports.input._
import acab.devcon0.domain.ports.input.federationmember._
import acab.devcon0.domain.service.federationmember.InformationService
import acab.devcon0.trile.utils.EffectsUtils
import cats.effect.IO
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class InformationQueryHandlerImpl(informationService: InformationService[IO]) extends InformationQueryHandler {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def handle(query: InformationQuery): IO[FederationMemberInformation] = {
    handleInner(query)
      .flatMap(EffectsUtils.forceOptionExistence[FederationMemberInformation]())
  }

  private def handleInner(query: InformationQuery): IO[Option[FederationMemberInformation]] = {
    query match
      case InformationByP2pPeerIdQuery(p2pPeerId)   => informationService.getByP2pPeerId(p2pPeerId)
      case InformationByIdQuery(federationMemberId) => informationService.getById(federationMemberId)
  }

  override def handleOptional(query: InformationQuery): IO[Option[FederationMemberInformation]] = {
    handleInner(query)
  }
}
