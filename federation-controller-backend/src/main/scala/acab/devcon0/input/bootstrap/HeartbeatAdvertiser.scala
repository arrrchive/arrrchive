package acab.devcon0.input.bootstrap

import scala.concurrent.duration.DurationInt
import scala.concurrent.duration.FiniteDuration

import acab.devcon0.domain.ports.input.federation.HeartbeatCommand
import acab.devcon0.domain.ports.input.federation.HeartbeatCommandHandler
import acab.devcon0.domain.ports.input.federation.HeartbeatCommandImplicits.HeartbeatCommandEventFlattenOps
import acab.devcon0.trile.utils.EffectsUtils
import acab.devcon0.trile.utils.IORetry
import cats.effect.IO
import cats.implicits.catsSyntaxMonadError
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class HeartbeatAdvertiser(heartbeatCommandHandler: HeartbeatCommandHandler) {

  private implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]
  private val halfMinute: FiniteDuration  = 30.seconds

  def run(): IO[Unit] = {
    logger.info(s"Heartbeat-er started") >> triggerHeartbeat
  }

  private def triggerHeartbeat: IO[Unit] = {
    val command: HeartbeatCommand = HeartbeatCommand()
    triggerHeartbeat(command)
      .attemptTap(EffectsUtils.attemptTLog)
      .flatTap(_ => logger.debug(s"Heartbeat sent to the federation members"))
      .recoverWith(_ => IO.unit)
      .flatTap(_ => IO.sleep(halfMinute))
      .foreverM
  }

  private def triggerHeartbeat(command: HeartbeatCommand): IO[Unit] = {
    val heartbeatIO: IO[Unit] = heartbeatCommandHandler.handle(command).flattenEvents
    IORetry.fibonacci(heartbeatIO)
  }
}
