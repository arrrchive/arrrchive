package acab.devcon0.input.p2ppubsub
import acab.devcon0.configuration.P2pConfiguration
import acab.devcon0.domain.dtos.pubsub
import acab.devcon0.domain.ports.input.federationmember.BounceSharingFolderUpdateCommand
import acab.devcon0.domain.ports.input.federationmember.BounceSharingFolderUpdateCommandHandler
import acab.devcon0.domain.ports.input.federationmember.InformationByP2pPeerIdQuery
import acab.devcon0.domain.ports.input.federationmember.InformationQueryHandler
import acab.devcon0.trile.domain.codecs.P2pCodecs
import acab.devcon0.trile.domain.codecs.P2pCodecs.Decoders.sharingFolderUpdate
import acab.devcon0.trile.domain.dtos.pubsub.P2p
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Listener.Mode
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Listener.Params
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberSharingFolderUpdate
import acab.devcon0.trile.domain.dtos.pubsub.P2p.MessageType
import cats.effect._
import cats.effect.unsafe.IORuntime
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class FederationMemberSharingFolderUpdateListener(
    p2pListener: P2pListener,
    p2pConfiguration: P2pConfiguration,
    bounceSharingFolderUpdateCommandHandler: BounceSharingFolderUpdateCommandHandler,
    informationQueryHandler: InformationQueryHandler
) {

  private implicit val runtime: IORuntime = cats.effect.unsafe.IORuntime.global
  private val logger: Logger[IO]          = Slf4jLogger.getLogger[IO]

  def run(): IO[Unit] = {
    for
      topic <- IO(p2pConfiguration.topics.sharingFolderUpdate)
      listenerParams = Params(
        topic = topic,
        messageTypes = List(MessageType.Event),
        mode = Mode.Broadcast
      )
      _ <- p2pListener
        .run[FederationMemberSharingFolderUpdate](
          listenerParams,
          message => triggerCommandInBackground(message)
        )
    yield ()
  }

  private def triggerCommandInBackground(message: Message[FederationMemberSharingFolderUpdate]): IO[Unit] =
    IO {
      val pubSubMessage = message.data
      (for
        _ <- logger.debug(
          s"IPFS peer IPNS update received over P2P pub/sub. beginningOfMessage${message.data.toString.substring(0, 64)}}"
        )
        federationMemberInformation <- informationQueryHandler.handle(InformationByP2pPeerIdQuery(message.meta.from))
        ipfsPeerIpnsUpdateMessage = pubsub.Redis.FederationMemberSharingFolderUpdateMessage(
          federationMemberInformation.id,
          pubSubMessage.sharedFolderCid,
          pubSubMessage.timestamp
        )
        command = BounceSharingFolderUpdateCommand(ipfsPeerIpnsUpdateMessage)
        _ <- bounceSharingFolderUpdateCommandHandler.handle(command)
      yield ()).unsafeRunAndForget()
    }
}
