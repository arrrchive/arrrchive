package acab.devcon0.input.p2ppubsub

import acab.devcon0.configuration.P2pConfiguration
import acab.devcon0.domain.ports.input.federationmember.InformationByP2pPeerIdQuery
import acab.devcon0.domain.ports.input.federationmember.InformationQueryHandler
import acab.devcon0.domain.ports.input.federationmember.InformationRefreshCommand
import acab.devcon0.domain.ports.input.federationmember.UpdateInformationCommandHandler
import acab.devcon0.trile.domain.codecs.P2pCodecs
import acab.devcon0.trile.domain.codecs.P2pCodecs.Decoders.federationMemberHeartbeat
import acab.devcon0.trile.domain.dtos.pubsub.P2p
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Listener.Mode
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Listener.Params
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message
import acab.devcon0.trile.domain.dtos.pubsub.P2p.Message.FederationMemberHeartbeat
import acab.devcon0.trile.domain.dtos.pubsub.P2p.MessageType
import cats.effect._
import cats.effect.unsafe.IORuntime
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class FederationMemberHeartbeatListener(
    p2pListener: P2pListener,
    p2pConfiguration: P2pConfiguration,
    informationQueryHandler: InformationQueryHandler,
    updateInformationCommandHandler: UpdateInformationCommandHandler
) {

  private implicit val runtime: IORuntime = cats.effect.unsafe.IORuntime.global
  private val logger: Logger[IO]          = Slf4jLogger.getLogger[IO]

  def run(): IO[Unit] = {
    for
      topic <- IO(p2pConfiguration.topics.heartbeat)
      listenerParams = Params(
        topic = topic,
        messageTypes = List(MessageType.Event),
        mode = Mode.Broadcast
      )
      _ <- p2pListener.run(listenerParams, message => triggerCommandInBackground(message))
    yield ()
  }

  private def triggerCommandInBackground(message: Message[FederationMemberHeartbeat]): IO[Unit] = IO {
    val heartbeat: FederationMemberHeartbeat = message.data

    (for
      _ <- logger.debug(s"Hearbeat in message received over P2P pub/sub. from=${message.meta.from}}")

      information <- informationQueryHandler.handle(InformationByP2pPeerIdQuery(message.meta.from))
      command = InformationRefreshCommand(id = information.id, heartbeat.sharedFolderCid)
      _ <- updateInformationCommandHandler.handle(command)
    yield ())
      .unsafeRunAndForget()
  }
}
