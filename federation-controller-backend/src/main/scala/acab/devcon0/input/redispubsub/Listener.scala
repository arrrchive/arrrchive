package acab.devcon0.input.redispubsub

import cats.effect._
import cats.effect.unsafe.IORuntime
import dev.profunktor.redis4cats.connection.RedisClient
import dev.profunktor.redis4cats.data._
import dev.profunktor.redis4cats.log4cats._
import dev.profunktor.redis4cats.pubsub.PubSub
import dev.profunktor.redis4cats.pubsub.SubscribeCommands
import fs2.Pipe
import fs2.Stream
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class Listener(redisClient: Resource[IO, RedisClient]) {

  private implicit val runtime: IORuntime             = cats.effect.unsafe.IORuntime.global
  private implicit val logger: Logger[IO]             = Slf4jLogger.getLogger[IO]
  private val stringCodec: RedisCodec[String, String] = RedisCodec.Utf8

  def run(channel: RedisChannel[String], pipe: Pipe[IO, String, Unit]): Unit = {
    subscribeToRedisPubSubStream(channel)
      .through(pipe)
      .compile
      .foldMonoid
      .foreverM
      .unsafeRunAndForget()
  }

  private def subscribeToRedisPubSubStream(channel: RedisChannel[String]): Stream[IO, String] = {
    Stream
      .resource(redisClient)
      .flatMap(getSubscriberConnectionResource)
      .flatMap(_.subscribe(channel))
  }

  private def getSubscriberConnectionResource(
      client: RedisClient
  ): Stream[IO, SubscribeCommands[[String] =>> Stream[IO, String], String, String]] = {
    Stream.resource(PubSub.mkSubscriberConnection[IO, String, String](client, stringCodec))
  }
}
