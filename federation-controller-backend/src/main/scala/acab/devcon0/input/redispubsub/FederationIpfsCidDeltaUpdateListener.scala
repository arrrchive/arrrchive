package acab.devcon0.input.redispubsub

import acab.devcon0.domain.codecs.FederationCodecs
import acab.devcon0.domain.dtos.pubsub.Redis.FederationIpfsCidsDeltaUpdateMessage
import acab.devcon0.domain.ports.input.federation.IpfsCidDeltaUpdateCommand
import acab.devcon0.domain.ports.input.federation.IpfsCidDeltaUpdateCommandHandler
import acab.devcon0.output.repository.redisutils.Redis
import acab.devcon0.trile.utils.IORetry
import cats.effect._
import cats.effect.std.Supervisor
import cats.effect.unsafe.IORuntime
import dev.profunktor.redis4cats.data._
import fs2.Pipe
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class FederationIpfsCidDeltaUpdateListener(
    redisListener: Listener,
    ipfsCidDeltaUpdateCommandHandler: IpfsCidDeltaUpdateCommandHandler
) {

  cats.effect.unsafe.IORuntime.global
  implicit val logger: Logger[IO]           = Slf4jLogger.getLogger[IO]
  private val channel: RedisChannel[String] = Redis.Channels.federationIpfsCidsDeltaUpdate

  def run(): Unit = {
    redisListener.run(channel, processMessage())
  }

  private def processMessage(): Pipe[IO, String, Unit] = stream => {
    stream
      .evalMap(FederationCodecs.Decoders.IpfsCidsDeltaUpdateMessage(_))
      .evalTap(processMessageInner)
      .evalTap(logReceivedMessage)
      .evalMap(* => IO.unit)
  }

  private def processMessageInner(message: FederationIpfsCidsDeltaUpdateMessage): IO[Unit] = {
    Supervisor[IO](await = true)
      .use(_ => {
        val cmd: IpfsCidDeltaUpdateCommand = IpfsCidDeltaUpdateCommand(delta = message.delta)
        IORetry.fibonacci(ipfsCidDeltaUpdateCommandHandler.handle(cmd))
      })
      .void
  }

  private def logReceivedMessage(message: FederationIpfsCidsDeltaUpdateMessage): IO[Unit] = {
    logger.info(
      s"Message received & processed over Redis pub/sub. additions=${message.delta.additions.size} removals=${message.delta.removals.size}"
    )
  }
}
