package acab.devcon0.input.redispubsub

import acab.devcon0.domain.codecs.FederationMemberCodecs
import acab.devcon0.domain.codecs.FederationMemberCodecs.Decoders.ChangelogUpdateMessage
import acab.devcon0.domain.dtos.FederationMemberIpfsCidDelta
import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberChangelogUpdateMessage
import acab.devcon0.domain.dtos.pubsub.Redis.FederationMemberIpfsCidsDeltaUpdateMessage
import acab.devcon0.domain.ports.input.federationmember.SyncCommandImplicits.SyncAckCommandEventFlattenOps
import acab.devcon0.domain.ports.input.federationmember._
import acab.devcon0.domain.ports.output.publisher.RedisPublisher
import acab.devcon0.output.repository.redisutils.Redis
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect._
import cats.effect.std.Supervisor
import cats.effect.unsafe.IORuntime
import cats.implicits.toTraverseOps
import dev.profunktor.redis4cats.data._
import fs2.Pipe
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class FederationMemberChangelogUpdateEventListener(
    redisListener: Listener,
    ipfsCidUpdateCommandHandler: IpfsCidDeltaUpdateCommandHandler,
    updateInformationRefreshCommandHandler: UpdateInformationCommandHandler,
    redisPublisher: RedisPublisher[IO],
    syncCommandHandler: SyncCommandHandler
) {

  cats.effect.unsafe.IORuntime.global
  private val logger: Logger[IO]                 = Slf4jLogger.getLogger[IO]
  private val redisChannel: RedisChannel[String] = Redis.Channels.federationMemberChangelogUpdate
  private val chunkSize: Int                     = 5000

  def run(): Unit = {
    redisListener.run(redisChannel, processMessage())
  }

  private def processMessage(): Pipe[IO, String, Unit] = stream => {
    stream
      .evalMap(FederationMemberCodecs.Decoders.ChangelogUpdateMessage(_))
      .evalTap(logReceivedMessage)
      .evalTap(processInner)
      .evalMap(* => IO.unit)
  }

  private def processInner(message: FederationMemberChangelogUpdateMessage): IO[Unit] = {
    Supervisor[IO](await = true).use { supervisor =>
      for
        event <- triggerIpfsCidFileTreeUpdateCommand(message)
        _     <- triggerUpdateInformation(message.federationMemberId, message.federationMemberChangelogItem.ipfsCid)
        _     <- reportSyncAckToMember(message)
        _ <- event match
          case IpfsCidDeltaUpdateSuccessEvent(delta) => report(delta)
          case IpfsCidDeltaUpdateErrorEvent()        => reportSyncNackToMember(message)
      yield ()
    }
  }

  private def reportSyncAckToMember(message: FederationMemberChangelogUpdateMessage): IO[Unit] = {
    syncCommandHandler.handle(SyncAckCommand(message.federationMemberId)).flattenEvents
  }

  private def reportSyncNackToMember(message: FederationMemberChangelogUpdateMessage): IO[Unit] = {
    syncCommandHandler.handle(SyncNackCommand(message.federationMemberId)).flattenEvents
  }

  private def report(delta: FederationMemberIpfsCidDelta): IO[Unit] = {
    for
      _ <- reportAdditions(delta)
      _ <- reportRemovals(delta)
    yield ()
  }

  private def reportAdditions(delta: FederationMemberIpfsCidDelta): IO[Unit] = {
    Supervisor[IO](await = true).use { supervisor =>
      val messages: List[FederationMemberIpfsCidsDeltaUpdateMessage] = delta.additions
        .grouped(chunkSize)
        .map(cids => FederationMemberIpfsCidDelta(delta.federationMemberId, cids, Set()))
        .map(newDelta => FederationMemberIpfsCidsDeltaUpdateMessage(newDelta))
        .toList
      supervisor.supervise(messages.traverse(redisPublisher.publish))
    }.void
  }

  private def reportRemovals(delta: FederationMemberIpfsCidDelta): IO[Unit] = {
    Supervisor[IO](await = true).use { supervisor =>
      val messages = delta.removals
        .grouped(chunkSize)
        .map(cids => FederationMemberIpfsCidDelta(delta.federationMemberId, Set(), cids))
        .map(newDelta => FederationMemberIpfsCidsDeltaUpdateMessage(newDelta))
        .toList
      supervisor.supervise(messages.traverse(redisPublisher.publish))
    }.void
  }

  private def triggerUpdateInformation(federationMemberId: FederationMemberId, ipfsCid: IpfsCid): IO[Unit] = {
    val command = InformationRefreshCommand(federationMemberId, ipfsCid)
    logger.info(s"triggerUpdateInformation command=$command") >>
      updateInformationRefreshCommandHandler.handle(command).void
  }

  private def triggerIpfsCidFileTreeUpdateCommand(
      message: FederationMemberChangelogUpdateMessage
  ): IO[IpfsCidDeltaUpdateEvent[?]] = {
    val command: IpfsCidDeltaUpdateCommand = IpfsCidDeltaUpdateCommand(
      message.federationMemberId,
      message.federationMemberChangelogItem
    )
    ipfsCidUpdateCommandHandler.handle(command)
  }

  private def logReceivedMessage(message: FederationMemberChangelogUpdateMessage): IO[Unit] = {
    logger.info(s"Message received & processed over Redis pub/sub. message=$message")
  }
}
