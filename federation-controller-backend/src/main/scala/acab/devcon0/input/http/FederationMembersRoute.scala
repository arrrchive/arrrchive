package acab.devcon0.input.http

import acab.devcon0.domain.codecs.FederationMemberCodecs.Encoders.Information.codec
import acab.devcon0.domain.ports.input.federationmembers.InformationQuery
import acab.devcon0.domain.ports.input.federationmembers.InformationQueryHandler
import cats.effect._
import org.http4s.HttpRoutes
import org.http4s.circe.CirceEntityEncoder._
import org.http4s.dsl.io._
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class FederationMembersRoute(informationQueryHandler: InformationQueryHandler) {

  private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  val routes: HttpRoutes[IO] = {
    HttpRoutes
      .of[IO] { case GET -> Root =>
        (for
          federationMembersInformation <- informationQueryHandler.handle(InformationQuery())
          response                     <- Ok(federationMembersInformation)
        yield response)
          .handleErrorWith(throwable => {
            BadRequest().flatTap(_ => logError(throwable))
          })
      }
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }
}
