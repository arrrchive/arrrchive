package acab.devcon0.input.http

import acab.devcon0.domain.codecs.FederationMemberCodecs
import acab.devcon0.domain.dtos.FederationMemberInformation
import acab.devcon0.domain.ports.input.federationmember
import acab.devcon0.domain.ports.input.federationmember.InformationByIdQuery
import acab.devcon0.domain.ports.input.federationmember.InformationQueryHandler
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import cats.effect._
import org.http4s.HttpRoutes
import org.http4s.circe.CirceEntityEncoder.circeEntityEncoder
import org.http4s.dsl.io._
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class FederationMemberRoute(informationQueryHandler: InformationQueryHandler) {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  val routes: HttpRoutes[IO] = HttpRoutes
    .of[IO] { case GET -> Root / federationMemberId / "info" =>
      (for
        maybeFederationMemberInformation <- getFederationMemberInformation(federationMemberId)
        response <- maybeFederationMemberInformation match {
          case Some(info) => FederationMemberCodecs.Encoders.Information(info).flatMap(Ok(_))
          case None       => NotFound()
        }
      yield response).handleErrorWith(_ => BadRequest())
    }

  private def getFederationMemberInformation(
      federationMemberId: FederationMemberId
  ): IO[Option[FederationMemberInformation]] = {
    informationQueryHandler.handleOptional(InformationByIdQuery(federationMemberId))
  }
}
