package acab.devcon0.input.http

import acab.devcon0.configuration.P2pConfiguration
import cats.effect._
import org.http4s._
import org.http4s.circe.CirceEntityEncoder.circeEntityEncoder
import org.http4s.dsl.io._
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class P2pRoute(p2pConfiguration: P2pConfiguration) {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  val routes: HttpRoutes[IO] = HttpRoutes
    .of[IO] { case GET -> Root / "id" =>
      (for response <- Ok(p2pConfiguration.peerId.toBase58)
      yield response).handleErrorWith(_ => BadRequest())

    }
}
