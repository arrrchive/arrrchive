package acab.devcon0.input.http

import cats.effect.IO
import fs2.Pipe
import fs2.Stream
import org.http4s.HttpApp
import org.http4s.HttpRoutes
import org.http4s.dsl.io._
import org.http4s.server.websocket.WebSocketBuilder2
import org.http4s.websocket.WebSocketFrame

class WebsocketRoute {

  def app(subscribeToRedisPubSubStream: fs2.Stream[IO, String], wsb: WebSocketBuilder2[IO]): HttpApp[IO] = {
    HttpRoutes
      .of[IO] { case GET -> Root / "websocket" =>
        val send: Stream[IO, WebSocketFrame] = subscribeToRedisPubSubStream.map(WebSocketFrame.Text(_))
        val receive: Pipe[IO, WebSocketFrame, Unit] = _.foreach(frame => {
          IO.unit
        })
        wsb.build(send, receive)
      }
      .orNotFound
  }
}
