package acab.devcon0.input.http

import cats.effect.IO
import org.http4s.Http
import org.http4s.server.Router
import org.http4s.server.middleware.CORS

class Routes(
    federationRoute: FederationRoute,
    federationMemberRoute: FederationMemberRoute,
    federationMembersRoute: FederationMembersRoute,
    ipfsCidRoute: IpfsCidRoute,
    p2pRoute: P2pRoute
) {

  val httpApp: Http[IO, IO] = CORS.policy.withAllowOriginAll(
    Router(
      "/api/federation/"         -> federationRoute.routes,
      "/api/federation/members" -> federationMembersRoute.routes,
      "/api/federation/members" -> federationMemberRoute.routes,
      "/api/ipfs/"              -> ipfsCidRoute.routes,
      "/api/p2p/"               -> p2pRoute.routes
    ).orNotFound
  )

}
