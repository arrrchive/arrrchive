package acab.devcon0.input.http

import acab.devcon0.configuration.Configuration
import acab.devcon0.trile.utils.EffectsUtils
import cats.effect._
import cats.implicits.catsSyntaxMonadError
import fs2.io.file.Files
import fs2.io.file.Path
import org.http4s._
import org.http4s.circe.CirceEntityEncoder.circeEntityEncoder
import org.http4s.dsl.io._
import org.http4s.headers.Authorization
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class FederationRoute(configuration: Configuration) {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  val routes: HttpRoutes[IO] = HttpRoutes
    .of[IO] { case req @ GET -> Root / ".fedvars" =>
      validateToken(req)
        .flatMap {
          case ok @ Response(Status.Ok, _, _, _, _) =>
            val filePath: Path = Path(configuration.fedvarsAbsolutePath)
            Files[IO]
              .readAll(filePath)
              .through(fs2.text.utf8.decode)
              .compile
              .string
              .flatMap(fileContent => Ok(fileContent))
          case badRequest @ Response(Status.BadRequest, _, _, _, _)     => IO.pure(badRequest)
          case unauthorized @ Response(Status.Unauthorized, _, _, _, _) => IO.pure(unauthorized)
        }
        .attemptTap(EffectsUtils.attemptTLog)
    }

  private def validateToken(req: Request[IO]): IO[Response[IO]] = {
    val maybeAuthorization: Option[Authorization] = req.headers.get[Authorization]
    maybeAuthorization match {
      case Some(Authorization(Credentials.Token(AuthScheme.Bearer, joinToken))) =>
        if isValidToken(joinToken) then { IO(Response(Status.Ok)) }
        else { IO(Response(Status.Unauthorized)) }
      case _ => IO(Response(Status.BadRequest).withEntity("Missing Authorization Header"))
    }
  }

  private def isValidToken(joinToken: String): Boolean = {
    configuration.auth.joinToken.equals(joinToken)
  }
}
