package acab.devcon0.output.repository.redisutils;


import io.lettuce.core.protocol.ProtocolKeyword;

import java.nio.charset.StandardCharsets;

public enum FTSearchProtocolKeyword implements ProtocolKeyword {
    FT_SEARCH;

    public final byte[] bytes;

    FTSearchProtocolKeyword() {
        bytes = "FT.SEARCH".getBytes(StandardCharsets.US_ASCII);
    }

    @Override
    public byte[] getBytes() {
        return bytes;
    }
}
