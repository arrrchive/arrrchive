package acab.devcon0.output.repository.redisutils;


import io.lettuce.core.protocol.ProtocolKeyword;

import java.nio.charset.StandardCharsets;

public enum FTListProtocolKeyword implements ProtocolKeyword {
    FT__LIST;

    public final byte[] bytes;

    FTListProtocolKeyword() {
        bytes = "FT._LIST".getBytes(StandardCharsets.US_ASCII);
    }

    @Override
    public byte[] getBytes() {
        return bytes;
    }
}
