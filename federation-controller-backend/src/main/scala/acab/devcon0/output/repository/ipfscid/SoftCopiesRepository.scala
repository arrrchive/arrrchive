package acab.devcon0.output.repository.ipfscid

import acab.devcon0.domain.ports.output.repository.ipfscid.SoftCopiesRepository
import acab.devcon0.output.repository.redisutils
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import acab.devcon0.trile.utils.EffectsUtils
import cats.effect.IO
import cats.effect.kernel.Resource
import cats.syntax.all._
import dev.profunktor.redis4cats._
import dev.profunktor.redis4cats.tx.TxStore
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

private object SoftCopiesRepository {

  def key(ipfsCid: IpfsCid): String = {
    String.join(":", redisutils.Redis.SetPrefixes.ipfsCidFlatSoftCopies, ipfsCid)
  }

  object Operations {

    def get(ipfsCid: IpfsCid): RedisCommands[IO, String, String] => IO[Int] = { redis =>
      redis.get(key(ipfsCid)).map(_.getOrElse("0").toInt)
    }

    def set(ipfsCid: IpfsCid, count: Int): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
      redis.set(key(ipfsCid), count.toString).void
    }

    def delete(ipfsCid: IpfsCid): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
      redis.del(key(ipfsCid)).void
    }
  }
}

class SoftCopiesRepositoryImpl(val commandsApi: Resource[IO, RedisCommands[IO, String, String]])
    extends SoftCopiesRepository[IO] {

  private implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def get(ipfsCid: IpfsCid): IO[Int] = {
    commandsApi
      .use(SoftCopiesRepository.Operations.get(ipfsCid)(_))
  }

  override def set(ipfsCid: IpfsCid, count: Int): IO[Unit] = {
    commandsApi
      .use(setInner(ipfsCid, count, _))
  }

  override def delete(ipfsCid: IpfsCid): IO[Unit] = {
    commandsApi
      .use(deleteInner(ipfsCid, _))
  }

  private def setInner(ipfsCid: IpfsCid, count: Int, redis: RedisCommands[IO, String, String]): IO[Unit] = {
    val operations: TxStore[IO, String, String] => List[IO[Unit]] = { _ =>
      List(
        SoftCopiesRepository.Operations.set(ipfsCid, count)(redis)
      )
    }
    redis
      .transact(operations)
      .void
      .attemptTap(EffectsUtils.attemptTLog)
  }

  private def deleteInner(ipfsCid: IpfsCid, redis: RedisCommands[IO, String, String]): IO[Unit] = {
    val operations: TxStore[IO, String, String] => List[IO[Unit]] = { _ =>
      List(
        SoftCopiesRepository.Operations.delete(ipfsCid)(redis)
      )
    }
    redis
      .transact(operations)
      .void
      .attemptTap(EffectsUtils.attemptTLog)
  }
}
