package acab.devcon0.output.repository.federationmember

import java.time.Instant

import scala.collection.immutable.List

import acab.devcon0.domain.codecs.FederationMemberCodecs
import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.output.repository.federationmember.ChangelogRepository
import acab.devcon0.output.repository.redisutils
import acab.devcon0.trile.domain.dtos.aliases.FederationMemberId
import cats.effect.IO
import cats.effect.kernel.Resource
import cats.implicits._
import dev.profunktor.redis4cats._
import dev.profunktor.redis4cats.effects.Score
import dev.profunktor.redis4cats.effects.ScoreWithValue
import dev.profunktor.redis4cats.effects.ZRange
import dev.profunktor.redis4cats.tx.TxStore
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

object FederationMemberChangelog {

  private def key(id: FederationMemberId): String = {
    String.join(":", redisutils.Redis.SetPrefixes.federationMemberChangelog, id)
  }

  object Operations {
    def add(
        id: FederationMemberId,
        timestamp: Instant,
        rawJson: String
    ): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
      val score: Score                           = Score(timestamp.toEpochMilli.toDouble)
      val scoreWithValue: ScoreWithValue[String] = ScoreWithValue(score, rawJson)
      redis.zAdd(key(id), args = None, scoreWithValue).void
    }

    def remove(
        id: FederationMemberId,
        rawJson: String
    ): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
      redis.zRem(key(id), rawJson).void
    }

    def getNewest(
        id: FederationMemberId
    ): RedisCommands[IO, String, String] => IO[Option[String]] = { redis =>
      val key: String = FederationMemberChangelog.key(id)
      getNewestByKey(key)(redis)
    }

    def getOlderThan(
        id: FederationMemberId,
        timestamp: Instant
    ): RedisCommands[IO, String, String] => IO[List[String]] = { redis =>
      val key: String      = FederationMemberChangelog.key(id)
      val instant: Instant = timestamp
      redis.zRangeByScore(key, ZRange(Long.MinValue, instant.toEpochMilli), limit = None)
    }

    private def getNewestByKey(
        key: String
    ): RedisCommands[IO, String, String] => IO[Option[String]] = { redis =>
      redis
        .zRevRangeWithScores(key, 0, 0)
        .map(value => value.headOption.map(_.value))
    }
  }
}

class ChangelogRepositoryImpl(val commandsApi: Resource[IO, RedisCommands[IO, String, String]])
    extends ChangelogRepository[IO] {

  private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def getNewest(id: FederationMemberId): IO[Option[FederationMemberChangelogItem]] = {
    commandsApi
      .use(FederationMemberChangelog.Operations.getNewest(id)(_))
      .flatMap(decode)

  }

  override def getPrevious(
      id: FederationMemberId,
      timestamp: Instant
  ): IO[List[FederationMemberChangelogItem]] = {
    commandsApi
      .use(FederationMemberChangelog.Operations.getOlderThan(id, timestamp)(_))
      .flatMap(decode)

  }

  override def add(
      id: FederationMemberId,
      federationMemberChangelogItem: FederationMemberChangelogItem
  ): IO[FederationMemberChangelogItem] = {
    FederationMemberCodecs.Encoders
      .ChangelogItem(federationMemberChangelogItem)
      .flatMap(rawJson => {
        commandsApi.use(addInner(id, federationMemberChangelogItem, rawJson, _))
      })
  }

  override def update(
      id: FederationMemberId,
      federationMemberChangelogItemOld: FederationMemberChangelogItem,
      federationMemberChangelogItemNew: FederationMemberChangelogItem
  ): IO[FederationMemberChangelogItem] = {
    for
      rawJsonOld <- FederationMemberCodecs.Encoders.ChangelogItem(federationMemberChangelogItemOld)
      rawJsonNew <- FederationMemberCodecs.Encoders.ChangelogItem(federationMemberChangelogItemNew)
      _ <- commandsApi.use(
        updateInner(id, federationMemberChangelogItemNew, rawJsonOld, rawJsonNew, _)
      )
    yield federationMemberChangelogItemNew
  }

  private def decode(rawJsons: List[String]): IO[List[FederationMemberChangelogItem]] = {
    rawJsons.traverse(FederationMemberCodecs.Decoders.ChangelogItem(_))
  }

  private def decode(maybeRawJson: Option[String]): IO[Option[FederationMemberChangelogItem]] = {
    maybeRawJson match
      case Some(rawJson) => FederationMemberCodecs.Decoders.ChangelogItem(rawJson).map(Some(_))
      case None          => IO.pure(None)
  }

  private def addInner(
      id: FederationMemberId,
      federationMemberChangelogItem: FederationMemberChangelogItem,
      rawJson: String,
      redis: RedisCommands[IO, String, String]
  ): IO[FederationMemberChangelogItem] = {
    val operations: TxStore[IO, String, String] => List[IO[Unit]] = { _ =>
      val timestamp = federationMemberChangelogItem.timestamp
      List(FederationMemberChangelog.Operations.add(id, timestamp, rawJson)(redis))
    }
    runTransaction(redis, operations)
      .map(_ => federationMemberChangelogItem)
  }

  private def updateInner(
      id: FederationMemberId,
      federationMemberChangelogItem: FederationMemberChangelogItem,
      rawJsonOld: String,
      rawJsonNew: String,
      redis: RedisCommands[IO, String, String]
  ): IO[FederationMemberChangelogItem] = {
    val operations: TxStore[IO, String, String] => List[IO[Unit]] = { _ =>
      val timestamp = federationMemberChangelogItem.timestamp
      getUpdateOperations(id, timestamp, rawJsonOld, rawJsonNew, redis)
    }
    runTransaction(redis, operations)
      .map(_ => federationMemberChangelogItem)
  }

  private def getUpdateOperations(
      id: FederationMemberId,
      timestamp: Instant,
      rawJsonOld: String,
      rawJsonNew: String,
      redis: RedisCommands[IO, String, String]
  ): List[IO[Unit]] = {
    List(
      FederationMemberChangelog.Operations.remove(id, rawJsonOld)(redis),
      FederationMemberChangelog.Operations.add(id, timestamp, rawJsonNew)(redis)
    )
  }

  private def runTransaction(
      redis: RedisCommands[IO, String, String],
      operations: TxStore[IO, String, String] => List[IO[Unit]]
  ): IO[Unit] = {
    redis
      .transact(operations)
      .void
      .onError(logError)
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }
}
