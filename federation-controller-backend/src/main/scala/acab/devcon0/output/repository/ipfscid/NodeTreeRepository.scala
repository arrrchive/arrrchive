package acab.devcon0.output.repository.ipfscid

import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.output.repository.ipfscid.Repository
import acab.devcon0.output.repository.redisutils
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO
import cats.effect.kernel.Resource
import dev.profunktor.redis4cats._
import dev.profunktor.redis4cats.tx.TxStore
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

private object NodeTreeRepository {

  def key(ipfsCid: IpfsCid): String = {
    String.join(":", redisutils.Redis.SetPrefixes.ipfsCidNodeTree, ipfsCid)
  }

  object Operations {
    def get(ipfsCid: IpfsCid): RedisCommands[IO, String, String] => IO[Option[String]] = { redis =>
      redis.get(key(ipfsCid))
    }

    def save(ipfsCid: IpfsCid, rawJson: String): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
      redis.set(key(ipfsCid), rawJson)
    }
  }
}

class NodeTreeRepository(val commandsApi: Resource[IO, RedisCommands[IO, String, String]]) extends Repository[IO] {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def get(
      ipfsCid: IpfsCid
  ): IO[Option[IpfsCidDto]] = {
    commandsApi
      .use(NodeTreeRepository.Operations.get(ipfsCid)(_))
      .flatMap(Repository.Decoder.to)
  }

  override def save(
      ipfsCidDto: IpfsCidDto
  ): IO[IpfsCidDto] = {
    Repository.Encoder
      .from(ipfsCidDto)
      .flatMap(rawJson => commandsApi.use(saveInner(ipfsCidDto.cid, rawJson, _)))
      .map(_ => ipfsCidDto)
  }

  private def saveInner(ipfsCid: IpfsCid, rawJson: String, redis: RedisCommands[IO, String, String]): IO[Unit] = {
    val operations: TxStore[IO, String, String] => List[IO[Unit]] = { _ =>
      getSetOperations(ipfsCid, rawJson, redis)
    }
    runTransaction(redis, operations)
  }

  private def getSetOperations(
      ipfsCid: IpfsCid,
      rawJson: String,
      redis: RedisCommands[IO, String, String]
  ): List[IO[Unit]] =
    List(
      NodeTreeRepository.Operations.save(ipfsCid, rawJson)(redis)
    )

  private def runTransaction(
      redis: RedisCommands[IO, String, String],
      operations: TxStore[IO, String, String] => List[IO[Unit]]
  ): IO[Unit] = {
    redis
      .transact(operations)
      .void
      .onError(logError)
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }
}
