package acab.devcon0.output.repository.ipfscid

import acab.devcon0.domain.codecs.IpfsCidCodecs
import acab.devcon0.domain.codecs.IpfsCidCodecs.Decoders.Dto
import acab.devcon0.domain.codecs.IpfsCidCodecs.Encoders.Dto
import acab.devcon0.domain.dtos._
import cats.effect.IO
import cats.syntax.all._

object Repository {
  object Encoder {
    def from(ipfsCidDto: IpfsCidDto): IO[String] = {
      IpfsCidCodecs.Encoders.Dto(ipfsCidDto)
    }
  }

  object Decoder {
    def to(maybeRawJson: Option[String]): IO[Option[IpfsCidDto]] = {
      maybeRawJson match
        case Some(rawJson) => to(rawJson).map(Some(_))
        case None          => IO.pure(None)
    }

    def to(rawJson: String): IO[IpfsCidDto] = {
      IpfsCidCodecs.Decoders.Dto(rawJson)
    }

    def to(rawJsons: Set[String]): IO[Set[IpfsCidDto]] = {
      rawJsons.toSeq.traverse(to).map(_.toSet)
    }
  }
}
