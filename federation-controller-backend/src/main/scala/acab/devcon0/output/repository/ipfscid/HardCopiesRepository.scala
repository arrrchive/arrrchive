package acab.devcon0.output.repository.ipfscid

import acab.devcon0.domain.ports.output.repository.ipfscid.HardCopiesRepository
import acab.devcon0.output.repository.redisutils
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO
import cats.effect.kernel.Resource
import cats.syntax.all._
import dev.profunktor.redis4cats._
import dev.profunktor.redis4cats.tx.TxStore
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

private object CopiesRepository {

  def key(ipfsCid: IpfsCid): String = {
    String.join(":", redisutils.Redis.SetPrefixes.ipfsCidFlatHardCopies, ipfsCid)
  }

  object Operations {

    def get(ipfsCid: IpfsCid): RedisCommands[IO, String, String] => IO[Int] = { redis =>
      redis.get(key(ipfsCid)).map(_.getOrElse("0").toInt)
    }

    def increment(ipfsCids: Set[IpfsCid]): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
      ipfsCids.toSeq.traverse(ipfsCid => redis.incr(key(ipfsCid))).void
    }

    def decrement(ipfsCids: Set[IpfsCid]): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
      ipfsCids.toSeq.traverse { ipfsCid =>
        val redisKey: String = key(ipfsCid)
        for
          currentVal <- redis.get(redisKey).map(_.getOrElse("0").toInt)
          _          <- if currentVal > 0 then redis.del(redisKey) else IO.unit
        yield ()
      }.void
    }
  }
}

class HardCopiesRepositoryImpl(val commandsApi: Resource[IO, RedisCommands[IO, String, String]])
    extends HardCopiesRepository[IO] {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def get(ipfsCid: IpfsCid): IO[Int] = {
    commandsApi
      .use(CopiesRepository.Operations.get(ipfsCid)(_))
  }

  override def increment(ipfsCids: Set[IpfsCid]): IO[Unit] = {
    commandsApi
      .use(incrementInner(ipfsCids, _))
  }

  override def decrement(ipfsCids: Set[IpfsCid]): IO[Unit] = {
    commandsApi
      .use(decrementInner(ipfsCids, _))
  }

  private def incrementInner(ipfsCids: Set[IpfsCid], redis: RedisCommands[IO, String, String]): IO[Unit] = {
    val operations: TxStore[IO, String, String] => List[IO[Unit]] = { _ =>
      List(
        CopiesRepository.Operations.increment(ipfsCids)(redis)
      )
    }
    runTransaction(redis, operations)
      .flatTap(_ => logger.debug(s"CopiesRepository.save OK for tuple=$ipfsCids"))
  }

  private def decrementInner(ipfsCids: Set[IpfsCid], redis: RedisCommands[IO, String, String]): IO[Unit] = {
    val operations: TxStore[IO, String, String] => List[IO[Unit]] = { _ =>
      List(
        CopiesRepository.Operations.decrement(ipfsCids)(redis)
      )
    }
    runTransaction(redis, operations)
      .flatTap(_ => logger.debug(s"CopiesRepository.delete OK for tuple=$ipfsCids"))
  }

  private def runTransaction(
      redis: RedisCommands[IO, String, String],
      operations: TxStore[IO, String, String] => List[IO[Unit]]
  ): IO[Unit] = {
    redis
      .transact(operations)
      .void
      .onError(logError)
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }
}
