package acab.devcon0.output.repository.ipfscid

import java.util

import scala.jdk.CollectionConverters.CollectionHasAsScala
import scala.jdk.CollectionConverters.IterableHasAsJava
import scala.util.Try

import acab.devcon0.domain.dtos.SearchParameters
import acab.devcon0.domain.dtos.SearchResult
import acab.devcon0.domain.ports.output.repository.ipfscid.SearchNameRepository
import acab.devcon0.output.repository.redisutils
import acab.devcon0.output.repository.redisutils.FTCreateProtocolKeyword
import acab.devcon0.output.repository.redisutils.FTListProtocolKeyword
import acab.devcon0.output.repository.redisutils.FTSearchProtocolKeyword
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO
import cats.effect.kernel.Resource
import dev.profunktor.redis4cats._
import dev.profunktor.redis4cats.tx.TxStore
import io.lettuce.core.codec.StringCodec
import io.lettuce.core.output.ArrayOutput
import io.lettuce.core.output.StatusOutput
import io.lettuce.core.protocol.CommandArgs
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

private object SearchNameRepository {

  object Operations {

    Slf4jLogger.getLogger[IO]
    private val prefix: String      = redisutils.Redis.SetPrefixes.ipfsCidFlat
    private val indexPrefix: String = redisutils.Redis.SetPrefixes.ipfsCidFlatIndex
    private val pageSize: Long      = 10

    def search(searchParameters: SearchParameters): RedisCommands[IO, String, String] => IO[SearchResult[IpfsCid]] = {
      redis =>
        val commandArgs: CommandArgs[String, String] = new CommandArgs(StringCodec.UTF8)
          .add(s"$indexPrefix")
          .add(s"'@name: %${searchParameters.term}% | @name: *${searchParameters.term}*'")
          .add("RETURN")
          .add("0")
          .add("LIMIT")
          .add((pageSize * searchParameters.page).toString)
          .add(pageSize.toString)

        redis
          .unsafe(
            _.dispatch(
              FTSearchProtocolKeyword.FT_SEARCH,
              new ArrayOutput[String, String](StringCodec.UTF8),
              commandArgs
            )
          )
          .map(searchResults => {
            val totalCount: Long       = Long.unbox(searchResults.get(3))
            val ipfsCids: Set[IpfsCid] = redisResultsToIpfsCids(searchResults)
            SearchResult(searchParameters, ipfsCids, totalCount, pageSize)
          })
    }

    def createIndex(): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
      for
        indexes <- listIndexesInner(redis)
        _       <- if indexes.contains(indexPrefix) then IO.unit else createIndexInner(redis)
      yield ()

    }

    private def redisResultsToIpfsCids(searchResults: util.List[AnyRef]): Set[String] = {
      Try {
        searchResults
          .get(7)
          .asInstanceOf[util.ArrayList[util.ArrayList[String]]] // scalafix:ok
          .asScala
          .map(_.get(1))
          .map(_.replace(redisutils.Redis.SetPrefixes.ipfsCidFlat, "").substring(1))
          .toSet
      }.getOrElse(Set())
    }

    private def createIndexInner(redis: RedisCommands[IO, String, String]): IO[Unit] = {
      val schema = s"$indexPrefix ON HASH PREFIX 1 $prefix SCHEMA name TEXT SORTABLE"
      val commandArgs: CommandArgs[String, String] = new CommandArgs(StringCodec.UTF8)
        .addValues(schema.split(" ").toList.asJava)
      redis
        .unsafe(
          _.dispatch(
            FTCreateProtocolKeyword.FT_CREATE,
            new StatusOutput[String, String](StringCodec.UTF8),
            commandArgs
          )
        )
        .void
    }

    private def listIndexesInner(redis: RedisCommands[IO, String, String]): IO[List[String]] = {
      redis
        .unsafe(
          _.dispatch(
            FTListProtocolKeyword.FT__LIST,
            new ArrayOutput[String, String](StringCodec.UTF8),
            new CommandArgs(StringCodec.UTF8)
          )
        )
        .map(_.asScala.toList.map(_.toString))
    }
  }
}

class SearchNameRepositoryImpl(val commandsApi: Resource[IO, RedisCommands[IO, String, String]])
    extends SearchNameRepository[IO] {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def createIndex(): IO[Unit] = {
    commandsApi.use(redis => {
      val operations: TxStore[IO, String, String] => List[IO[Unit]] = { _ =>
        List(SearchNameRepository.Operations.createIndex()(redis))
      }
      runTransaction(redis, operations)
        .flatTap(_ => logger.info(s"SearchNameRepository.createIndex OK"))
    })
  }

  override def search(searchParameters: SearchParameters): IO[SearchResult[IpfsCid]] = {
    commandsApi.use(SearchNameRepository.Operations.search(searchParameters)(_))
  }

  private def runTransaction(
      redis: RedisCommands[IO, String, String],
      operations: TxStore[IO, String, String] => List[IO[Unit]]
  ): IO[Unit] = {
    redis
      .transact(operations)
      .void
      .onError(logError)
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }
}
