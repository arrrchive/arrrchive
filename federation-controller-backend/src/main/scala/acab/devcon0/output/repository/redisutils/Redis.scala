package acab.devcon0.output.repository.redisutils

import dev.profunktor.redis4cats.data.RedisChannel

object Redis {
  object SetPrefixes {
    val ipfsCidFlat: String           = "IPFS:CID:FLAT"
    val ipfsCidFlatIndex: String      = "IPFS:CID:FLAT/INDEX"
    val ipfsCidFlatHardCopies: String = "IPFS:CID:FLAT/HARD_COPIES"
    val ipfsCidFlatSoftCopies: String = "IPFS:CID:FLAT/SOFT_COPIES"
    val ipfsCidNodeTree: String       = "IPFS:CID:NODE_TREE"
    val ipfsCidFileTree: String       = "IPFS:CID:FILE_TREE"

    val federationMemberInformation: String      = "FEDERATION:MEMBER:INFORMATION"
    val federationMemberInformationIndex: String = "FEDERATION:MEMBER:INFORMATION/INDEX"
    val federationMemberChangelog: String        = "FEDERATION:MEMBER:CHANGELOG"
    val federationMemberCidsIndex: String        = "FEDERATION:MEMBER:CIDS/INDEX"
  }

  object IndexesKeys {
    val p2pPeerId: String         = "P2P_PEER_ID"
    val ipfsClusterNodeId: String = "IPFS_CLUSTER_NODE_ID"
  }
  object Channels {
    val federationIpfsCidsDeltaUpdate: RedisChannel[String] = RedisChannel("FEDERATION_IPFS_DELTA_UPDATE")
    val federationIpfsCidAddition: RedisChannel[String]     = RedisChannel("FEDERATION_IPFS_ADDITION")
    val federationIpfsCidRemoval: RedisChannel[String]      = RedisChannel("FEDERATION_IPFS_REMOVAL")

    val federationMemberChangelogUpdate: RedisChannel[String] = RedisChannel("FEDERATION_MEMBER_CHANGELOG_UPDATE")
    val federationMemberSharingFolderUpdate: RedisChannel[String] = RedisChannel(
      "FEDERATION_MEMBER_SHARING_FOLDER_UPDATE"
    )
    val federationMemberIpfsCidsDeltaUpdate: RedisChannel[String] = RedisChannel(
      "FEDERATION_MEMBER_IPFS_CIDS_DELTA_UPDATE"
    )
  }
}
