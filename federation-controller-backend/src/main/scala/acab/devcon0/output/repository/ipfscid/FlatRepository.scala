package acab.devcon0.output.repository.ipfscid

import acab.devcon0.domain.dtos._
import acab.devcon0.domain.ports.output.repository.ipfscid.Repository
import acab.devcon0.output.repository.redisutils
import acab.devcon0.trile.domain.dtos.aliases.IpfsCid
import cats.effect.IO
import cats.effect.kernel.Resource
import cats.implicits._
import dev.profunktor.redis4cats._
import dev.profunktor.redis4cats.tx.TxStore
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

private object FlatRepository {

  private val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  def key(ipfsCid: IpfsCid): String = {
    String.join(":", redisutils.Redis.SetPrefixes.ipfsCidFlat, ipfsCid)
  }

  object Operations {

    def get(ipfsCid: IpfsCid): RedisCommands[IO, String, String] => IO[Option[String]] = { redis =>
      redis.hGet(key(ipfsCid), "json")
    }

    def getAll(ipfsCids: Set[IpfsCid]): RedisCommands[IO, String, String] => IO[Set[String]] = { redis =>
      for
        keys      <- IO(ipfsCids.map(key))
        resultMap <- keys.toSeq.traverse(redis.hmGet(_, "json"))
      yield {
        resultMap.flatMap(_.values).toSet
      }
    }

    def save(ipfsCidDto: IpfsCidDto, rawJson: String): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
      redis.hSet(key(ipfsCidDto.cid), Map("json" -> rawJson, "name" -> ipfsCidDto.name)).void
    }

    def saveAll(cidJsonTuples: List[(IpfsCidDto, String)]): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
      cidJsonTuples
        .traverse(tuple => save(tuple._1, tuple._2)(redis))
        .flatTap(_ => logger.error("SAVE ALL OK"))
        .onError(_ => logger.error("SAVE ALL NOT OK"))
        .void
    }

    def deleteAll(ipfsCids: Set[IpfsCid]): RedisCommands[IO, String, String] => IO[Unit] = { redis =>
      val keys = ipfsCids.map(key).toSeq
      redis.del(keys*).void
    }
  }
}

class FlatRepository(val commandsApi: Resource[IO, RedisCommands[IO, String, String]]) extends Repository[IO] {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]

  override def get(ipfsCid: IpfsCid): IO[Option[IpfsCidDto]] = {
    commandsApi
      .use(FlatRepository.Operations.get(ipfsCid)(_))
      .flatMap(Repository.Decoder.to)
  }

  override def save(ipfsCidDto: IpfsCidDto): IO[IpfsCidDto] = {
    Repository.Encoder
      .from(ipfsCidDto)
      .flatMap(rawJson => {
        commandsApi.use(saveInner(ipfsCidDto, rawJson, _))
      })
      .map(_ => ipfsCidDto)
  }

  def getAll(ipfsCids: Set[IpfsCid]): IO[Set[IpfsCidDto]] = {
    commandsApi
      .use(FlatRepository.Operations.getAll(ipfsCids)(_))
      .flatMap(Repository.Decoder.to)
  }

  def saveAll(ipfsCidDtos: Set[IpfsCidDto]): IO[Set[IpfsCidDto]] = {
    ipfsCidDtos.toList
      .traverse(dto => Repository.Encoder.from(dto).map((dto, _)))
      .flatMap(dtoRawJsons => commandsApi.use(saveAllInner(dtoRawJsons, _)))
      .map(_ => ipfsCidDtos)
  }

  def deleteAll(ipfsCids: Set[IpfsCid]): IO[Unit] = {
    if ipfsCids.nonEmpty then commandsApi.use(deleteAllInner(ipfsCids, _))
    else IO.unit
  }

  private def saveInner(
      ipfsCidDto: IpfsCidDto,
      rawJson: String,
      redis: RedisCommands[IO, String, String]
  ): IO[Unit] = {
    val operations: TxStore[IO, String, String] => List[IO[Unit]] = { _ =>
      List(
        FlatRepository.Operations.save(ipfsCidDto, rawJson)(redis)
      )
    }
    runTransaction(redis, operations)
  }

  private def saveAllInner(
      tuplesIpfsCidDtoRawJson: List[(IpfsCidDto, String)],
      redis: RedisCommands[IO, String, String]
  ): IO[Unit] = {
    val operations: TxStore[IO, String, String] => List[IO[Unit]] = { _ =>
      List(
        FlatRepository.Operations.saveAll(tuplesIpfsCidDtoRawJson)(redis)
      )
    }
    runTransaction(redis, operations)
  }

  private def deleteAllInner(
      ipfsCids: Set[IpfsCid],
      redis: RedisCommands[IO, String, String]
  ): IO[Unit] = {
    val operations: TxStore[IO, String, String] => List[IO[Unit]] = { _ =>
      List(
        FlatRepository.Operations.deleteAll(ipfsCids)(redis)
      )
    }
    runTransaction(redis, operations)
  }

  private def runTransaction(
      redis: RedisCommands[IO, String, String],
      operations: TxStore[IO, String, String] => List[IO[Unit]]
  ): IO[Unit] = {
    redis
      .transact(operations)
      .void
      .onError(logError)
  }

  private def logError(throwable: Throwable): IO[Unit] = {
    logger.error(s"throwable=$throwable")
  }
}
