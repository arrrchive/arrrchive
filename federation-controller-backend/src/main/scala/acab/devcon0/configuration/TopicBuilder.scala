package acab.devcon0.configuration

import java.security.MessageDigest

import io.libp2p.core.pubsub.Topic

object TopicBuilder {
  def apply(name: String, swarmKeyValue: String): Topic = {
    Topic(s"$name:${getSwarmKeyValueHash(swarmKeyValue)}")
  }

  private def getSwarmKeyValueHash(swarmKeyValue: String): String = {
    MessageDigest
      .getInstance("SHA-256")
      .digest(swarmKeyValue.getBytes("UTF-8"))
      .map("%02x".format(_))
      .mkString
  }
}
