package acab.devcon0.boot.ioc

import acab.devcon0.boot.ioc
import acab.devcon0.boot.ioc.CommonsIoC.Configuration
import acab.devcon0.domain.adapters.ipfscid._
import acab.devcon0.domain.ports.input.ipfscid._
import acab.devcon0.domain.ports.output.client.IpfsClient
import acab.devcon0.domain.ports.output.client.IpfsClusterClient
import acab.devcon0.domain.ports.output.repository.ipfscid.HardCopiesRepository
import acab.devcon0.domain.ports.output.repository.ipfscid.SearchNameRepository
import acab.devcon0.domain.ports.output.repository.ipfscid.SoftCopiesRepository
import acab.devcon0.domain.service.IpfsClusterService
import acab.devcon0.domain.service.IpfsClusterServiceImpl
import acab.devcon0.domain.service.IpfsService
import acab.devcon0.domain.service.IpfsServiceImpl
import acab.devcon0.domain.service.ipfscid._
import acab.devcon0.input.bootstrap.RedisIndexesBootstrap
import acab.devcon0.output.client.IpfsClientImpl
import acab.devcon0.output.client.IpfsClusterClientImpl
import acab.devcon0.output.repository.ipfscid._
import cats.effect.IO
import cats.effect.Resource
import sttp.client4.SyncBackend
import sttp.client4.httpurlconnection.HttpURLConnectionBackend

object IpfsIoC {

  object Input {
    val redisIndexesBootstrap: RedisIndexesBootstrap = new RedisIndexesBootstrap(
      searchNameService = Domain.Service.searchNameService
    )
  }

  object Domain {
    object Port {
      val fileTreeQueryQueryHandler: TreeQueryHandler = TreeQueryHandlerImpl(
        facadeService = Service.facadeService,
        ipfsService = Domain.Service.ipfsService
      )

      val flatQueryHandler: FlatQueryHandler = FlatQueryHandlerImpl(
        facadeService = Service.facadeService,
        ipfsService = Domain.Service.ipfsService
      )

      val metaDataQueryHandler: MetadataQueryHandler = MetadataQueryHandlerImpl(
        facadeService = Service.facadeService,
        ipfsService = Domain.Service.ipfsService,
        hardCopiesService = Service.hardCopiesService,
        softCopiesService = Service.softCopiesService
      )

      val searchQueryHandler: SearchQueryHandler = SearchQueryHandlerImpl(
        facadeService = Service.facadeService,
        searchNameService = Service.searchNameService,
        hardCopiesService = Service.hardCopiesService,
        softCopiesService = Service.softCopiesService
      )
    }

    object Service {

      val ipfsService: IpfsService = new IpfsServiceImpl(
        ipfsClient = Output.ipfsClient,
        flatRepository = Output.flatRepository
      )

      val ipfsClusterService: IpfsClusterService[IO] = new IpfsClusterServiceImpl(
        client = Output.ipfsClusterClient
      )

      val searchNameService: SearchNameService[IO] = new SearchNameServiceImpl(
        repository = Output.searchNameRepository
      )

      val hardCopiesService: HardCopiesService[IO] = new HardCopiesServiceImpl(
        repository = Output.hardCopiesRepository
      )

      val softCopiesService: SoftCopiesService[IO] = new SoftCopiesServiceImpl(
        repository = Output.softCopiesRepository
      )

      val facadeService: FacadeService[IO] = new FacadeServiceImpl(
        flatRepository = Output.flatRepository,
        fileTreeRepository = Output.fileTreeRepository,
        nodeTreeRepository = Output.nodeTreeRepository
      )
    }
  }

  object Output {

    private val backend: SyncBackend                       = HttpURLConnectionBackend()
    private val backendResource: Resource[IO, SyncBackend] = Resource.make(IO(backend))(res => IO(res.close()))

    val ipfsClusterClient: IpfsClusterClient[IO] = new IpfsClusterClientImpl(
      apiUrl = Configuration.configuration.ipfsCluster.apiUrl,
      backendResource = backendResource
    )

    val ipfsClient: IpfsClient = new IpfsClientImpl(
      backendResource = backendResource,
      ipfsConfiguration = Configuration.configuration.ipfs
    )

    val hardCopiesRepository: HardCopiesRepository[IO] = new HardCopiesRepositoryImpl(
      commandsApi = CommonsIoC.InputOutput.redisCommandsFactory()
    )

    val softCopiesRepository: SoftCopiesRepository[IO] = new SoftCopiesRepositoryImpl(
      commandsApi = CommonsIoC.InputOutput.redisCommandsFactory()
    )

    val searchNameRepository: SearchNameRepository[IO] = new SearchNameRepositoryImpl(
      commandsApi = CommonsIoC.InputOutput.redisCommandsFactory()
    )

    val flatRepository: FlatRepository = new FlatRepository(
      commandsApi = CommonsIoC.InputOutput.redisCommandsFactory()
    )

    val fileTreeRepository: FileTreeRepository = new FileTreeRepository(
      commandsApi = CommonsIoC.InputOutput.redisCommandsFactory()
    )

    val nodeTreeRepository: NodeTreeRepository = new NodeTreeRepository(
      commandsApi = CommonsIoC.InputOutput.redisCommandsFactory()
    )
  }
}
