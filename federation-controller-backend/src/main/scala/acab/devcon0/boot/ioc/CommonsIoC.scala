package acab.devcon0.boot.ioc

import acab.devcon0.boot.ioc
import acab.devcon0.configuration._
import acab.devcon0.domain.ports.output
import acab.devcon0.domain.ports.output._
import acab.devcon0.domain.ports.output.client.P2pClient
import acab.devcon0.domain.ports.output.publisher.RedisPublisher
import acab.devcon0.domain.service.P2pService
import acab.devcon0.domain.service.P2pServiceImpl
import acab.devcon0.input.http._
import acab.devcon0.input.p2ppubsub
import acab.devcon0.input.redispubsub
import acab.devcon0.input.redispubsub._
import acab.devcon0.inputoutput.P2pBackend
import acab.devcon0.inputoutput.P2pBackendImpl
import acab.devcon0.output._
import acab.devcon0.output.client.P2pClientImpl
import acab.devcon0.output.publisher.RedisPublisherImpl
import cats.effect.IO
import cats.effect.Resource
import dev.profunktor.redis4cats.RedisCommands
import dev.profunktor.redis4cats.connection.RedisClient
import dev.profunktor.redis4cats.data._
import dev.profunktor.redis4cats.log4cats._
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

object CommonsIoC {

  object InputOutput {

    val p2pBackend: P2pBackend[IO] = new P2pBackendImpl(p2pConfiguration = Configuration.configuration.p2p)

    implicit val logger: Logger[IO]                     = Slf4jLogger.getLogger[IO]
    private val stringCodec: RedisCodec[String, String] = RedisCodec.Utf8
    private val redisConfiguration: RedisConfiguration  = Configuration.configuration.redis
    private val redisHost = s"redis://${redisConfiguration.host}:${redisConfiguration.port}"
    def redisClientFactory(): Resource[IO, RedisClient] = {
      RedisClient[IO].from(redisHost)
    }

    def redisCommandsFactory(): Resource[IO, RedisCommands[IO, String, String]] = {
      RedisClient[IO]
        .from(redisHost)
        .flatMap(dev.profunktor.redis4cats.Redis[IO].fromClient(_, stringCodec))
    }
  }

  object Configuration {
    val configuration: Configuration = TrileControllerBackendConfigFactory.build()

  }
  object Input {

    val p2pPubSubListener: p2ppubsub.P2pListener = new p2ppubsub.P2pListener(
      p2pBackend = CommonsIoC.InputOutput.p2pBackend
    )

    val redisListener: redispubsub.Listener = new redispubsub.Listener(
      redisClient = CommonsIoC.InputOutput.redisClientFactory()
    )

    val nodeRoute: FederationMemberRoute =
      FederationMemberRoute(informationQueryHandler = FederationMemberIoC.Domain.Port.informationQueryHandler)
    val ipfsCidRoute: IpfsCidRoute = IpfsCidRoute(
      ipfsCidTreeQueryHandler = IpfsIoC.Domain.Port.fileTreeQueryQueryHandler,
      ipfsCidFlatQueryHandler = IpfsIoC.Domain.Port.flatQueryHandler,
      ipfsCidMetadataQueryHandler = IpfsIoC.Domain.Port.metaDataQueryHandler,
      ipfsCidSearchQueryHandler = IpfsIoC.Domain.Port.searchQueryHandler
    )

    val federationMembersRoute: FederationMembersRoute = FederationMembersRoute(
      informationQueryHandler = FederationMemberIoC.Domain.Port.federationMembersInformationQueryHandler
    )

    val federationRoute: FederationRoute = new FederationRoute(Configuration.configuration)

    val p2pRoute: P2pRoute = new P2pRoute(Configuration.configuration.p2p)

    val routes: Routes = new Routes(
      federationRoute = federationRoute,
      federationMemberRoute = nodeRoute,
      federationMembersRoute = federationMembersRoute,
      ipfsCidRoute = ipfsCidRoute,
      p2pRoute = p2pRoute
    )

    def websocketRoute: WebsocketRoute = new WebsocketRoute()

  }

  object Domain {
    object Service {
      val p2pService: P2pService[IO] = new P2pServiceImpl(
        p2pClient = Output.p2pClient
      )
    }
  }

  object Output {
    import CommonsIoC.InputOutput.*

    val p2pClient: P2pClient[IO] = new P2pClientImpl(
      p2pBackend = CommonsIoC.InputOutput.p2pBackend,
      p2pConfiguration = Configuration.configuration.p2p
    )

    val redisPubSubPublisher: RedisPublisher[IO] = {
      new RedisPublisherImpl(
        redisClient = CommonsIoC.InputOutput.redisClientFactory()
      )
    }
  }
}
