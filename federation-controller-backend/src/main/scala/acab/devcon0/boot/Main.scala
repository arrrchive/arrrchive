package acab.devcon0.boot

import acab.devcon0.boot.ioc.CommonsIoC
import acab.devcon0.boot.ioc.FederationIoC
import acab.devcon0.boot.ioc.FederationMemberIoC
import acab.devcon0.boot.ioc.IpfsIoC
import acab.devcon0.configuration.Configuration
import acab.devcon0.configuration.ConfigurationCodecs
import acab.devcon0.configuration.ConfigurationCodecs.Encoders._
import cats.effect.ExitCode
import cats.effect.IO
import cats.effect.IOApp
import cats.effect.std.Supervisor
import com.comcast.ip4s.Port
import com.comcast.ip4s.host
import org.http4s.ember.server.EmberServerBuilder
import org.typelevel.log4cats.Logger
import org.typelevel.log4cats.slf4j.Slf4jLogger

object Main extends IOApp.Simple {

  private val logger: Logger[IO]                = Slf4jLogger.getLogger[IO]
  private val trileConfiguration: Configuration = CommonsIoC.Configuration.configuration

  override protected def blockedThreadDetectionEnabled: Boolean = true

  private val logConfigurationProgram: IO[Unit] = {
    ConfigurationCodecs.Encoders
      .Configuration(trileConfiguration)
      .flatMap(str => logger.warn(s"Configuration=$str"))
  }
  private val bootstrapPrograms: IO[Unit] = {
    CommonsIoC.InputOutput.p2pBackend.init >>
      IO(IpfsIoC.Input.redisIndexesBootstrap.run())
  }

  private val p2pListenerPrograms: IO[Unit] = {
    FederationIoC.Input.heartbeatAdvertiser.run() &>
      FederationMemberIoC.Input.heartbeatListener.run() &>
      FederationMemberIoC.Input.checkInListener.run() &>
      FederationMemberIoC.Input.syncListener.run() &>
      FederationMemberIoC.Input.sharingFolderUpdateP2pListener.run()
  }

  private val redisListenerPrograms: IO[Unit] = {
    IO(FederationMemberIoC.Input.sharingFolderUpdateRedisListener.run()) >>
      IO(FederationMemberIoC.Input.changelogUpdateRedisListener.run()) >>
      IO(FederationIoC.Input.federationMemberIpfsCidDeltaUpdateRedisListener.run()) >>
      IO(FederationIoC.Input.ipfsCidDeltaUpdateRedisListener.run())
  }

  private val serverProgram: IO[Unit] = {
    EmberServerBuilder
      .default[IO]
      .withHost(host"0.0.0.0")
      .withPort(Port.fromInt(trileConfiguration.http.port).get)
      .withHttpApp(CommonsIoC.Input.routes.httpApp)
      .build
      .use(_ => IO.never)
      .as(ExitCode.Success)
      .void
  }

  val run: IO[Unit] = Supervisor[IO](await = true).use { supervisor =>
    (for
      _ <- logConfigurationProgram
      _ <- bootstrapPrograms
      _ <- supervisor.supervise(p2pListenerPrograms)
      _ <- supervisor.supervise(redisListenerPrograms)
      _ <- supervisor.supervise(serverProgram)
    yield ()) >> IO.never.void
  }
}
